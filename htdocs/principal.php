﻿<?php
session_start();
 
require_once "codigo/connr.php"; 

if (isset($_GET["cat"])){
    $cat = $_GET["cat"];
	$_SESSION["cat"]= $cat;
/*	if( ($cat == "M1" || $cat == "M2" || $cat == "M3" ) ) {
		header('Location:trabajando.php'); 
	}  */  
}else{
	if (isset($_SESSION["cat"])){
		$cat=$_SESSION["cat"];
		/*
		if( ($cat == "M1" || $cat == "M2" || $cat == "M3" ) ) {
			header('Location:trabajando.php'); 
		} 
		*/
	}else{
		header('Location:menu.php'); 
	}
}

?>

<!DOCTYPE html>
<html lang="es-ES">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="xmlrpc.php">
	<title>Campeonato Infantil de F&uacute;tbol &#8211; Noviembre 2019 &#8211; Club de Regatas Bella Vista</title>
	<link rel='dns-prefetch' href='//fonts.googleapis.com' />
	
	<link rel='stylesheet' id='style-css'  href='css/style.css' type='text/css' media='all' />
	<link rel='stylesheet' id='framework-css'  href='css/framework.css' type='text/css' media='all' />
	<link rel='stylesheet' id='agregado-css'  href='css/agregado.css' type='text/css' media='all' />
	<link rel='stylesheet' id='style002-css'  href='css/style002.css' type='text/css' media='all' />
	<link rel='stylesheet' id='sportspress'  href='css/sportspress-sponsors.css' type='text/css' media='all' />
	
	<link rel="stylesheet" type="text/css" href="css/themes/smoothness/jquery-ui-1.8.4.custom.css" />
	
	<script type="text/javascript" src="codigo/jquery-1.8.2.js"></script>
	<script type="text/javascript" src="codigo/jquery-ui-1.9.0.custom.min.js"></script>
	
	<link rel="stylesheet" type="text/css" href="slick/slick.css">
	<link rel="stylesheet" type="text/css" href="slick/slick-theme.css">
	<script src="slick/slick.js" type="text/javascript" charset="utf-8"></script>
 
 
	<script type="text/javascript" src="codigo/principal.js"></script>
	
	
</head>

<body class="home page-template-default page page-id-242 custom-background">

<div class="sp-header"></div>
<div id="page" class="hfeed site">
	

	<header id="masthead" class="site-header" role="banner">
				<div class="header-area header-area-has-search">
				<div class="site-branding site-branding-empty">
					<div class="site-identity"></div>
				</div><!-- .site-branding -->
			<div class="site-banner" style="position: relative;height: 148px;">
					<img class="site-banner-image" src="images/banner1000x148.jpg" alt="Club de Regatas Bella Vista" style="position: absolute;z-index: -1;">
					<?php
					if( $cat != "D"){
						$url='<img src="images/auspiciantesCat/ausp'.$cat.'.png" alt="Auspiciante" style="height: 148px;float:right;z-index:10;padding: 14px;">';
						echo($url);
					}
					if( $cat == "D"){
						echo(
							'<div style="display:flex;align-items:center;justify-content:space-around">
							  <img src="images/auspiciantesCat/auspD"  style="width:20%;float:right;z-index:10;padding: 14px;">
							  <img src="images/auspiciantesCat/auspD1"  style="width:20%;float:right;z-index:10;padding: 14px;">
							  <img src="images/auspiciantesCat/auspD2"  style="width:20%;float:right;z-index:10;padding: 14px; background:white;">
							  <img src="images/auspiciantesCat/auspD3"  style="width:20%;float:right;z-index:10;padding: 14px; background:white;">');
					}
					if( $cat == 'E'){
						echo('<img src="images/auspiciantesCat/auspE1"  style="height: 148px;float:right;z-index:10;padding: 14px;">');
					}
					?>
			</div><!-- .site-banner -->
	<div class="site-menu">
	
    <nav id="site-navigation" class="main-navigation" role="navigation">
					
	
    <div class="menuIz" style="width: 85%;">
		<span id="btinicio" class="menug">Inicio</span>
		<span id="btfixture" class="menug">Fixture</span>
        <span id="btjugadores" class="menug">Jugadores y Equipos</span>
		<span id="btcanchas" class="menug">Canchas</span>
        <span id="btregalmento" class="menug">Reglamento</span>
        <span id="btcontacto" class="menug">Contacto</span>
    </div>

    <div class="menuDe"style="width: 15%;"
        <span id="btautorizados" class="menug"  style="padding: 0.625em 0;">Usuarios Autorizados</span>
    </div>

	</nav>
    
    	
				</div>
						</div>
		</header><!-- #masthead -->

	<div id="content" class="site-content">
		
	<div id="primary" class="content-area content-area-right-sidebar">
		<main id="main" class="site-main" role="main">

			
				
<article id="post-242" class="post-242 page type-page status-publish hentry">
	<header class="entry-header">
				
		<h1 class="entry-title">Categor&iacute;a <?php echo($cat);?> </h1>	</header><!-- .entry-header -->

	<div class="entry-content">
		<p>
		<?php include 'novedades/novedades'.$cat.'.html'; ?>	
	</p>
<?php
$sql = "select distinct zona 
	from tabla_posiciones
	where idcategoria ='".$cat."'
	and idtorneo = (select idtorneo from torneo where estado='A')
	and length(zona) > 1
	order by zona;";

$result = $mysqli->query($sql);

if($result->num_rows > 0){
	echo ("<h2 class='sp-event-staff'><b>Segunda Ronda</b></h2>");
}

while ($row = $result->fetch_assoc()) {	
?>
<div class="sportspress sp-widget-align-none">
<div class="sp-template sp-template-league-table">
	<h4 class="sp-table-caption">Zona <?php echo ($row['zona']);?></h4>
	<div class="sp-table-wrapper">
		<table class="sp-league-table sp-data-table sp-sortable-table sp-scrollable-table sp-paginated-table" data-sp-rows="10">
		<thead>
		<tr>
		<th class="data-rank">Pos</th>
		<th class="data-name">Equipo</th>
		<th class="data-pj">PJ</th>
		<th class="data-g">G</th>
		<th class="data-e">E</th>
		<th class="data-p-2">P</th>
		<th class="data-gf">GF</th>
		<th class="data-gc">GC</th>
		<th class="data-dg">+/-</th>
		<th class="data-pts">Pts</th>
		</tr>
		</thead>
		<tbody>
		<?php
		$sql2 = "SELECT idtorneo, idcategoria, zona, posicion, idequipo, equipoDesc, PJ, G, E, P, GF, GC, Dif, Pts
				FROM tabla_posiciones
				where idtorneo = (select idtorneo from torneo where estado='A') 
				and idcategoria ='".$cat."'
				and zona = '".$row['zona']."'
				order by pts desc, dif desc, GF desc ;";
		$result2 = $mysqli->query($sql2);
		$pos=1;
		while ($row2 = $result2->fetch_assoc()) {	
		?>
		<tr class="odd sp-row-no-0">
		<td class="data-rank"><?php echo($pos)?></td>
		<td class="data-name"><a href="equipo.php?idequipo=<?php echo($row2['idequipo']);?>"><?php echo($row2['equipoDesc']);?></a></td>
		<td class="data-pj"><?php echo($row2['PJ']);?></td>
		<td class="data-g"><?php echo($row2['G']);?></td>
		<td class="data-e"><?php echo($row2['E']);?></td>
		<td class="data-p-2"><?php echo($row2['P']);?></td>
		<td class="data-gf"><?php echo($row2['GF']);?></td>
		<td class="data-gc"><?php echo($row2['GC']);?></td>
		<td class="data-dg"><?php echo($row2['Dif']);?></td>
		<td class="data-pts"><?php echo($row2['Pts']);?></td>
		</tr>
		<?php
		$pos++;
		}
		?>
		</tbody>
		</table>
	</div>
</div>
</div>
<p>&nbsp;</p>
<?php
}
?>

<?php
$sql = "select distinct zona 
	from tabla_posiciones
	where idcategoria ='".$cat."'
	and idtorneo = (select idtorneo from torneo where estado='A')
	and length(zona) = 1
	order by zona;";

$result = $mysqli->query($sql);

if($result->num_rows > 0){
	echo ("<h2 class='sp-event-staff'><b>Primera Ronda</b></h2>");
}

while ($row = $result->fetch_assoc()) {	
?>
<div class="sportspress sp-widget-align-none">
<div class="sp-template sp-template-league-table">
<h4 class="sp-table-caption">Zona <?php echo ($row['zona']);?></h4>
	<div class="sp-table-wrapper">
		<table class="sp-league-table sp-data-table sp-sortable-table sp-scrollable-table sp-paginated-table" data-sp-rows="10">
		<thead>
		<tr>
		<th class="data-rank">Pos</th>
		<th class="data-name">Equipo</th>
		<th class="data-pj">PJ</th>
		<th class="data-g">G</th>
		<th class="data-e">E</th>
		<th class="data-p-2">P</th>
		<th class="data-gf">GF</th>
		<th class="data-gc">GC</th>
		<th class="data-dg">+/-</th>
		<th class="data-pts">Pts</th>
		</tr>
		</thead>
		<tbody>
		<?php
		$sql2 = "SELECT idtorneo, idcategoria, zona, posicion, idequipo, equipoDesc, PJ, G, E, P, GF, GC, Dif, Pts
				FROM tabla_posiciones
				where idtorneo = (select idtorneo from torneo where estado='A') 
				and idcategoria ='".$cat."'
				and zona = '".$row['zona']."'
				order by pts desc, dif desc, GF desc ;";
		$result2 = $mysqli->query($sql2);
		$pos=1;
		while ($row2 = $result2->fetch_assoc()) {	
		?>
		<tr class="odd sp-row-no-0">
		<td class="data-rank"><?php echo($pos)?></td>
		<td class="data-name"><a href="equipo.php?idequipo=<?php echo($row2['idequipo']);?>"><?php echo($row2['equipoDesc']);?></a></td>
		<td class="data-pj"><?php echo($row2['PJ']);?></td>
		<td class="data-g"><?php echo($row2['G']);?></td>
		<td class="data-e"><?php echo($row2['E']);?></td>
		<td class="data-p-2"><?php echo($row2['P']);?></td>
		<td class="data-gf"><?php echo($row2['GF']);?></td>
		<td class="data-gc"><?php echo($row2['GC']);?></td>
		<td class="data-dg"><?php echo($row2['Dif']);?></td>
		<td class="data-pts"><?php echo($row2['Pts']);?></td>
		</tr>
		<?php
		$pos++;
		}
		?>
		</tbody>
		</table>
	</div>
</div>
</div>
<p>&nbsp;</p>
<?php
}
?>

			</div><!-- .entry-content -->
</article><!-- #post-## -->

				
			
		</main><!-- #main -->
	</div><!-- #primary -->

<div id="secondary" class="widget-area widget-area-right" role="complementary">
    <aside id="ngg-images-2" class="widget ngg_images">
		<div class="hslice" id="ngg-webslice">
	
	<section>
		<img class="img" style="width:100%" src="/sponsorsimg/conin.jpeg">
  	</section>

</div></aside><div class="sp-widget-align-none"><aside id="sportspress-event-list-3" class="widget widget_sportspress widget_sp_event_list"><div class="sp-template sp-template-event-list">

	</div>
</aside></div></div><!-- #secondary -->
			<style type="text/css">
			.sp-footer-sponsors {
				background: #f4f4f4;
				color: #363f48;
			}
			.sp-footer-sponsors .sp-sponsors .sp-sponsors-title {
				color: #363f48;
			}
			</style>
			<div class="sp-footer-sponsors">
				<div class="sportspress">	
				<?php include 'sponsors.php'; ?>

				</div>			
			</div>
			
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-area">
			<div id="quaternary" class="footer-widgets" role="complementary">
		
		<div class="footer-widget-region">
									
		<aside id="awesomeweatherwidget-2" class="widget widget_awesomeweatherwidget">
		<h3 class="widget-title">El Clima en la cancha</h3>
		<div id="awesome-weather-bella-vista-buenos-aires" class="awesome-weather-wrap awecf awe_wide awe_custom awe_with_stats awe-code-701 awe-desc-niebla awe-preset-atmosphere darken" style=" color: #ffffff;   ">
			
		<div id="cont_321e0f7f5561f9896a0c2aabd859cb3c"><script type="text/javascript" async src="https://www.meteored.com.ar/wid_loader/321e0f7f5561f9896a0c2aabd859cb3c"></script></div>
			
		</aside>
		</div>
		
		
	<div class="footer-widget-region">
		<div class="sp-widget-align-none">
		
		
		<aside id="sportspress-facebook-2" class="widget widget_sportspress widget_sp_facebook">
									
	<h3 class="widget-title">Nuestro Facebook</h3>	
	<div class="sp-template sp-template-facebook">
		<div class="sp-facebook">
		<iframe class="" name="f3843f4291321de" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:page Facebook Social Plugin" style="border: medium none; visibility: visible; width: 300px; height: 300px;" src="https://www.facebook.com/v2.8/plugins/page.php?adapt_container_width=true&app_id=818713328266556&channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FXBwzv5Yrm_1.js%3Fversion%3D42%23cb%3Df11e782fa4acbaa%26domain%3Dwww.campeonatoinfantildefutbol.com.ar%26origin%3Dhttp%253A%252F%252Fwww.campeonatoinfantildefutbol.com.ar%252Ff8c41713042498%26relation%3Dparent.parent&container_width=300&hide_cover=false&href=https%3A%2F%2Fwww.facebook.com%2Frevistaregatas%2F&locale=en_US&sdk=joey&show_facepile=true&small_header=false&tabs=timeline" frameborder="0">
		</iframe>
		</div>
	</div>
	</aside>
	
	
	
	</div></div>
									
							</div>
		</div><!-- .footer-area -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<p>&nbsp;</p>

<div id="dialog" title="Atenci&oacute;n">
Cargando...	
</div>

</body>
