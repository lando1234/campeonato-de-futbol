<?php
session_start();
 
if (isset($_GET["idequipo"])){
    $idequipo = $_GET["idequipo"];
}else{
		header('Location:menu.php'); 
}

require_once "codigo/connr.php"; 

$sql0 = "SELECT idequipo, equipoDesc, idcategoria, zona FROM equipo 
		where idtorneo = (select idtorneo from torneo where estado='A') 
		and idequipo = $idequipo;";

$result = $mysqli->query($sql0);
$row = $result->fetch_assoc();

$cat = $row['idcategoria']; 
$equipoDesc = $row['equipoDesc']; 
$zona = $row['zona']; 


	
?>

<!DOCTYPE html>
<html lang="es-ES">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="xmlrpc.php">
	<title>Campeonato Infantil de F&uacute;tbol &#8211; Noviembre 2019 &#8211; Club de Regatas Bella Vista</title>
	<link rel='dns-prefetch' href='//fonts.googleapis.com' />
	
	<link rel='stylesheet' id='style-css'  href='css/style.css' type='text/css' media='all' />
	<link rel='stylesheet' id='framework-css'  href='css/framework.css' type='text/css' media='all' />
	<link rel='stylesheet' id='agregado-css'  href='css/agregado.css' type='text/css' media='all' />
	<link rel='stylesheet' id='style002-css'  href='css/style002.css' type='text/css' media='all' />
	<link rel='stylesheet' id='sportspress'  href='css/sportspress-sponsors.css' type='text/css' media='all' />
	
	<link rel="stylesheet" type="text/css" href="css/themes/smoothness/jquery-ui-1.8.4.custom.css" />
	
	<script type="text/javascript" src="codigo/jquery-1.8.2.js"></script>
	<script type="text/javascript" src="codigo/jquery-ui-1.9.0.custom.min.js"></script>
	
	<link rel="stylesheet" type="text/css" href="slick/slick.css">
	<link rel="stylesheet" type="text/css" href="slick/slick-theme.css">
	<script src="slick/slick.js" type="text/javascript" charset="utf-8"></script>
 
 
	<script type="text/javascript" src="codigo/equipo.js"></script>
	
	
</head>

<body class="home page-template-default page page-id-242 custom-background">

<div class="sp-header"></div>
<div id="page" class="hfeed site">
	

	<header id="masthead" class="site-header" role="banner">
				<div class="header-area header-area-has-search">
				<div class="site-branding site-branding-empty">
					<div class="site-identity"></div>
				</div><!-- .site-branding -->
			<div class="site-banner" style="position: relative;height: 148px;">
					<img class="site-banner-image" src="images/banner1000x148.jpg" alt="Club de Regatas Bella Vista" style="position: absolute;z-index: -1;">
					<img src="images/auspiciantesCat/ausp<?php echo($cat);?>.png" alt="Auspiciante" style="height: 148px;float:right;z-index:10;padding: 14px;">
					<?php if( $cat == 'C'){
						echo('<img src="/sponsorsimg/clubMilanesa.png"  style="height: 148px;float:right;z-index:10;padding: 14px;">');
					} ?>
			</div><!-- .site-banner -->
	<div class="site-menu">
	
    <nav id="site-navigation" class="main-navigation" role="navigation">
					
	
    <div class="menuIz" style="width: 85%;">
		<span id="btinicio" class="menug">Inicio</span>
		<span id="btcategoria" class="menug">Tabla Posiciones</span>
		<span id="btfixture" class="menug">Fixture</span>
        <span id="btjugadores" class="menug">Jugadores y Equipos</span>
		<span id="btcanchas" class="menug">Canchas</span>
        <span id="btregalmento" class="menug">Reglamento</span>
        <span id="btcontacto" class="menug">Contacto</span>
    </div>

    <div class="menuDe"style="width: 15%;"
        <span id="btautorizados" class="menug"  style="padding: 0.625em 0;">Usuarios Autorizados</span>
    </div>

	</nav>
    
    	
				</div>
						</div>
		</header><!-- #masthead -->

	<div id="content" class="site-content">
		
	<div id="primary" class="content-area content-area-right-sidebar">
		<main id="main" class="site-main" role="main">

			
				
<article id="post-242" class="post-242 page type-page status-publish hentry">
	<header class="entry-header">
				
		<h1 class="entry-title"><?php echo($equipoDesc);?> </h1> </header><!-- .entry-header -->
	<div style="text-align: center;">Categor&iacute;a: <?php echo($cat);?> - Zona: <?php echo($zona);?></div>
	<div class="entry-content">
	

<div class="sportspress sp-widget-align-none">
<div class="sp-template sp-template-league-table">
	<h4 class="sp-table-caption">Jugadores</h4>
	<div class="sp-table-wrapper">
		<table class="sp-league-table sp-data-table sp-sortable-table sp-scrollable-table sp-paginated-table" data-sp-rows="10">
		<thead>
		<tr><th class="data-name"></th></tr>
		<tr>
		<th class="data-name" style="background:#dedede;">Nombre y Apellido</th>
		
		</tr>
		</thead>
		<tbody>
		<?php
		$sql2 = "SELECT j.DNIjugador, j.nombreApellido, je.puesto, je.nivel, je.respEntrenador, SUBSTRING(j.fechaNacimiento,1,4) as anio
                        FROM jugador_equipo je 
                        inner join jugador j on je.DNIjugador = j.DNIjugador
                        inner join inscriptos i on i.DNIjugador = j.DNIjugador
                        where i.aprobado = '1'
                        and idequipo = $idequipo
						and i.idtorneo = (select idtorneo from torneo where estado = 'A')
						order by nombreApellido;";
		$result2 = $mysqli->query($sql2);

		while ($row2 = $result2->fetch_assoc()) {	
		?>
		
		<tr class="odd sp-row-no-0">
		<td class="data-name" style="text-align: left;font-size: 14px;<?php if($row2['respEntrenador']=='1'){echo "color:#00a69c;";}?>"><?php echo($row2['nombreApellido']);if($row2['respEntrenador']=='1'){echo " (Cap.)";}?></td>
		</tr>
		<?php
		}
		?>
		<tr><th class="data-name"></th></tr>
		</tbody>
		</table>
	</div>
</div>

<p>&nbsp;</p>
<!--
<div class="sp-template sp-template-league-table">
	<h4 class="sp-table-caption">Fixture de '<?php echo($equipoDesc);?>'</h4>
	<div class="sp-table-wrapper">
		<table id="tbfixture" class="sp-league-table sp-data-table sp-sortable-table sp-scrollable-table sp-paginated-table" data-sp-rows="10">
		<thead>
		<tr><th class="data-name"></th></tr>
		<tr>
		<th class="data-name">Equipo 1</th>
		<th class="data-name">Resultado</th>
		<th class="data-name">Equipo 2</th>
		<th class="data-name">Resultado</th>
		<th class="data-name">D&iacute;a</th>
		<th class="data-name">Hora</th>
		<th class="data-name">Cancha</th>
		<th class="data-name">Observaciones</th>
		</tr>
		</thead>
		<tbody>
		<?php
		$sql4 = "select f.idfixture, f.equipo1, e1.equipoDesc as eDesc1, f.resultado1, f.equipo2, e2.equipoDesc as eDesc2, f.resultado2,
			date(fecha) as dia, TIME_FORMAT(fecha, '%H:%i') as hora, f.zona1, f.zona2, f.cancha, f.observaciones
			FROM fixture f
			inner join equipo e1
			on f.equipo1 = e1.idequipo
			inner join equipo e2
			on f.equipo2 = e2.idequipo
			where f.idtorneo = (select idtorneo from torneo where estado='A')
			and (f.equipo1 = $idequipo or f.equipo2= $idequipo)			
            order by fecha;";
		$result4 = $mysqli->query($sql4);

		while ($row4 = $result4->fetch_assoc()) {	
		?>
		<tr class="odd sp-row-no-0">
		<td class="data-name"><a href="equipo.php?idequipo=<?php echo $row4['equipo1']?>"><?php echo($row4['eDesc1']);?></a></td>
		<td><input type="text" id="Res1" size="2" style="text-align: center;" value="<?php echo $row4['resultado1']?>" disabled></td>
		<td class="data-name"><a href="equipo.php?idequipo=<?php echo $row4['equipo2']?>"><?php echo($row4['eDesc2']);?></a></td>
		<td><input type="text" id="Res1" size="2" style="text-align: center;" value="<?php echo $row4['resultado2']?>" disabled></td>
		<td class="data-g"><?php echo($row4['dia']);?></td>
		<td class="data-g"><?php echo($row4['hora']);?></td>
		<td class="data-g"><?php echo($row4['cancha']);?></td>
		<td class="data-g"><?php echo($row4['observaciones']);?></td>
		</tr>
		<?php
		}
		?>
		<tr><th class="data-name"></th></tr>
		</tbody>
		</table>
	</div>
</div>
-->
</div>
<p>&nbsp;</p>

			</div><!-- .entry-content -->
</article><!-- #post-## -->

				
			
		</main><!-- #main -->
	</div><!-- #primary -->

<div id="secondary" class="widget-area widget-area-right" role="complementary">
    <aside id="ngg-images-2" class="widget ngg_images"><div class="hslice" id="ngg-webslice">
	
	<section>
		<img class="img" style="width:100%" src="/sponsorsimg/conin.jpeg">
  	</section>

</div></aside><div class="sp-widget-align-none"><aside id="sportspress-event-list-3" class="widget widget_sportspress widget_sp_event_list"><div class="sp-template sp-template-event-list">

	</div>
</aside></div></div><!-- #secondary -->
			<style type="text/css">
			.sp-footer-sponsors {
				background: #f4f4f4;
				color: #363f48;
			}
			.sp-footer-sponsors .sp-sponsors .sp-sponsors-title {
				color: #363f48;
			}
			</style>
			<div class="sp-footer-sponsors">
				<div class="sportspress">	
				<?php include 'sponsors.php'; ?>

				</div>			
			</div>
			
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-area">
			<div id="quaternary" class="footer-widgets" role="complementary">
		
		<div class="footer-widget-region">
									
		<aside id="awesomeweatherwidget-2" class="widget widget_awesomeweatherwidget">
		<h3 class="widget-title">El Clima en la cancha</h3>
		<div id="awesome-weather-bella-vista-buenos-aires" class="awesome-weather-wrap awecf awe_wide awe_custom awe_with_stats awe-code-701 awe-desc-niebla awe-preset-atmosphere darken" style=" color: #ffffff;   ">
			
		<div id="cont_321e0f7f5561f9896a0c2aabd859cb3c"><script type="text/javascript" async src="https://www.meteored.com.ar/wid_loader/321e0f7f5561f9896a0c2aabd859cb3c"></script></div>
			
		</aside>
		</div>
		
		
	<div class="footer-widget-region">
		<div class="sp-widget-align-none">
		
		
		<aside id="sportspress-facebook-2" class="widget widget_sportspress widget_sp_facebook">
									
	<h3 class="widget-title">Nuestro Facebook</h3>	
	<div class="sp-template sp-template-facebook">
		<div class="sp-facebook">
		<iframe class="" name="f3843f4291321de" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:page Facebook Social Plugin" style="border: medium none; visibility: visible; width: 300px; height: 300px;" src="https://www.facebook.com/v2.8/plugins/page.php?adapt_container_width=true&app_id=818713328266556&channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FXBwzv5Yrm_1.js%3Fversion%3D42%23cb%3Df11e782fa4acbaa%26domain%3Dwww.campeonatoinfantildefutbol.com.ar%26origin%3Dhttp%253A%252F%252Fwww.campeonatoinfantildefutbol.com.ar%252Ff8c41713042498%26relation%3Dparent.parent&container_width=300&hide_cover=false&href=https%3A%2F%2Fwww.facebook.com%2Frevistaregatas%2F&locale=en_US&sdk=joey&show_facepile=true&small_header=false&tabs=timeline" frameborder="0">
		</iframe>
		</div>
	</div>
	</aside>
	
	
	
	</div></div>
									
							</div>
		</div><!-- .footer-area -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<p>&nbsp;</p>

<div id="dialog" title="Atenci&oacute;n">
Cargando...	
</div>

</body>
