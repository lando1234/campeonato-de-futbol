<?php

include ('fpdf/fpdf.php');
require_once "../codigo/connr.php";
$dni = $_GET["dni"];

	$sql3 ="select nombreApellido from responsable_juvenil where respDNI = '$dni'";
	$result3 = $mysqli->query($sql3);
	$row3 = $result3->fetch_assoc();
	$nombreResp=$row3['nombreApellido'];

	$sql2 ="select sum(monto) as pago 
	from pagos where 
	respDNI = '$dni' 
	and idtorneo = (select idtorneo from torneo where estado='A');";
	$result2 = $mysqli->query($sql2);
	$row2 = $result2->fetch_assoc();
	$pago=$row2['pago'];
	
	$sql = "SELECT rej.respDNI, e.equipoDesc, e.idcategoria, c.cant, rej.monto, rej.aprobado
			FROM responsable_equipo_juvenil rej
			inner join equipo_juvenil e
			on rej.idequipo = e.idequipo
			left join
			(select idequipo, count(*) cant
				from jugador_equipo_juvenil
				group by idequipo
			)c
			on c.idequipo = rej.idequipo
			where rej.respDNI='$dni'
			and idtorneo =(select idtorneo from torneo where estado='A')";

	$result = $mysqli->query($sql);

$pdf = new FPDF();
$pdf->AddPage();

$logo = "images/banner1000x148.jpg";
$pdf->Image($logo,10,8,180);
$pdf->Ln(30);
$pdf->SetFont('Arial','',16);
$pdf->Write(5,utf8_decode("   - 49° Campeonato Infantil de Fútbol del Club Regatas Bella Vista -"));
$pdf->Ln(10);
$pdf->SetFont('Arial','',12);
$pdf->Write(5,utf8_decode("Planilla de Control de Pagos"));
$pdf->Ln(10);
$pdf->SetFont('Arial','',10);
$pdf->Write(5,utf8_decode("Responsable de la/s inscripción/es: "));
$pdf->SetFont('Arial','B',10);
$pdf->Write(5,utf8_decode($nombreResp));
$pdf->SetFont('Arial','',10);
$pdf->Write(5,utf8_decode("   -  DNI: "));
$pdf->SetFont('Arial','B',10);
$pdf->Write(5,utf8_decode($dni));
$pdf->SetFont('Arial','',10);

$pdf->Ln(10);

$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(200,200,200);
$pdf->Cell(60,6,"Nombre Equipo",1,0,'C','true');
$pdf->Cell(30,6,"Categoria",1,0,'C','true');
$pdf->Cell(30,6,"Cant. Jugadores",1,0,'C','true');
$pdf->Cell(20,6,"Monto",1,0,'C','true');
$pdf->Ln();
$total = 0;
while ($row = $result->fetch_assoc()) {
	$total = $total+$row['monto'];

	$pdf->SetFont('Arial','',8);	
	$pdf->Cell(60,6,utf8_decode($row['equipoDesc']),1);
	$pdf->Cell(30,6,$row['idcategoria'],1,0,'C');
	$pdf->Cell(30,6,$row['cant'],1,0,'C');
	$pdf->Cell(20,6,"$ ".$row['monto'],1,0,'R');
	$pdf->Ln();
}
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(120,6,"Total   ",1,0,'R','true');
	$pdf->Cell(20,6,"$ ".$total.".00",1,0,'R','true');
	$pdf->SetFont('Arial','',10);
$pdf->Ln(10);

//Pagos
$pdf->SetFont('Arial','B',10);
$pdf->Write(5,utf8_decode("Pagos realizados:"));
$pdf->SetFont('Arial','',10);
$pdf->Ln(5);

$sql4 = "SELECT fechaPago, lugarPago, recibo, monto FROM pagos_juvenil where respDNI = '$dni' and idtorneo = (select idtorneo from torneo where estado='A');";
$result4 = $mysqli->query($sql4);

$pagado = 0;
if ($result4->num_rows>0){
	$pdf->SetFont('Arial','B',10);
	$pdf->SetFillColor(200,200,200);
	$pdf->Cell(30,6,"Fecha",1,0,'C','true');
	$pdf->Cell(60,6,"Lugar",1,0,'C','true');
	$pdf->Cell(30,6,"Recibo",1,0,'C','true');
	$pdf->Cell(20,6,"Monto",1,0,'C','true');
	$pdf->Ln();

while ($row4 = $result4->fetch_assoc()) {
	$pagado = $pagado+$row4['monto'];
	
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(30,6,$row4['fechaPago'],1,0,'C');
	$pdf->Cell(60,6,utf8_decode($row4['lugarPago']),1);
	$pdf->Cell(30,6,$row4['recibo'],1,0,'C');
	$pdf->Cell(20,6,"$ ".$row4['monto'].".00",1,0,'R');	
	$pdf->Ln();
}
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(120,6,"Total   ",1,0,'R','true');
	$pdf->Cell(20,6,"$ ".$pagado.".00",1,0,'R','true');
	$pdf->SetFont('Arial','',10);

}else{
	$pdf->Write(5,utf8_decode("Aún no se registran pagos."));
}

$deuda=$total-$pagado;

$pdf->Ln(15);
$pdf->SetFont('Arial','B',10);
$pdf->Write(5,utf8_decode("Importe final:       "));
$pdf->SetFillColor(200,200,200);
$pdf->Cell(30,6,"$ ".$deuda.".00",1,0,'C','true');
$pdf->SetFont('Arial','',10);
$pdf->Ln(10);

$pdf->SetFont('Arial','',8);
$pdf->Write(5,utf8_decode("La inscripción no estará finalizada hasta tanto no cancele el pago a través de pagomiscuentas.com (el pago a travez de pagomiscuentas tiene que ser con el mismo dni del responsable de la inscripcion), o de los puntos de cobro detallados."));
$pdf->Ln();
$pdf->Write(5,utf8_decode("En caso de hacerlo por pagomiscuentas, es necesario que el documento del responsable de la inscripción coincida con el del"));
$pdf->Write(5,utf8_decode("titular de la cuenta bancaria, a fin de poder identificarlo."));
$pdf->Ln();
$pdf->Write(5,utf8_decode("De no ser asi, rogamos nos envie el pago a "));
$pdf->SetFont('Arial','B',8);
$pdf->Write(5,utf8_decode("secretaria@regatasbellavista.com.ar"));
$pdf->SetFont('Arial','',8);
$pdf->Write(5,utf8_decode(", informando allí quien es el responsable."));
$pdf->Ln(10);
$pdf->SetFont('Arial','B',8);
$pdf->Write(5,utf8_decode("Atención: LOS PAGOS REALIZADOS DESPUES DEL 28/9 SERAN ACTUALIZADOS, INDEPENDIENTEMENTE DE LA FECHA EN QUE SE HAYA REALIZADO LA INSCRIPCION."));
$pdf->Ln(10);

$pdf->SetFont('Arial','B',10);
$pdf->Write(5,utf8_decode("Cupones:"));
$pdf->Ln(8);

$pdf->SetFont('Arial','',8);
$pdf->Cell(5,6,"",0,0,'C');
$pdf->Cell(55,6,"Club Regatas",'LTR',0,'C','true');
$pdf->Cell(5,6,"",0,0,'C');
$pdf->Cell(55,6,"Responsable ",'LTR',0,'C','true');
$pdf->Cell(5,6,"",0,0,'C');
$pdf->Cell(55,6,"Punto de Venta ",'LTR',0,'C','true');
$pdf->Ln();
$pdf->SetFont('Arial','B',12);
$pdf->Cell(5,12,"",0,0,'C');
$pdf->Cell(55,12,"Nro: ".$dni,'LR',0,'C');
$pdf->Cell(5,12,"",0,0,'C');
$pdf->Cell(55,12,"Nro: ".$dni,'LR',0,'C');
$pdf->Cell(5,12,"",0,0,'C');
$pdf->Cell(55,12,"Nro: ".$dni,'LR',0,'C');
$pdf->Ln();
$pdf->SetFont('Arial','',8);
$pdf->Cell(5,3,"",0,0,'C');
$pdf->Cell(55,3,utf8_decode("49° Campeonato Infantil de Fútbol"),'LR',0,'C');
$pdf->Cell(5,3,"",0,0,'C');
$pdf->Cell(55,3,utf8_decode("49° Campeonato Infantil de Fútbol"),'LR',0,'C');
$pdf->Cell(5,3,"",0,0,'C');
$pdf->Cell(55,3,utf8_decode("49° Campeonato Infantil de Fútbol"),'LR',0,'C');
$pdf->Ln();
$pdf->Cell(5,3,"",0,0,'C');
$pdf->Cell(55,3,"del Club Regatas Bella Vista",'LR',0,'C');
$pdf->Cell(5,3,"",0,0,'C');
$pdf->Cell(55,3,"del Club Regatas Bella Vista",'LR',0,'C');
$pdf->Cell(5,3,"",0,0,'C');
$pdf->Cell(55,3,"del Club Regatas Bella Vista",'LR',0,'C');
$pdf->Ln();
$pdf->Cell(5,3,"",0,0,'C');
$pdf->Cell(55,3,"2019",'LR',0,'C');
$pdf->Cell(5,3,"",0,0,'C');
$pdf->Cell(55,3,"2019",'LR',0,'C');
$pdf->Cell(5,3,"",0,0,'C');
$pdf->Cell(55,3,"2019",'LR',0,'C');
$pdf->Ln();
$pdf->SetFont('Arial','B',12);
$pdf->Cell(5,12,"",0,0,'C');
$pdf->Cell(55,12,"Importe: $ ".$deuda.".00",'LR',0,'C');
$pdf->Cell(5,12,"",0,0,'C');
$pdf->Cell(55,12,"Importe: $ ".$deuda.".00",'LR',0,'C');
$pdf->Cell(5,12,"",0,0,'C');
$pdf->Cell(55,12,"Importe: $ ".$deuda.".00",'LR',0,'C');
$pdf->Ln();
$pdf->SetFont('Arial','',8);
$pdf->Cell(5,5,"",0,0,'C');
$pdf->Cell(55,5,utf8_decode("La inscripción no estará completa hasta"),'LR',0,'C','true');
$pdf->Cell(5,5,"",0,0,'C');
$pdf->Cell(55,5,utf8_decode("La inscripción no estará completa hasta"),'LR',0,'C','true');
$pdf->Cell(5,5,"",0,0,'C');
$pdf->Cell(55,5,utf8_decode("La inscripción no estará completa hasta"),'LR',0,'C','true');
$pdf->Ln();
$pdf->Cell(5,5,"",0,0,'C');
$pdf->Cell(55,5,utf8_decode("tanto no se formalice y acredite el pago."),'LRB',0,'C','true');
$pdf->Cell(5,5,"",0,0,'C');
$pdf->Cell(55,5,utf8_decode("tanto no se formalice y acredite el pago."),'LRB',0,'C','true');
$pdf->Cell(5,5,"",0,0,'C');
$pdf->Cell(55,5,utf8_decode("tanto no se formalice y acredite el pago."),'LRB',0,'C','true');
$pdf->Ln();


$pdf->Ln(15);
$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(200,200,200);
$pdf->Cell(185,6,utf8_decode("Lugares habilitados para el pago: Secretaría del club"),1,0,'C','true');

$pdf->Output();
exit;

?>
