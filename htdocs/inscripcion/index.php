<?php

header('Location:menu.php'); 
	
?>

<!DOCTYPE html>
<html lang="es-ES">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="xmlrpc.php">
	<title>Campeonato Infantil de F&uacute;tbol &#8211; Noviembre 2019 &#8211; Club de Regatas Bella Vista</title>
	<link rel='dns-prefetch' href='//fonts.googleapis.com' />
	
	<link rel='stylesheet' id='style-css'  href='../css/style.css' type='text/css' media='all' />
	<link rel='stylesheet' id='framework-css'  href='../css/framework.css' type='text/css' media='all' />
	<link rel='stylesheet' id='agregado-css'  href='../css/agregado.css' type='text/css' media='all' />
	<link rel='stylesheet' id='style002-css'  href='../css/style002.css' type='text/css' media='all' />
	<link rel='stylesheet' id='sportspress'  href='../css/sportspress-sponsors.css' type='text/css' media='all' />
	
	<link rel="stylesheet" type="text/css" href="../css/themes/smoothness/jquery-ui-1.8.4.custom.css" />
	
	<script type="text/javascript" src="../codigo/jquery-1.8.2.js"></script>
	<script type="text/javascript" src="../codigo/jquery-ui-1.9.0.custom.min.js"></script>
	
	<script type="text/javascript" src="codigo/index.js"></script>
	
</head>

<body class="home page-template-default page page-id-242 custom-background">
<div class="sp-header"></div>
<div id="page" class="hfeed site">
	

	<header id="masthead" class="site-header" role="banner">
				<div class="header-area header-area-has-search">
				<div class="site-branding site-branding-empty">
					<div class="site-identity"></div>
				</div><!-- .site-branding -->
												<div class="site-banner">
					<img class="site-banner-image" src="images/banner1000x148.jpg" alt="Club de Regatas Bella Vista">
				</div><!-- .site-banner -->
												<div class="site-menu">
	
    <nav id="site-navigation" class="main-navigation" role="navigation">
					
	
    <div class="menuIz" style="width: 85%;">
            <span id="btregalmento" class="menug">Reglamento</span>
            <span id="btcontacto" class="menug">Contacto</span>
            <span id="btpago" class="menug">Gesti&oacute;n Pagos</span>
    </div>

    <div class="menuDe"style="width: 15%;"
        <span id="btautorizados" class="menug"  style="padding: 0.625em 0;">Usuarios Autorizados</span>
    </div>

	</nav>
    
    	
				</div>
						</div>
		</header><!-- #masthead -->

	<div id="content" class="site-content">
	
	<div id="primary" class="content-area-full-width">
		<main id="main" class="site-main" role="main">
			
				
<article id="post-242" class="post-242 page type-page status-publish hentry">
	<header class="entry-header">
				
		<h1 class="entry-title">Inscripci&oacute;n</h1>	</header><!-- .entry-header -->

	<div class="entry-content">
		
		Bienvenidos al 49&deg; Campeonato Infantil de F&uacute;tbol del Club Regatas Bella Vista. Orgullosos de organizar uno 
		de los eventos deportivos m&aacute;s importantes del a&ntilde;o con el que desde 1971 damos cierre a las actividades de 
		rugby y hockey, invitamos a toda la comunidad, sean socios o no, a concurrir a nuestro club y participar del torneo, 
		tanto compitiendo como acompa&ntilde;ando a sus hijos/as. Hemos modernizado el sistema de inscripci&oacute;n al campeonato, 
		haci&eacute;ndolo m&aacute;s sencillo y completo, esperando que sea de su satisfacci&oacute;n y encontr&aacute;ndonos 
		a su disposici&oacute;n para recibir todo tipo de sugerencia. Nuevamente, con el deseo que disfruten en plenitud 
		el campeonato, les damos la bienvenida.-
		

			</div><!-- .entry-content -->
</article><!-- #post-## -->

				
			
		</main><!-- #main -->
	</div><!-- #primary -->
	
		<div id="primary" class="content-area content-area-right-sidebar">
		<main id="main" class="site-main" role="main">
			
				
<article id="post-242" class="post-242 page type-page status-publish hentry">
	<header class="entry-header">
				
			</header><!-- .entry-header -->

	<div class="entry-content">
		
<div class="sportspress sp-widget-align-none"><div class="sp-template sp-template-league-table">
	<h4 class="sp-table-caption">Seleccione el grupo</h4>
	<div class="sp-table-wrapper">
	<table class="sp-league-table sp-data-table sp-sortable-table sp-scrollable-table sp-paginated-table" data-sp-rows="10"><thead>
	<tr>
	
	<th class="data-name"></th>
	<th class="data-pj"></th>
	<tbody>
	<tr class="odd sp-row-no-0">
		<td class="data-name"><a href="infantil_r.php"><input class="boton_categoria bcategoriaI" value="Infantil" type="button"></a></td>
	<td class="data-pj">Jugadores nacidos entre el 2005 y 2014. <br>
						Socios: $1650 - No Socios: $2100 <!--span style="color:red;"><b>Inscripci&oacuten Cerrada.</b></span--></td>
	</tr>
	<tr class="odd sp-row-no-0">
		<td class="data-name"><a href="femenino_r.php"><input class="boton_categoria bcategoriaF" value="Femenino" type="button"></a></td>
	<td class="data-pj">Chicas del Nivel Primario y Secundario. <br>
						Socias: $1650 - No Socias: $2100 <!--span style="color:red;"><b>Inscripci&oacuten Cerrada.</b></span--></td>
	</tr>
	<tr class="odd sp-row-no-0">
		<td class="data-name"><a href="juvenil_r.php"><input class="boton_categoria bcategoriaM" value="Juvenil" type="button"></a></td>
	<td class="data-pj">Por equipos. Cupos Limitados <br>
						Por Equipo: $18000<!--span style="color:red;"><b>Inscripci&oacuten Cerrada.</b></span--></td>
	 <!--Correspondientes a los jugadores de entre 15 y 31 a&ntilde;os.-->
	</tr>
	
	</tbody>
	</table>
	</div></div>
</div>

			</div><!-- .entry-content -->
</article><!-- #post-## -->

				
			
		</main><!-- #main -->
	</div><!-- #primary -->

<div id="secondary" class="widget-area widget-area-right" role="complementary">
    <aside id="ngg-images-2" class="widget ngg_images"><div class="hslice" id="ngg-webslice"><h1 class="entry-title widget-title">Torneos anteriores</h1><div class="ngg-widget entry-content">
            <img title="Cat. A - 2017"
                 alt="demoA"
                 src="images/thumbs_demoA.jpg"
                 width="148"
                 height="111"
            />
            <img title="Cat. A - 2017"
                 alt="demoC"
                 src="images/thumbs_demoC.jpg"
                 width="148"
                 height="111"
            />
            <img title="Cat. A - 2017"
                 alt="demoE"
                 src="images/thumbs_demoE.jpg"
                 width="148"
                 height="111"
            />
            <img title="Cat. A - 2017"
                 alt="demoF"
                 src="images/thumbs_demoF.jpg"
                 width="148"
                 height="111"
            />
    </div>

</div></aside>
<div class="sp-widget-align-none">
<aside id="sportspress-event-list-3" class="widget widget_sportspress widget_sp_event_list">

</aside></div></div><!-- #secondary -->
			<style type="text/css">
			.sp-footer-sponsors {
				background: #f4f4f4;
				color: #363f48;
			}
			.sp-footer-sponsors .sp-sponsors .sp-sponsors-title {
				color: #363f48;
			}
			</style>
			<div class="sp-footer-sponsors">
				<div class="sportspress">	
				<?php include '../sponsors.php'; ?>

				</div>			
			</div>
			
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-area">
			<div id="quaternary" class="footer-widgets" role="complementary">
		
		<div class="footer-widget-region">
									
		<aside id="awesomeweatherwidget-2" class="widget widget_awesomeweatherwidget">
		<h3 class="widget-title">El Clima en la cancha</h3>
		<div id="awesome-weather-bella-vista-buenos-aires" class="awesome-weather-wrap awecf awe_wide awe_custom awe_with_stats awe-code-701 awe-desc-niebla awe-preset-atmosphere darken" style=" color: #ffffff;   ">
			
		<div id="cont_321e0f7f5561f9896a0c2aabd859cb3c"><script type="text/javascript" async src="https://www.meteored.com.ar/wid_loader/321e0f7f5561f9896a0c2aabd859cb3c"></script></div>
			
		</aside>
		</div>
		
		
	<div class="footer-widget-region">
		<div class="sp-widget-align-none">
		
		
		<aside id="sportspress-facebook-2" class="widget widget_sportspress widget_sp_facebook">
									
	<h3 class="widget-title">Nuestro Facebook</h3>	
	<div class="sp-template sp-template-facebook">
		<div class="sp-facebook">
		<iframe class="" name="f3843f4291321de" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:page Facebook Social Plugin" style="border: medium none; visibility: visible; width: 300px; height: 300px;" src="https://www.facebook.com/v2.8/plugins/page.php?adapt_container_width=true&app_id=818713328266556&channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FXBwzv5Yrm_1.js%3Fversion%3D42%23cb%3Df11e782fa4acbaa%26domain%3Dwww.campeonatoinfantildefutbol.com.ar%26origin%3Dhttp%253A%252F%252Fwww.campeonatoinfantildefutbol.com.ar%252Ff8c41713042498%26relation%3Dparent.parent&container_width=300&hide_cover=false&href=https%3A%2F%2Fwww.facebook.com%2Frevistaregatas%2F&locale=en_US&sdk=joey&show_facepile=true&small_header=false&tabs=timeline" frameborder="0">
		</iframe>
		</div>
	</div>
	</aside>
	
	
	
	</div></div>
									
							</div>
		</div><!-- .footer-area -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<p>&nbsp;</p>

<div id="dialog" title="Atenci&oacute;n">
Cargando...	
</div>

<div id="dialog2" title="Control de Pagos">
Cargando...	
</div>

</body>
