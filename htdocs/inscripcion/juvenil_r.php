﻿<?php 
	//header('Location: /inscripcionJuvenilCerro.php');
?>
<!DOCTYPE html>
<html lang="es-ES">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="xmlrpc.php">
	<title>Campeonato Infantil de F&uacute;tbol &#8211; Noviembre 2017 &#8211; Club de Regatas Bella Vista</title>
	<link rel='dns-prefetch' href='//fonts.googleapis.com' />
	
	<link rel='stylesheet' id='style-css'  href='../css/style.css' type='text/css' media='all' />
	<link rel='stylesheet' id='framework-css'  href='../css/framework.css' type='text/css' media='all' />
	<link rel='stylesheet' id='style002-css'  href='../css/style002.css' type='text/css' media='all' />
	<link rel='stylesheet' id='sportspress'  href='../css/sportspress-sponsors.css' type='text/css' media='all' />
	<link rel='stylesheet' id='agregado-css'  href='../css/agregado.css' type='text/css' media='all' />
	
	<link rel="stylesheet" type="text/css" href="../css/themes/smoothness/jquery-ui-1.8.4.custom.css" />
	
	<script type="text/javascript" src="../codigo/jquery-1.8.2.js"></script>
	<script type="text/javascript" src="../codigo/jquery-ui-1.9.0.custom.min.js"></script>
		
	<script type="text/javascript" src="codigo/juvenil.js"></script>
	
</head>

<body class="home page-template-default page page-id-242 custom-background">

<div class="sp-header"></div>
<div id="page" class="hfeed site">


	<header id="masthead" class="site-header" role="banner">
				<div class="header-area header-area-has-search">
				<div class="site-branding site-branding-empty">
					<div class="site-identity"></div>
				</div><!-- .site-branding -->
												<div class="site-banner">
					<img class="site-banner-image" src="images/banner1000x148.jpg" alt="Club de Regatas Bella Vista">
				</div><!-- .site-banner -->
	<div class="site-menu">
	
	<nav id="site-navigation" class="main-navigation" role="navigation">
    <div class="menuIz" style="width: 85%;">
            <span id="btindex" class="menug">Inicio</span>
            <span id="btcontacto" class="menug">Contacto</span>
    </div>

    <div class="menuDe"style="width: 15%;"
        <span id="btautorizados" class="menug"  style="padding: 0.625em 0;">Usuarios Autorizados</span>
    </div>
	</nav>
    
		</div>
						</div>
		</header><!-- #masthead -->

	<div id="content" class="site-content">
		
	<div id="primary" class="content-area-full-width content-area-right-sidebar">
		<main id="main" class="site-main" role="main">

			
				
<article id="post-242" class="post-242 page type-page status-publish hentry">
	<header class="entry-header">
				
		<h1 class="entry-title">Inscripci&oacute;n F&uacute;tbol Juvenil</h1>	</header><!-- .entry-header -->

	<div class="entry-content">
		<p>Aqu&iacute; se podr&aacute;n inscribir las categor&iacute;as F, G y H. Correspondientes a los jugadores de entre 15 y 31 a&ntilde;os.
		<br>
		<br>
		Para equipos Juveniles, el costo del campeonato es de $ 18.000.-, y cada equipo podr&aacute; contar con hasta 15 jugadores, y un m&iacute;nimo de 3 socios
		</p>
<div class="sportspress sp-widget-align-none">
	<div class="sp-template sp-template-league-table" >
	
	<h4 class="sp-table-caption" style="border-top: 8px solid #263c76;">Responsable</h4>
	<div class="sp-table-wrapper">
	<table>
	<tr><td colspan='2'></td></tr>
	<tbody>
					<tr >
						<td class="formD">DNI responsable:</td>
						<td class="formC"><input type="text" maxlength="10" id="dniResponsable" name="dniResponsable" size="43" /> *
						<input id="botonPago" name="botonPago" type="button" value="Contol de Pagos" hidden="hidden"/>
						</td>
					</tr>
					<tr>
						<td class="formD">Nombre y Apellido:</td>
						<td class="formC"><input type="text" maxlength="100" id="nombreApellidoResponsable"
							name="nombreApellidoResponsable" size="43" /> *
						</td>
					</tr>
					<tr>
						<td class="formD">Telefono:</td>
						<td class="formC"><input type="text" maxlength="30" id="telefonoResponsable"
							name="telefonoResponsable" size="43" /> *
						</td>
					</tr>
					<tr>
						<td class="formD">Mail:</td>
						<td class="formC"><input type="text" maxlength="50" id="mailResponsable"
							name="mailResponsable" size="43" /> *
						</td>
					</tr>
					<tr>
						<td class="formD">Password:</td>
						<td class="formC"><input type="text" maxlength="50" id="passResponsable"
							name="passResponsable" size="43"/> *
						</td>
					</tr>
					
	<tr><td colspan='2'></td></tr>
	</tbody>
	</table>
	</div>
	
    <input type="hidden" id="idequipo" name="idequipo">
	
	
	<h4 class="sp-table-caption" style="border-top: 8px solid #263c76;">Equipo</h4>
	<div class="sp-table-wrapper">
	<table>
	<th class="data-pj"></th>
	<tbody>
					<tr>
						<td class="formD">Nombre:</td>
						<td class="formC"><input type="text" maxlength="70" id="NombreEquipo"
							name="NombreEquipo" size="43" /> *
						</td>
						
					</tr>
					<tr>
						<td class="formD">Categor&iacute;a:</td>
						<td class="formC"><select id="categoria" name="categoria">
							<option value="0"> Seleccione </option>
							<option value="F"> Cat. F -> 2003 - 2004 </option>
							<option value="G"> Cat. G -> 2000 - 2002 </option>
							<option value="H"> Cat. H -> 1988 - 1999 </option>
						</select> *
						</td>
					</tr>
					
	<th class="data-pj"></th>
	</tbody>
	</table>
	</div>

	
	<h4 class="sp-table-caption" style="border-top: 8px solid #263c76;">Datos de los Jugadores</h4>
	<div class="sp-table-wrapper">
	<table>
	
	
	<thead>
	<th class="data-rank">Nro</th>
	<th class="data-name">DNI *</th>
	<th class="data-name">Nombre y Apellido *</th>
	<th class="data-name">F. Nacimiento<br>(dd/mm/aaaa) *</th>
	<th class="data-name">Telefono *</th>
	<th class="data-name">Mail *</th>
	<th class="data-name">Socio *</th>
	</thead>
	<tbody>
	<?php
	for ($i = 1; $i <= 15; $i++) {
	?>	
	
	<tr>
		<td><?php echo $i; ?>. </td>
		<td><input type="text" maxlength="10" id="dni_<?php echo $i; ?>" name="dni_<?php echo $i; ?>"	size="8" class="dni"/></td>
		<td><input type="text" maxlength="100" id="nombApe_<?php echo $i; ?>" name="nombApe_<?php echo $i; ?>"	size="20" /></td>
		<td><input type="text" maxlength="10" id="fnacimiento_<?php echo $i; ?>" name="fnacimiento_<?php echo $i; ?>"	size="10" /></td>
		<td><input type="text" maxlength="20" id="tel_<?php echo $i; ?>" name="tel_<?php echo $i; ?>"	size="10" /></td>
		<td><input type="text" maxlength="100" id="mail_<?php echo $i; ?>" name="mail_<?php echo $i; ?>"	size="20" /></td>
		<td><select class="classsocio" id="socio_<?php echo $i; ?>" name="socio_<?php echo $i; ?>"><option value="0">NO</option><option value="1">SI</option></td>
								
	</tr>
	
	<?php }	?>	
	<tr><td></td>
	<td  colspan='6' style='font-size: 12px; padding: 0 15px; text-align: left;'>* Campos obligatorios.</td></tr>
	<tr><td colspan='7'></td></tr>
	</tbody>
	</table>
	</div>
	
	
	</div>
	
	<p><input type="checkbox" id="checkVal" value=""	 /> 
	Acepto todas las normas y reglamentos dispuestos para su realizaci&oacute;n y dejo constancia de que los datos consignados son correctos.</p>
	<div id="div_inscribir">		
		<input id="botonInscribir" name="botonInscribir" type="button" value="Guardar" />
	</div>	
	
</div>

<div id="div_resultado"></div>
<input id="botonPago2" name="botonPago2" type="button" value="Contol de Pagos" hidden="hidden"/>

			</div><!-- .entry-content -->
</article><!-- #post-## -->
			
		</main><!-- #main -->
	</div><!-- #primary -->

			<div class="sp-footer-sponsors">
				<div class="sportspress">	
				<?php include '../sponsors.php'; ?>

				</div>			
			</div>
			
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-area">
			<div id="quaternary" class="footer-widgets" role="complementary">
		
		<div class="footer-widget-region">
									
		<aside id="awesomeweatherwidget-2" class="widget widget_awesomeweatherwidget">
		<h3 class="widget-title">El Clima en la cancha</h3>
		<div id="awesome-weather-bella-vista-buenos-aires" class="awesome-weather-wrap awecf awe_wide awe_custom awe_with_stats awe-code-701 awe-desc-niebla awe-preset-atmosphere darken" style=" color: #ffffff;   ">
			
		<div id="cont_321e0f7f5561f9896a0c2aabd859cb3c"><script type="text/javascript" async src="https://www.meteored.com.ar/wid_loader/321e0f7f5561f9896a0c2aabd859cb3c"></script></div>
			
		</aside>
		</div>
		
		
	<div class="footer-widget-region">
		<div class="sp-widget-align-none">
		
		
		<aside id="sportspress-facebook-2" class="widget widget_sportspress widget_sp_facebook">
									
	<h3 class="widget-title">Nuestro Facebook</h3>	
	<div class="sp-template sp-template-facebook">
		<div class="sp-facebook">
		<iframe class="" name="f3843f4291321de" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:page Facebook Social Plugin" style="border: medium none; visibility: visible; width: 300px; height: 300px;" src="https://www.facebook.com/v2.8/plugins/page.php?adapt_container_width=true&app_id=818713328266556&channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FXBwzv5Yrm_1.js%3Fversion%3D42%23cb%3Df11e782fa4acbaa%26domain%3Dwww.campeonatoinfantildefutbol.com.ar%26origin%3Dhttp%253A%252F%252Fwww.campeonatoinfantildefutbol.com.ar%252Ff8c41713042498%26relation%3Dparent.parent&container_width=300&hide_cover=false&href=https%3A%2F%2Fwww.facebook.com%2Frevistaregatas%2F&locale=en_US&sdk=joey&show_facepile=true&small_header=false&tabs=timeline" frameborder="0">
		</iframe>
		</div>
	</div>
	</aside>
	
	</div></div>
									
							</div>
		</div><!-- .footer-area -->
	</footer><!-- #colophon -->
</div><!-- #page -->


<p>&nbsp;</p>

<div id="dialog" title="Atenci&oacute;n">
Cargando...	
</div>

<div id="dialog2" title="Control de Pagos">
Cargando...	
</div>

<div id="dialog3" title="Control de Password">
    Para poder editar el equipo, Ud. necesita ingresar la clave (password) que fue solicitada en la creaci&oacute;n del equipo.
    <br><br>
    <table>
        <tr>
            <td class="formD">Password:</td>
            <td class="formC"><input type="text" maxlength="50" id="passResponsable2" name="passResponsable2" size="20" />
        </tr>
    </table>
    <div id="div_inscribir">		
		<input id="botonPass" name="botonPass" type="button" value="Aceptar" />
        <input id="botonCanc" name="botonCanc" type="button" value="Cancelar" />
	</div>	
</div>
<div id="dialog4" title="Aviso Importante">
	<div class="modal-box">	
		<div class="modal__header">
				<span id="dialog3Cerrar" class="modal__cerrar"></span>
		</div>
		<div class="modal_body">
			<h5 class="modal__titulo">
				Capitanes y Jugadores de los equipos de las categor&iacute;as F y G. Les informamos que, debido a las actividades de rugby del club,
				que este año terminan sus torneos a mediados de noviembre, el campeonato en estas 2 categor&iacute;as comenzara un poco mas tarde y 
				se desarrollara de manera diferente. Ambas categor&iacute;as comenzaran sus primeros partidos el lunes 18/11 (feriado), <u>jugando una fecha doble</u>
				y deberán en el resto de las fechas <u>jugar 2 fechas doles m&aacute;s</u> para finalizar el campeonato jugando el mismo d&iacute;a todas las 
				finales juntas. Las fechas dobles seran siempre en turnos Mañana y Tarde. La categor&iacute;a H comienza el campeonato normalmente.
				Agradeciendo la comprensión, los esperamos en la cancha!
			</h5>
		</div>
	</div>
</div>

</body>
