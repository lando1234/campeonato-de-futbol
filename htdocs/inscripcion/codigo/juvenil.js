$(document).ready(function(){

    $('#botonInscribir').attr("disabled", true); 

        $("#dialog").dialog({
                autoOpen: false,
                modal: true,
                width: 450,
                height: 250,
                show: "show",
                hide: "hide"
        });
        
        $("#dialog").click(function() {
                $("#dialog").html("");
                $("#dialog").dialog("close");
            });
        
        $("#dialog2").dialog({
                autoOpen: false,
                modal: true,
                width: 750,
                show: "show",
                hide: "hide"
        });
        
        $("#dialog3").dialog({
                autoOpen: false,
                modal: true,
                width: 450,
                height: 250,
                show: "show",
                hide: "hide"
        });

        $("#dialog4").dialog({
            autoOpen: true,
            modal: true,
            width: 600,
            height:370,
            resizable:false,
            show: "show",
            hide: "hide"
    });

    $("#dniResponsable").keyup(function (){
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    $(".dni").keyup(function (){
                this.value = (this.value + '').replace(/[^0-9]/g, '');
    });

    function validarFormatoFecha(campo) {
        var RegExPattern = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
        if ((campo.match(RegExPattern)) && (campo!='')) {
            return true;
        } else {
            return false;
        }
    }
              
	$("#dniResponsable").change(function(){
		
		$("#nombreApellidoResponsable").val("");
		$("#telefonoResponsable").val("");
		$("#mailResponsable").val("");
		$("#passResponsable").val("");
		$("#NombreEquipo").val("");
		$("#categoria").val(0);
		$("#idequipo").val("");
		for(var i=1;i<16;i++){
				$("#dni_"+(i)).val("");
				$("#nombApe_"+(i)).val("");
				$("#fnacimiento_"+(i)).val("");
				$("#tel_"+(i)).val("");
				$("#mail_"+(i)).val("");
				//$("#socio_"+(i)).val(0);
		}
		$("#div_resultado").html("");
		$("#checkVal").prop('checked', false);
		$('#botonInscribir').attr("disabled", !$(this).prop('checked')); 	
		
		$('#botonPago').hide();
		$('#botonPago2').hide();
		
		 var id = $("#dniResponsable").val();
		 var data = 'dniR=' + id;

		  $.ajax({
                type: "GET",
                url: "codigo/buscarRespJuv.php",
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data) {
                        for (var i = 0; i < data.length; i++) { 
							$("#nombreApellidoResponsable").val(data[i].nombreApellido);
                            $("#telefonoResponsable").val(data[i].respTelefono);
                            $("#mailResponsable").val(data[i].respMail);
                        }
                    }
						if ($("#nombreApellidoResponsable").val().length!=0){
                            $('#botonPago').show();
						}
                        $('#passResponsable').attr("disabled", false);
                        $("#passIsSet").val("true");
                        
                } 
            });
			$.ajax({
                type: "GET",
                url: "codigo/buscarEquipoJuv.php",
                async:false,    
                cache:false,  
                data: data,
                dataType: 'json',
                success: function (data) {
					
                    if (data) {
                        for (var i = 0; i < data.length; i++) { 
							$("#NombreEquipo").val(data[i].equipoDesc);
							$("#categoria").val(data[i].idcategoria);
                            $("#idequipo").val(data[i].idequipo);
                        }
                    }
					
                } 
            });

			$.ajax({
                type: "GET",
                url: "codigo/buscarJugadoresJuv.php",
                async:false,    
                cache:false,  
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data) {
                        for (var i = 0; i < data.length; i++) { 
							$("#dni_"+(i+1)).val(data[i].DNIjugador);
							$("#nombApe_"+(i+1)).val(data[i].nombreApellido);
							feNac = data[i].fechaNacimiento;
							$("#fnacimiento_"+(i+1)).val(feNac.substring(8,10)+'/'+feNac.substring(5,7)+'/'+feNac.substring(0,4));
							$("#tel_"+(i+1)).val(data[i].telefono);
							$("#mail_"+(i+1)).val(data[i].email);
							$("#socio_"+(i+1)).val(data[i].socioClub);
                        }
                    }
					
                } 
            });  			
    });            

          
	$("#checkVal").change(function(){
			$('#botonInscribir').attr("disabled", !$(this).prop('checked')); 	
	});
    
	$("#categoria").change(function(){
			$(".error").remove();	
	});
	
		
    $("#botonInscribir").click(function(){
		
		//Validar formulario
		$(".error").remove();
        
        
        if( $("#dniResponsable").val() == ""){
			$("#dniResponsable").focus().after("<br><span class='error'>Debe ingresar el DNI.</span>");
			return false;
        }else if( Number($("#dniResponsable").val()) < 1000000){
            $("#dniResponsable").focus().after("<br><span class='error'>Debe ingresar un DNI v&aacute;lido.</span>");
            return false;
		}else if( $("#nombreApellidoResponsable").val().length < 3){
            $("#nombreApellidoResponsable").focus().after("<br><span class='error'>Debe ingresar su Nombre y Apellido.</span>");
            return false;
		}else if( $("#telefonoResponsable").val().length < 7){
            $("#telefonoResponsable").focus().after("<br><span class='error'>Debe ingresar un Tel&eacute;fono v&aacute;lido.</span>");
            return false;
		}else if( validateEmail($("#mailResponsable").val())=='false'){
            $("#mailResponsable").focus().after("<br><span class='error'>Debe ingresar un eMail v&aacute;lido.</span>");
            return false;
		}else if( $("#passIsSet").val()=="true" && $("#passResponsable").val().length <5) {
            $("#passResponsable").focus().after("<br><span class='error'>Debe ingresar una Password de al menos 5 caracteres.</span>");
            return false;
		}else if( $("#NombreEquipo").val().length < 3){
            $("#NombreEquipo").focus().after("<br><span class='error'>Debe ingresar el nombre del Equipo.</span>");
            return false;
		}else if( $("#categoria").val()=="0"){
            $("#categoria").focus().after("<br><span class='error'>Debe ingresar la Categor&iacute;a del equipo.</span>");
            return false;
		}
        
		qsocios=0;
        for(i=1;i<16;i++){
            if($("#dni_"+i).val().length > 6){
				qsocios += Number($("#socio_"+i).val());
                if( $("#nombApe_"+i).val().length < 3){
                    $("#nombApe_"+i).focus().after("<br><span class='error'>Debe ingresar el Nombre.</span>");
                    return false;
				}else if( $("#fnacimiento_"+i).val().length < 10){
                    $("#fnacimiento_"+i).focus().after("<br><span class='error'>Debe ingresar la fecha correcta.</span>");
                    return false;
                }else if(!validarFormatoFecha($("#fnacimiento_"+i).val())){
                    $("#fnacimiento_"+i).focus().after("<br><span class='error'>Debe ingresar la fecha correcta.</span>");
                    return false;
                }else if( Number($("#fnacimiento_"+i).val().substring(6,10))<1988 || Number($("#fnacimiento_"+i).val().substring(6,10))>2004 ){
                    $("#fnacimiento_"+i).focus().after("<br><span class='error'>Fecha fuera de rango.</span>");
                    return false;
                }else if( $("#tel_"+i).val().length < 7){
                    $("#tel_"+i).focus().after("<br><span class='error'>Debe ingresar un Tel&eacute;fono.</span>");
                    return false;
                }else if( validateEmail($("#mail_"+i).val())=='false'){
                    $("#mail_"+i).focus().after("<br><span class='error'>Debe ingresar un eMail v&aacute;lido.</span>");
                    return false;
                }
            }
			else{
                if(i<3){
                    alert("Al menos deben ser 2 jugadores para dar de alta al equipo. Y al menos 9 para jugar.");
                    return false;
                }
            }
			
        }
        
		/*
		if($("#categoria").val()=="F" && qsocios<3){
			 alert("Al menos deben ser 3 socios para esta categoria");
             return false;
		}
			
		if($("#categoria").val()=="G" && qsocios<4){
			 alert("Al menos deben ser 4 socios para esta categoria");
             return false;
		}
		
		if($("#categoria").val()=="H" && qsocios<5){
			 alert("Al menos deben ser 5 socios para esta categoria");
             return false;
		}
        */
        
		//Validar Socios
		/*
		for(i=1;i<16;i++){
			if($("#dni_"+i).val().length > 6 && $("#socio_"+i).val()=="1"){
				data="dni="+$("#dni_"+i).val();
				$.ajax({
					async:false,    
                    cache:false, 
					dataType: 'json',
					url: "codigo/validaSocio.php",
					dataType: 'text',
					data: data,
					success: function(result) {
						alert(result);
						if (result!="OK."){
							$("#socio_"+i).focus().after("<br><span class='error'>No es socio.</span>");
						}
					} 
				}); 
			}
		}
		*/
        //alert("OK");
		
        if($("#passResponsable").val()==""){
            $("#passResponsable").focus().after("<br><span class='error'>Debe ingresar una Password de al menos 5 caracteres.</span>");
            return false;
        }else{
            Guardar();
        }
        
		
	});	
    
    $(document).on("keyup", function(){
		$(".error").remove();
	});
    
    function validateEmail(emailField){
        var re = /\S+@\S+\.\S+/;
        if (re.test(emailField) == false){
			return 'false';
        }
        return 'true';
	}
    
    
    function Guardar(){
        $("#div_resultado").html("Guardando...por favor, espere.");
        var cat="3";
        switch($("#categoria").val()) {
            case "F":
                cat = "3" 
                break;
            case "G":
                cat = "4" 
                break;
            case "H":
                cat = "5" 
                break;
        } 
       
        alert("Recuerde que para poder participar, al menos deben ser 9 jugadores, de los cuales al menos "+cat+" deben ser socios para esta categoria. Recuerde su password para modificar su equipo en el futuro.");
        
           
		var data = 'dniR='+$("#dniResponsable").val()
		+'&nomApeRes='+$("#nombreApellidoResponsable").val()
		+"&telResp="+$("#telefonoResponsable").val()
		+"&mailResp="+$("#mailResponsable").val()
		+"&passResponsable="+$("#passResponsable").val()
        +"&NombreEquipo="+$("#NombreEquipo").val()
		+"&categoria="+$("#categoria").val()
        +"&idequipo="+$("#idequipo").val()
		;
        
        
        for(i=1;i<16;i++){
            if($("#dni_"+i).val().length > 6){
				var fecha = $("#fnacimiento_"+i).val();
				var fechabien = fecha.substring(6,10)+"-"+fecha.substring(3,5) +"-"+fecha.substring(0,2);
                data=data+"&dni_"+i+"="+$("#dni_"+i).val();
                data=data+"&nombApe_"+i+"="+$("#nombApe_"+i).val();
				data=data+"&fnacimiento_"+i+"="+fechabien;
                data=data+"&tel_"+i+"="+$("#tel_"+i).val();
                data=data+"&mail_"+i+"="+$("#mail_"+i).val();
                data=data+"&socio_"+i+"="+$("#socio_"+i).val();                
            }
        }
        
        //alert (data);
		
	
		$.ajax({
                type: "POST",
                url: "codigo/guardarjuvenil.php",
                async:false,    
                cache:false,  
                data: data,
                success: function(result) {
					//alert(result);
                    //$("#div_resultado").html(result);
                    
					if (result.indexOf("OK.") >= 0){
                        //var pos = result.indexOf("Idequipo");
                        //$("#idequipo").val(result.substring(pos+9,pos+15));
						$("#dniResponsable").change();
                        $('#passResponsable').val("");
                        $('#passResponsable').attr("disabled", true);
						$("#div_resultado").html("<p>El equipo <b>"+$("#NombreEquipo").val()+"</b> fue guardado con &eacute;xito!!!</p>");
						$('#botonPago2').show();
					}else{
						$("#div_resultado").html(result);	
					}
                    
					
                } 
            }); 

			
	}
    
    
    $("#botonPass").click(function(){
        if ($("#passResponsable2").val()!=""){
            var data = 'dniR='+$("#dniResponsable").val();
            $.ajax({
                    type: "GET",
                    url: "codigo/validaPass.php",
                    async:false,    
                    cache:false,  
                    data: data,
                    success: function (result) {
                        if (result.indexOf($("#passResponsable2").val()) >= 0){
                            $("#dialog3").dialog("close");
                            Guardar();
                        }else{
                             alert("La password no coincide con la ingresada.");
                        }
                    } 
                });
        }
	});  
    
    $("#passResponsable2").keyup(function (e){
        if (e.which == 13) {
			$("#botonPass").click();
		}
    });


	$("#btindex").click(function(){
		$(location).attr('href','index.php');		
	});
	
	
	$("#btcontacto").click(function(){
		$("#dialog").html("Por favor, env&iacute;e un mail a secretar&iacute;a con su consulta.</br></br>secretaria@regatasbellavista.com.ar</br></br>Muchas Gracias.");
		$("#dialog").dialog("open");
		
	}); 
	
	$("#btinfantil").click(function(){
		$(location).attr('href','infantil.php');		
	}); 
	
	$("#btfemenino").click(function(){
		$(location).attr('href','femenino.php');		
	}); 
	
	$("#btautorizados").click(function(){
		$(location).attr('href','../admin/');		
	}); 

	
	$("#botonPago").click(function(){
		
		$("#dialog2").load('pagosJuvenil.php?dni='+$("#dniResponsable").val());
		$("#dialog2").dialog("open");
		
	}); 
	
	$("#botonPago2").click(function(){
		$("#botonPago").click();
	});
    
    $("#botonCanc").click(function(){
		$("#dialog3").dialog("close");
	});  
    

    
            
});
