$(document).ready(function(){
	
	$('#divjugador *').attr("disabled", true); 
	$('#botonInscribir').attr("disabled", true); 
	
	
	$("#sexo").prop('selectedIndex', 0);
	$("#es_socio").prop('selectedIndex', -1);
	$("#puesto_preferido").prop('selectedIndex', -1);
	$("#nivel_de_juego").prop('selectedIndex', -1);
	$("#asiste_esc_futbol").prop('selectedIndex', -1);
	$("#padre_puede_colaborar").prop('selectedIndex', -1);
	
	
	$("#dialog").dialog({
            autoOpen: false,
            modal: true,
            width: 450,
            height: 250,
            show: "show",
            hide: "hide"
	});
	
	$("#dialog").click(function() {
   		 	$("#dialog").html("");
	        $("#dialog").dialog("close");
        });
	
	$("#dialog2").dialog({
            autoOpen: false,
            modal: true,
            width: 750,
            show: "show",
            hide: "hide"
	});
	
	
	$("#dniResponsable").keyup(function (){
            this.value = (this.value + '').replace(/[^0-9]/g, '');
          });
	$("#dni").keyup(function (){
            this.value = (this.value + '').replace(/[^0-9]/g, '');
          });
	
	
	$("#dniResponsable").change(function(){
		
		$("#nombreApellidoResponsable").val("");
		$("#telefonoResponsable").val("");
		$("#mailResponsable").val("");
		$('#botonPago').hide();
		$('#botonPago2').hide();
		
		 var id = $("#dniResponsable").val();
		 var data = 'dniR=' + id;

		  $.ajax({
                type: "GET",
                url: "codigo/buscarResp.php",
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data) {
                        for (var i = 0; i < data.length; i++) { 
							$("#nombreApellidoResponsable").val(data[i].nombreApellido);
                            $("#telefonoResponsable").val(data[i].respTelefono);
							$("#mailResponsable").val(data[i].respMail);
                        }
					if ($("#nombreApellidoResponsable").val().length!=0){$('#botonPago').show();};					
                    }
                } 
            }); 
    $('#divjugador *').attr("disabled", false);
	$('#categoria').attr("disabled", true);
    });
	
	
	$("#dni").change(function(){
		limpiarDatos();
		$('#dniResponsable').attr("disabled", true);
		var dni = $("#dni").val();
		var data = 'dni=' + dni;
		
	 
		//Trae los datos del jugador
		  $.ajax({
                type: "GET",
                url: "codigo/buscarJugador.php",
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data) {
						var i=0;
							Inscripto = data[i].Inscripto;
							fechaHora = data[i].fechaHora;
							nombreApellido = data[i].nombreApellido;
							sexo = data[i].sexo;
							feNac = data[i].fechaNacimiento;
							respDNI = data[i].respDNI;
							email = data[i].email;
							localidad = data[i].localidad;
							calle = data[i].calle;
							nro = data[i].nro;
							piso = data[i].piso;
							dto = data[i].dto;
							telefono = data[i].telefono;
							colegio = data[i].colegio;
							obraSocial = data[i].obraSocial;
							afiliado = data[i].afiliado;
							telEmergencias = data[i].telEmergencias;
					}
					
					if (Inscripto==1){
						$("#dialog").html("El jugador indicado ya se encuentra inscripto.</br></br>Fecha de inscripci�n: "+fechaHora);
						$("#dialog").dialog("open");
					}
					
					//$("#nombre_y_apellido").val($("#dniResponsable").val());
					
					if (nombreApellido!=null && respDNI != $("#dniResponsable").val()){
						$("#dialog").html("El jugador indicado figura registrado en la base de datos bajo otro responsable.</br></br>Por favor, verifique los datos y/o comuniquese con los organizadores.</br></br>Gracias.");
						$("#dialog").dialog("open");
					}
					
					if (sexo=='F'){
						$("#sexo").prop('selectedIndex', 1);
						$("#sexo").change();
					}
					
					if (nombreApellido!=null && respDNI==$("#dniResponsable").val() && Inscripto!=1 && sexo!='F'){
								if (sexo=='M'){$("#sexo").prop('selectedIndex', 0)}
								$("#nombre_y_apellido").val(nombreApellido);
								$("#fecha_de_nacimiento").val(feNac.substring(8,10)+'/'+feNac.substring(5,7)+'/'+feNac.substring(0,4));
								$("#fecha_de_nacimiento").change();
								$("#email").val(email);
								$("#localidad").val(localidad);
								$("#domicilio_calle").val(calle);
								$("#domicilio_numero").val(nro);
								$("#domicilio_piso").val(piso);
								$("#domicilio_dto").val(dto);
								$("#telefono").val(telefono);
								$("#colegio").val(colegio);
								$("#obraSocial").val(obraSocial);
								$("#afiliado").val(afiliado);
								$("#telEmergencias").val(telEmergencias);
					}			
                    
                } 
            }); 
		
	});	
   
	
	function limpiarDatos(){
		$("#nombre_y_apellido").val("");
		$("#sexo").prop('selectedIndex', 0);
		
		$("#fecha_de_nacimiento").val("");
		$("#categoria").val("");
		$("#email").val("");
		$("#localidad").val("");
		$("#domicilio_calle").val("");
		$("#domicilio_numero").val("");
		$("#domicilio_piso").val("");
		$("#domicilio_dto").val("");
		$("#telefono").val("");
		$("#colegio").val("");
		$("#obraSocial").val("");
		$("#afiliado").val("");
		$("#telEmergencias").val("");
		$("#es_socio").prop('selectedIndex', -1);
		$("#puesto_preferido").prop('selectedIndex', -1);
		$("#nivel_de_juego").prop('selectedIndex', -1);
		$("#asiste_esc_futbol").prop('selectedIndex', -1);
		$("#padre_puede_colaborar").prop('selectedIndex', -1);

	}

	
	$("#sexo").change(function(){
		if ($("#sexo").val()=='F'){
				 $("#dialog").html("Este formulario es solo para inscribir chicos varones.</br></br>Para inscribir chicas, vuelva la la pantalla anterior y seleccione el bot&oacute;n 'Femenino'.</br></br>Gracias.");
				 $("#dialog").dialog("open");
		}
		
	});
	
	
	//Categoria
	$("#fecha_de_nacimiento").change(function(){
		$("#categoria").val("");
		var fecha = $("#fecha_de_nacimiento").val();
		var data = 'nac=' + fecha.substring(6,10)+ fecha.substring(3,5) + fecha.substring(0,2);
		$.ajax({
                type: "GET",
                url: "codigo/buscarCategoria.php",
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data) {
                        for (var i = 0; i < data.length; i++) {
							if (data[i].cant==1){
								$("#categoria").val(data[i].idcategoria);
							}else{
								$("#categoria").val("");
								$("#dialog").html("La fecha de nacimiento ingresada no puede ser asociada a una categor&iacute;a.</br></br>Por favor, verifique que sea correcta y vuelva a ingresarla con el formato correspondiente.</br></br>Gracias.");
								$("#dialog").dialog("open");
							}
                        }
                    }
                } 
            }); 
	});
	
	//Localidades
	var availableLoc = [
		"Bella Vista",
		"Capital Federal",
		"Hurlingham",
		"Mu�iz",
		"San Miguel",
		"Boulogne",
		"Ituzaingo",
		"Villa de Mayo",
		"Parque Leloi"
    ];
	
    $("#localidad").autocomplete({
      source: availableLoc
    });
	
	
	//Colegios
	var availableColegios = [
		"de la Providencia",
		"Sagrada Familia",
		"Santa Ethnea",
		"AlmaFuerte",
		"San Alfonso",
		"Aberdare",
		"Luigi Pirandelo",
		"San Pio",
		"Las Marias",
		"D Elia",
		"Del Valle",
		"Glasgow",
		"El Tato",
		"San Pablo",
		"Jesus Maria",
		"St Hildas",
		"Divina Pastor",
		"El Tato"
	];
	
	$("#colegio").autocomplete({
      source: availableColegios
    });
	 

	
	$("#checkVal").change(function(){
			$('#botonInscribir').attr("disabled", !$(this).prop('checked')); 	
	});	
	
	
	$("#botonInscribir").click(function(){
		
		//Validar formulario
		$(".error").remove();
		
		if( $("#dniResponsable").val() == ""){
			$("#dniResponsable").focus().after("<br><span class='error'>Debe ingresar el DNI.</span>");
			return false;
        }else if( Number($("#dniResponsable").val()) < 1000000){
            $("#dniResponsable").focus().after("<br><span class='error'>Debe ingresar un DNI v&aacute;lido.</span>");
            return false;
		}else if( $("#nombreApellidoResponsable").val().length < 3){
            $("#nombreApellidoResponsable").focus().after("<br><span class='error'>Debe ingresar su Nombre y Apellido.</span>");
            return false;
		}else if( $("#telefonoResponsable").val().length < 7){
            $("#telefonoResponsable").focus().after("<br><span class='error'>Debe ingresar un Tel&eacute;fono v&aacute;lido.</span>");
            return false;
		}else if( validateEmail($("#mailResponsable").val())=='false'){
            $("#mailResponsable").focus().after("<br><span class='error'>Debe ingresar un eMail v&aacute;lido.</span>");
            return false;
		}else if( Number($("#dni").val()) < 30000000){
            $("#dni").focus().after("<br><span class='error'>Debe ingresar el DNI del Jugador.</span>");
            return false;
		}else if( $("#sexo").val()== null){
            $("#sexo").focus().after("<br><span class='error'>Debe ingresar el sexo.</span>");
            return false;
		}else if( $("#nombre_y_apellido").val().length < 3){
            $("#nombre_y_apellido").focus().after("<br><span class='error'>Debe ingresar el Nombre y Apellido.</span>");
            return false;
		}else if( $("#categoria").val() ==""){
            $("#fecha_de_nacimiento").focus().after("<br><span class='error'>Debe ingresar una fecha correcta.</span>");
            return false;
		}else if( $("#telEmergencias").val().length < 7){
            $("#telEmergencias").focus().after("<br><span class='error'>Debe ingresar un Tel&eacute;fono v&aacute;lido.</span>");
            return false;
		}
		else if( $("#es_socio").val()== null){
            $("#es_socio").focus().after("<br><span class='error'>Seleccione la opci&oacute;n.</span>");
            return false;
		}else if( $("#puesto_preferido").val()== null){
            $("#puesto_preferido").focus().after("<br><span class='error'>Seleccione la opci&oacute;n.</span>");
            return false;
		}else if( $("#nivel_de_juego").val()== null){
            $("#nivel_de_juego").focus().after("<br><span class='error'>Seleccione la opci&oacute;n.</span>");
            return false;
		}else if( $("#asiste_esc_futbol").val()== null){
            $("#asiste_esc_futbol").focus().after("<br><span class='error'>Seleccione la opci&oacute;n.</span>");
            return false;
		}else if( $("#padre_puede_colaborar").val()== null){
            $("#padre_puede_colaborar").focus().after("<br><span class='error'>Seleccione la opci&oacute;n.</span>");
            return false;
		}
		
		//alert("OK");
		Guardar();
		
	});	
	

	$(document).on("keyup", function(){
		$(".error").remove();
	});
	
	
	function validateEmail(emailField){
        var re = /\S+@\S+\.\S+/;
        if (re.test(emailField) == false){
			return 'false';
        }
        return 'true';
	}

	
	function Guardar(){
		
		
		var fecha = $("#fecha_de_nacimiento").val();
		var fechabien = fecha.substring(6,10)+"-"+fecha.substring(3,5) +"-"+fecha.substring(0,2);
		var data = 'dniR='+$("#dniResponsable").val()
		+'&nomApeRes='+$("#nombreApellidoResponsable").val()
		+"&telResp="+$("#telefonoResponsable").val()
		+"&mailResp="+$("#mailResponsable").val()
		+"&dni="+$("#dni").val()
		+"&sexo="+$("#sexo").val()
		+"&nomapellido="+$("#nombre_y_apellido").val()
		+"&fechanac="+fechabien
		+"&categoria="+$("#categoria").val()
		+"&email="+$("#email").val()
		+"&localidad="+$("#localidad").val()
		+"&calle="+$("#domicilio_calle").val()
		+"&numero="+$("#domicilio_numero").val()
		+"&piso="+$("#domicilio_piso").val()
		+"&dto="+$("#domicilio_dto").val()
		+"&telefono="+$("#telefono").val()
		+"&colegio="+$("#colegio").val()
		+"&obraSocial="+$("#obraSocial").val()
		+"&afiliado="+$("#afiliado").val()
		+"&telEmergencias="+$("#telEmergencias").val()
		+"&socio="+$("#es_socio").val()
		+"&puesto="+$("#puesto_preferido").val()
		+"&nivel="+$("#nivel_de_juego").val()
		+"&asiste="+$("#asiste_esc_futbol").val()
		+"&colaborar="+$("#padre_puede_colaborar").val()
		;
	
		$.ajax({
                type: "POST",
                url: "codigo/guardar.php",
                data: data,
                success: function(result) {
					//alert(result);
					if (result=="OK."){
						$("#div_resultado").html("<p>El jugador <b>"+$("#nombre_y_apellido").val()+"</b> fue inscripto con &eacute;xito!!!</p>");
						$('#divCarga *').attr("disabled", true);
						$('#botonOtro').show();
						$('#botonPago2').show();
						$('#botonPago').attr("disabled", false);
					}else{
						$("#div_resultado").html(result);	
					}
					
                } 
            }); 
			
			
	}
	
	$("#botonOtro").click(function(){
		limpiarDatos();
		$('#checkVal').attr('checked', false);
		$('#divCarga *').attr("disabled", false);
		$('#categoria').attr("disabled", true);
		$("#div_resultado").html("");
		$('#botonOtro').hide();
		$("#dni").val("");
		$('#botonInscribir').attr("disabled", true); 
		$('#botonPago2').hide();
	}); 
	

	$("#btindex").click(function(){
		$(location).attr('href','index.php');		
	}); 
	
	$("#btcontacto").click(function(){
		$("#dialog").html("Por favor, env&iacute;e un mail a secretar&iacute;a con su consulta.</br></br>secretaria@regatasbellavista.com.ar</br></br>Muchas Gracias.");
		$("#dialog").dialog("open");
		
	}); 
	
	$("#btfemenino").click(function(){
		$(location).attr('href','femenino.php');		
	}); 
	
	$("#btautorizados").click(function(){
		$(location).attr('href','../admin/');		
	}); 

	
	$("#botonPago").click(function(){
		
		$("#dialog2").load('pagos.php?dni='+$("#dniResponsable").val());
		$("#dialog2").dialog("open");
		
	}); 
	
	$("#botonPago2").click(function(){
		$("#botonPago").click();
	}); 
	
	
});


