$(document).ready(function(){
	
	$("#dialog").dialog({
            autoOpen: false,
            modal: true,
            width: 450,
            height: 250,
            show: "show",
            hide: "hide"
	});
	
	$("#dialog").click(function() {
   		 	$("#dialog").html("");
	        $("#dialog").dialog("close");
        });

	$("#dialog2").dialog({
            autoOpen: false,
            modal: true,
            width: 750,
            show: "show",
            hide: "hide"
	});
	
	$("#btregalmento").click(function(){
		const loc = window.location.href;
		$(location).attr('href',`/reglamento/REGLAMENTO-${loc.substring(loc.indexOf('cat=')+4)}.pdf`);
	});
	
	$("#btcontacto").click(function(){
		$("#dialog").html("Por favor, env&iacute;e un mail a secretar&iacute;a con su consulta.</br></br>secretaria@regatasbellavista.com.ar</br></br>Muchas Gracias.");
		$("#dialog").dialog("open");
		
	});

	$("#btpago").click(function(){
		$("#dialog2").html("<table><tr><td class='formD'>Ingrese el DNI del responsable:</td><td class='formC'><input type='text' maxlength='10' id='dniResponsable' name='dniResponsable' value=''/>&nbsp;&nbsp;&nbsp;&nbsp;<input id='botonPago' name='botonPago' type='button' value='Contol de Pagos' onclick=\"$('#dialog2').load('pagos.php?dni='+$('#dniResponsable').val());\"/></td></tr></table>");
		$("#dialog2").dialog("open");
	});

	
	$("#btautorizados").click(function(){
		$(location).attr('href','../admin/');		
	});
	
});		
