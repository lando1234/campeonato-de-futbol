<?php


require_once "../../codigo/connw.php";

  
	$dniR = $_POST['dniR'];
	$nomApeRes =$_POST['nomApeRes'];
	$telResp =$_POST['telResp'];
	$mailResp =$_POST['mailResp'];
	$dni =$_POST['dni'];
	$sexo =$_POST['sexo'];
	$nomapellido =$_POST['nomapellido'];
	$fechanac =$_POST['fechanac'];
	$grado =$_POST['grado'];
	$categoria =$_POST['categoria'];
	$email =$_POST['email'];
	$localidad =$_POST['localidad'];
	$calle =$_POST['calle'];
	$numero =$_POST['numero'];
	$piso =$_POST['piso'];
	$dto =$_POST['dto'];
	$telefono =$_POST['telefono'];
	$colegio =$_POST['colegio'];
	$obraSocial =$_POST['obraSocial'];
	$afiliado =$_POST['afiliado'];
	$telEmergencias =$_POST['telEmergencias'];
	$socio =$_POST['socio'];
	$puesto =$_POST['puesto'];
	$nivel =$_POST['nivel'];
	$asiste =$_POST['asiste'];
	$colaborar =$_POST['colaborar'];
	
	$chica1_nombre =$_POST['chica1_nombre'];
	$chica1_mail =$_POST['chica1_mail'];
	$chica2_nombre =$_POST['chica2_nombre'];
	$chica2_mail =$_POST['chica2_mail'];
	$chica3_nombre =$_POST['chica3_nombre'];
	$chica3_mail =$_POST['chica3_mail'];
	$chica4_nombre =$_POST['chica4_nombre'];
	$chica4_mail =$_POST['chica4_mail'];
	$chica5_nombre =$_POST['chica5_nombre'];
	$chica5_mail =$_POST['chica5_mail'];
	$chica6_nombre =$_POST['chica6_nombre'];
	$chica6_mail =$_POST['chica6_mail'];
	$chica7_nombre =$_POST['chica7_nombre'];
	$chica7_mail =$_POST['chica7_mail'];
	$chica8_nombre =$_POST['chica8_nombre'];
	$chica8_mail =$_POST['chica8_mail'];
	
	//Torneo
	$sql0 = "select idtorneo from torneo where estado ='A';";
	$result = $mysqli->query($sql0);
	$row = $result->fetch_assoc();
	$torneo = $row['idtorneo'];
	
	//Socio
	$sql4 = "select count(*) essocio from socios where dni=$dni;";
	$result4 = $mysqli->query($sql4);
	$row4 = $result4->fetch_assoc();
	if ($socio=='1' and $row4['essocio']=='1'){
		$esSocio='1';
	}else{
		$esSocio='0';
	}

	//Tarifas
	$sql7 ="select sum(msocio)mSocio, sum(mnosocio)mNoSocio
    from (
    SELECT case when socio=1 then monto end as msocio,
    case when socio=0 then monto end as mnosocio
    FROM bdcampeonato.tarifas
    where fdesde < now() and fhasta > now()
    and idtorneo =(select idtorneo from torneo where estado='A')
    )a;";
	if (!$result7 = $mysqli->query($sql7)) {
		echo "Lo sentimos, este sitio web está experimentando problemas.";
		exit;
	}
	$row7 = $result7->fetch_assoc();
	$mSocio=$row7['mSocio'];
    $mNoSocio=$row7['mNoSocio'];
	
	
	//Validar sexo
	if ($sexo=='M'){
		echo "<span class='error'>Error - No se puede inscribir chicos en este formulario.</span>";
	}else{
		//Validar si esta inscripto
		$sql = "SELECT i.DNIjugador, i.fechahora FROM inscriptos i inner join torneo t on i.idtorneo = t.idtorneo where t.estado = 'A' and i.DNIjugador = '$dni';";
		$result = $mysqli->query($sql);
		$cant = $result->num_rows;
		
		if($cant>0){
			echo "<span class='error'>Error - El Jugador ya est&aacute; inscripto.</span>";
		}else{
			//Actualiza Resp
			$sql1 ="INSERT INTO responsable (respDNI, nombreApellido, respTelefono, respMail, fechaActualizacion)VALUES ('$dniR', '$nomApeRes', '$telResp', '$mailResp', now())
					ON DUPLICATE KEY UPDATE nombreApellido='$nomApeRes', respTelefono='$telResp', respMail='$mailResp', fechaActualizacion=now();";
			if ($mysqli->query($sql1) === TRUE) {
				//Actualiza Jugador
				$sql2 ="INSERT INTO jugador (DNIjugador, sexo, nombreApellido, email, fechaNacimiento, localidad,
					calle, nro, piso, dto, telefono, colegio, obraSocial, afiliado, telEmergencias, socioClub,
					puesto, nivel, escuelaFutbol, grado, respDNI, fechaActualizacion)
					VALUES ('$dni', '$sexo', '$nomapellido', '$email', '$fechanac', '$localidad', '$calle', '$numero',  
					'$piso', '$dto', '$telefono', '$colegio', '$obraSocial', '$afiliado', '$telEmergencias', '$socio', 
					'$puesto', '$nivel', '$asiste', '$grado', '$dniR', now())
					ON DUPLICATE KEY UPDATE sexo='$sexo', nombreApellido='$nomapellido', email='$email', fechaNacimiento='$fechanac',
					localidad='$localidad', calle='$calle', nro='$numero', piso='$piso', dto='$dto', telefono='$telefono',
					colegio='$colegio', obraSocial='$obraSocial', afiliado='$afiliado', telEmergencias='$telEmergencias',
					socioClub='$socio',puesto='$puesto', nivel='$nivel', escuelaFutbol='$asiste', grado='$grado', respDNI='$dniR', fechaActualizacion=now();";
				if ($mysqli->query($sql2) === TRUE) {
					//Agrega a Inscriptos
					$sql3 ="INSERT INTO inscriptos(DNIjugador, idtorneo, nombreApellido, sexo, idcategoria, socio, respDNI, respEntrenador, fechahora, dto, monto, aprobado)
					VALUES('$dni', '$torneo', '$nomapellido', '$sexo', '$categoria', '$esSocio', '$dniR', '$colaborar', now() , '0', '0', '0');";
					if ($mysqli->query($sql3) === TRUE) {
						//Agrega invitaciones
						if (strlen ($chica1_nombre)>0){
							$sql4 = "INSERT INTO invitacion_chicas (`dni`,`nombre_y_apellido`,`chica`,`mail`,`fechaActualizacion`,`enviado`) VALUES('$dni','$nomapellido','$chica1_nombre', '$chica1_mail', now(), 0)";
							$mysqli->query($sql4);}
						if (strlen ($chica2_nombre)>0){
							$sql4 = "INSERT INTO invitacion_chicas (`dni`,`nombre_y_apellido`,`chica`,`mail`,`fechaActualizacion`,`enviado`) VALUES('$dni','$nomapellido','$chica2_nombre', '$chica2_mail', now(), 0)";
							$mysqli->query($sql4);}
						if (strlen ($chica3_nombre)>0){
							$sql4 = "INSERT INTO invitacion_chicas (`dni`,`nombre_y_apellido`,`chica`,`mail`,`fechaActualizacion`,`enviado`) VALUES('$dni','$nomapellido','$chica3_nombre', '$chica3_mail', now(), 0)";
							$mysqli->query($sql4);}
						if (strlen ($chica4_nombre)>0){
							$sql4 = "INSERT INTO invitacion_chicas (`dni`,`nombre_y_apellido`,`chica`,`mail`,`fechaActualizacion`,`enviado`) VALUES('$dni','$nomapellido','$chica4_nombre', '$chica4_mail', now(), 0)";
							$mysqli->query($sql4);}
						if (strlen ($chica5_nombre)>0){
							$sql4 = "INSERT INTO invitacion_chicas (`dni`,`nombre_y_apellido`,`chica`,`mail`,`fechaActualizacion`,`enviado`) VALUES('$dni','$nomapellido','$chica5_nombre', '$chica5_mail', now(), 0)";
							$mysqli->query($sql4);}
						if (strlen ($chica6_nombre)>0){
							$sql4 = "INSERT INTO invitacion_chicas (`dni`,`nombre_y_apellido`,`chica`,`mail`,`fechaActualizacion`,`enviado`) VALUES('$dni','$nomapellido','$chica6_nombre', '$chica6_mail', now(), 0)";
							$mysqli->query($sql4);}
						if (strlen ($chica7_nombre)>0){
							$sql4 = "INSERT INTO invitacion_chicas (`dni`,`nombre_y_apellido`,`chica`,`mail`,`fechaActualizacion`,`enviado`) VALUES('$dni','$nomapellido','$chica7_nombre', '$chica7_mail', now(), 0)";
							$mysqli->query($sql4);}
						if (strlen ($chica8_nombre)>0){
							$sql4 = "INSERT INTO invitacion_chicas (`dni`,`nombre_y_apellido`,`chica`,`mail`,`fechaActualizacion`,`enviado`) VALUES('$dni','$nomapellido','$chica8_nombre', '$chica8_mail', now(), 0)";
							$mysqli->query($sql4);}
							
						$sql4 = "SELECT idtorneo, DNIjugador, socio, aprobado FROM inscriptos where respDNI='$dniR' and idtorneo=$torneo order by fechahora;";
						$result4 = $mysqli->query($sql4);
						$i = 0;
						$monto = 0;
						$dto='-';
						while ($row4 = $result4->fetch_assoc()) {
							$i=$i+1;
							if ($row4['socio']==1){$monto=$mSocio;}else{$monto=$mNoSocio;}
							if ($i>=3){$monto=$monto/2; $dto='50%';}else{$dto='-';}
							if ($row4['aprobado']==0){
								$sql5 = "update inscriptos set dto='".$dto."', monto='".$monto."' where idtorneo='".$row4['idtorneo']."' and DNIjugador='".$row4['DNIjugador']."';";
								//echo $sql5.'<br>';
								$result5 = $mysqli->query($sql5);
							}
						}
						echo "OK.";
					}else {
						echo "<span class='error'>Error guardando Inscriptos: " . $mysqli->error . "</span>";
					}
				} else {
					echo "<span class='error'>Error guardando Jugadora: " . $mysqli->error . "</span>";
				}
			} else {
				echo "<span class='error'>Error guardando Responsable: " . $mysqli->error . "</span>";
			}
			
			
			
		}
	}
	
	
/*
	echo 
	'dniR='.$dniR.'<br>'.
	'nomApeRes='.$nomApeRes.'<br>'.
	'telResp='.$telResp.'<br>'.
	'mailResp='.$mailResp.'<br>'.
	'dni='.$dni.'<br>'.
	'sexo='.$sexo.'<br>'.
	'nomapellido='.$nomapellido.'<br>'.
	'fechanac='.$fechanac.'<br>'.
	'categoria='.$categoria.'<br>'.
	'email='.$email.'<br>'.
	'localidad='.$localidad.'<br>'.
	'calle='.$calle.'<br>'.
	'numero='.$numero.'<br>'.
	'piso='.$piso.'<br>'.
	'dto='.$dto.'<br>'.
	'telefono='.$telefono.'<br>'.
	'colegio='.$colegio.'<br>'.
	'obraSocial='.$obraSocial.'<br>'.
	'afiliado='.$afiliado.'<br>'.
	'telEmergencias='.$telEmergencias.'<br>'.
	'socio='.$socio.'<br>'.
	'puesto='.$puesto.'<br>'.
	'nivel='.$nivel.'<br>'.
	'asiste='.$asiste.'<br>'.
	'colaborar='.$colaborar.'<br>'.
	'chica1_nombre='.$chica1_nombre.'<br>'.
	'chica1_mail='.$chica1_mail.'<br>'.
	'chica2_nombre='.$chica2_nombre.'<br>'.
	'chica2_mail='.$chica2_mail.'<br>'.
	'chica3_nombre='.$chica3_nombre.'<br>'.
	'chica3_mail='.$chica3_mail.'<br>'
	
	;
	*/

		
?>
