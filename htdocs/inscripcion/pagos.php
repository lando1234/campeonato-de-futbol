<?php

require_once "../codigo/connw.php";
$dni ="";

if (isset($_GET['dni'])){
	$dni =$_GET['dni'];

	$sql1 ="select nombreApellido from responsable where respDNI = '$dni'";
	if (!$result1 = $mysqli->query($sql1)) {
		echo "Lo sentimos, este sitio web est&aacute; experimentando problemas.";
		exit;
	}
	$row1 = $result1->fetch_assoc();
	$nombreResp=$row1['nombreApellido'];

	
	$sql2 ="select sum(monto) as pago 
	from pagos where 
	respDNI = '$dni' 
	and idtorneo = (select idtorneo from torneo where estado='A');";
	if (!$result2 = $mysqli->query($sql2)) {
		echo "Lo sentimos, este sitio web est&aacute; experimentando problemas.";
		exit;
	}
	$row2 = $result2->fetch_assoc();
	$pago=$row2['pago'];
	
	
	$sql = "SELECT idtorneo, respDNI,DNIjugador,nombreApellido,sexo,idcategoria,fechahora,socio,dto,monto 
    FROM inscriptos
	where respDNI='$dni'
	and idtorneo =(select idtorneo from torneo where estado='A')
	order by fechahora asc;
	";


	if (!$result = $mysqli->query($sql)) {
		echo "Lo sentimos, este sitio web est&aacute; experimentando problemas.";
		exit;
	}
	
	
?>

<!DOCTYPE html>
<html lang="es-ES">
<head>
<style>

</style>

</head>
<body>
Detalle del Importe del Responsable: <b><?php echo $nombreResp; ?></b> (DNI: <?php echo $dni; ?>)</br></br>
<table id="tbpagos" >
<tr>
	<th>Dni Jugador</th>
	<th>Nombre y Apellido</th>
	<th>Sexo</th>
	<th>Categor&iacute;a</th>
	<th>Inscripto</th>
    <th>Socio</th>
    <th>Dto</th>
	<th>Monto</th>
</tr>
<?php
	$total = 0;
	while ($row = $result->fetch_assoc()) {
		$total = $total+$row['monto'];
?>
<tr>
	<td><?php echo ($row['DNIjugador']);?></td>
	<td><?php echo ($row['nombreApellido']);?></td>
	<td><?php echo ($row['sexo']);?></td>
	<td><?php echo ($row['idcategoria']);?></td>
    <td><?php echo ($row['fechahora']);?></td>
	<td><?php if ($row['socio']=='0'){echo "No";}else{echo "Si";}?></td>
	<td><?php echo ($row['dto']);?></td>
	<td><?php echo ("$ ".$row['monto']);?></td>
</tr>
<?php
	}
	$result->free();
	$result1->free();
	$result2->free();
}


?>
<tr>
	<th colspan='7' style="text-align: right;">Total: </th>
	<th><?php echo("$ ".$total.".00");?></th>
</tr>
</table>
<p style="font-size:9px;">
La inscripci&oacute;n no estar&aacute; finalizada hasta tanto no cancele el pago a trav&eacute;s de pagomiscuentas.com (el pago a travez de pagomiscuentas tiene que ser con el mismo dni del responsable de la inscripcion), o de los puntos de cobro detallados.</br>
En caso de hacerlo por <a href='https://pagomiscuentas.com/'><b>pagomiscuentas</b></a>, es necesario que el documento del responsable de la inscripci&oacute;n coincida con el del titular de la 
cuenta bancaria, a fin de poder identificarlo. De no ser asi, rogamos nos envie el pago a <b>secretaria@regatasbellavista.com.ar</b>
informando all&iacute; quien es el responsable.</br>
Atenci&oacute;n: LOS PAGOS REALIZADOS DESPUES DEL 28/9 SERAN ACTUALIZADOS, INDEPENDIENTEMENTE DE LA FECHA EN QUE SE HAYA REALIZADO LA INSCRIPCION.
</p>
<p>
Pagado: <b><?php echo("$ ".$pago.".00");?></b></br>
Adeudado: <b><?php echo("$ ".($total-$pago).".00");?></b>
</p>

<?php
if ($total!=0){
?>
<div style="text-align: center;"><a style="background: #efefef none repeat scroll 0 0; border: 1px solid buttonshadow;padding: 5px;" 
href="pdfPago.php?dni=<?php echo $dni;?>">Generar Planilla de Pago</a></div>
<?php 
} 
?>

</body>


	

