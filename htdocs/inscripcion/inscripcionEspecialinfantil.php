<!DOCTYPE html>
<html lang="es-ES">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="xmlrpc.php">
	<title>Campeonato Infantil de F&uacute;tbol &#8211; Noviembre 2019 &#8211; Club de Regatas Bella Vista</title>
	<link rel='dns-prefetch' href='//fonts.googleapis.com' />
	
	<link rel='stylesheet' id='style-css'  href='../css/style.css' type='text/css' media='all' />
	<link rel='stylesheet' id='framework-css'  href='../css/framework.css' type='text/css' media='all' />
	<link rel='stylesheet' id='style002-css'  href='../css/style002.css' type='text/css' media='all' />
	<link rel='stylesheet' id='sportspress'  href='../css/sportspress-sponsors.css' type='text/css' media='all' />
	<link rel='stylesheet' id='agregado-css'  href='../css/agregado.css' type='text/css' media='all' />

	<link rel="stylesheet" type="text/css" href="../css/themes/smoothness/jquery-ui-1.8.4.custom.css" />
	
	<script type="text/javascript" src="../codigo/jquery-1.8.2.js"></script>
	<script type="text/javascript" src="../codigo/jquery-ui-1.9.0.custom.min.js"></script>
		
	<script type="text/javascript" src="codigo/infantil.js"></script>
	
</head>

<body class="home page-template-default page page-id-242 custom-background">

<div class="sp-header"></div>
<div id="page" class="hfeed site">


	<header id="masthead" class="site-header" role="banner">
				<div class="header-area header-area-has-search">
				<div class="site-branding site-branding-empty">
					<div class="site-identity"></div>
				</div><!-- .site-branding -->
												<div class="site-banner">
					<img class="site-banner-image" src="images/banner1000x148.jpg" alt="Club de Regatas Bella Vista">
				</div><!-- .site-banner -->

	<div class="site-menu">
	<nav id="site-navigation" class="main-navigation" role="navigation">
    <div class="menuIz" style="width: 85%;">
            <span id="btindex" class="menug">Inicio</span>
            <span id="btcontacto" class="menug">Contacto</span>
    </div>

    <div class="menuDe"style="width: 15%;"
        <span id="btautorizados" class="menug"  style="padding: 0.625em 0;">Usuarios Autorizados</span>
    </div>
	</nav>	
				</div>
						</div>
		</header><!-- #masthead -->

	<div id="content" class="site-content">
		
	<div id="primary" class="content-area-full-width content-area-right-sidebar">
		<main id="main" class="site-main" role="main">

			
				
<article id="post-242" class="post-242 page type-page status-publish hentry">
	<header class="entry-header">
				
		<h1 class="entry-title">Inscripci&oacute;n F&uacute;tbol Infantil</h1>	</header><!-- .entry-header -->

	<div class="entry-content">
		<p>Aqu&iacute; se podr&aacute;n inscribir las categor&iacute;as A, B, C, D, E. Correspondientes a los ni&ntilde;os nacidos entre el 2004 y 2013.</p>
<div id="divCarga" class="sportspress sp-widget-align-none">
	<div class="sp-template sp-template-league-table">
	<h4 class="sp-table-caption">Responsable</h4>
	
	
	<div class="sp-table-wrapper">
	<table>
	<tr><td colspan='2'></td></tr>
	<tbody>
					<tr >
						<td class="formD">DNI responsable:</td>
						<td class="formC"><input type="text" maxlength="10" id="dniResponsable" name="dniResponsable" size="43" /> *
						<input id="botonPago" name="botonPago" type="button" value="Contol de Pagos" hidden="hidden"/>
						</td>
					</tr>
					<tr>
						<td class="formD">Nombre y Apellido:</td>
						<td class="formC"><input type="text" maxlength="100" id="nombreApellidoResponsable"
							name="nombreApellidoResponsable" size="43" /> *
						</td>
					</tr>
					<tr>
						<td class="formD">Telefono:</td>
						<td class="formC"><input type="text" maxlength="30" id="telefonoResponsable"
							name="telefonoResponsable" size="43" /> *
						</td>
					</tr>
					<tr>
						<td class="formD">Mail:</td>
						<td class="formC"><input type="text" maxlength="50" id="mailResponsable"
							name="mailResponsable" size="43" /> *
						</td>
					</tr>
					
					
	<tr><td colspan='2'></td></tr>
	</tbody>
	</table>
	</div>
	
	<h4 class="sp-table-caption">Datos del Jugador</h4>
	
	
	<div name="divjugador" id="divjugador" class="sp-table-wrapper" >
	<table name="formulario" id="formulario">
	<tr><td colspan='2'></td></tr>
	<tbody>
	<tr>
						<td class="formD">DNI del Jugador:</td>
						<td class="formC"><input type="text" maxlength="10" id="dni" name="dni"
							size="43" /> *
						</td>
					</tr>
					<tr>
						<td class="formD">Sexo:</td>
						<td class="formC">
						<select id="sexo" name="sexo">
							<option value="M">Masculino</option>
							<option value="F">Femenino</option>
						</select> *
						</td>
					</tr>
					<tr>
						<td class="formD">Nombre y Apellido:</td>
						<td class="formC"><input type="text" maxlength="45" id="nombre_y_apellido"
							name="nombre_y_apellido" size="43" /> *
						</td>
					</tr>
					<tr>
						<td class="formD">Fecha de nacimiento: dd/mm/aaaa</td>
						<td class="formC"><input type="text" maxlength="10" id="fecha_de_nacimiento"
							name="fecha_de_nacimiento" "  size="43" /> *
						</td>

					</tr>
					<tr>
						<td class="formD">Categor&iacute;a:</td>
						<td class="formC"><input type="text" maxlength="45" id="categoria" name="categoria"
							size="43" disabled/>
					</tr>
					<tr>
						<td class="formD">Email Jugador:</td>
						<td class="formC"><input type="text" maxlength="45" id="email" name="email"
							size="43" />
						</td>
					</tr>
					<tr>
						<td class="formD">Localidad:</td>
						<td class="formC"><input type="text" maxlength="45" id="localidad" name="localidad"
							size="43" />
						</td>
					</tr>
					<tr>
						<td class="formD">Calle:</td>
						<td class="formC"><input type="text" maxlength="45" id="domicilio_calle"
							name="domicilio_calle" size="43" /></td>
					</tr>
					<tr>
						<td class="formD">Nro:</td>
						<td class="formC"><input type="text" maxlength="10" id="domicilio_numero"
							name="domicilio_numero" size="43" />
						</td>
					</tr>
					<tr>
						<td class="formD">Piso:</td>
						<td class="formC"><input type="text" maxlength="3" id="domicilio_piso"
							name="domicilio_piso" size="43" />
						</td>
					</tr>
					<tr>
						<td class="formD">Dto:</td>
						<td class="formC"><input type="text" maxlength="3" id="domicilio_dto"
							name="domicilio_dto" size="43" /></td>
					</tr>

					<tr>
						<td class="formD">Tel&eacute;fono:</td>
						<td class="formC"><input type="text" maxlength="20" id="telefono"
							name="telefono" size="43" />
						</td>
					</tr>
					<tr>
						<td class="formD">Colegio:</td>
						<td class="formC"><input type="text" maxlength="20" id="colegio"
							name="colegio" size="43" />
						</td>
					</tr>
					<tr>
						<td class="formD">Prepaga/Obra Social:</td>
						<td class="formC"><input type="text" maxlength="40" id="obraSocial"
							name="obraSocial" size="43" />
						</td>
					</tr>
					<tr>
						<td class="formD">Nro de Afiliado:</td>
						<td class="formC"><input type="text" maxlength="40"
							id="afiliado"
							name="afiliado" size="43" />
						</td>
					</tr>
					<tr>
						<td class="formD">Tel&eacute;fonos en caso de Emergencias:</td>
						<td class="formC"><input type="text" maxlength="50"
							id="telEmergencias"
							name="telEmergencias" size="43" /> *
						</td>
					</tr>
					<tr>
						<td class="formD">Socio del Club:</td>
						<td class="formC"><select id="es_socio" name="es_socio">
								<option value="1">SI</option>
								<option value="0">NO</option>
						</select> *
						</td>
					</tr>

					<tr>
						<td class="formD">Puesto en el que prefiere jugar:</td>
						<td class="formC"><select id="puesto_preferido" name="puesto_preferido">
								<option value="delantero">Delantero</option>
								<option value="medio campo">Medio campo</option>
								<option value="defensor">Defensor</option>
								<option value="arquero">Arquero</option>
						</select> *
						</td>
					</tr>
					<tr>
						<td class="formD">Nivel de juego</td>
						<td class="formC"><select id="nivel_de_juego" name="nivel_de_juego">
								<option value="1">Regular</option>
								<option value="2">Bueno</option>
								<option value="3">Muy Bueno</option>
								<option value="4">Excelente</option>
						</select> *
						</td>
					</tr>
					<tr>
						<td class="formD">Asiste a escuelita de f&uacute;tbol</td>
						<td class="formC"><select id="asiste_esc_futbol"
							name="asiste_esc_futbol">
								<option value="1">SI</option>
								<option value="0">NO</option>
						</select> *
						</td>
					</tr>
					<tr>	
						<td class="formD">&iquest;Puede colaborar siendo entrenador y su hijo capitan?</td>
						<td class="formC"><select id="padre_puede_colaborar"
							name="padre_puede_colaborar">
								<option value="1">SI</option>
								<option value="0">NO</option>
						</select> *
						</td>
					</tr>
	<tr><td></td><td style='font-size: 12px; padding: 0 15px; text-align: left;'>* Campos obligatorios.</td></tr>
	<tr><td colspan='2'></td></tr>
	</tbody>
	</table>
	</div>
	
	
	</div>
	
	<p><input type="checkbox" id="checkVal" value=""	 />
	Autorizo a mi hijo a participar en el Campeonato, acepto todas las normas y reglamentos dispuestos para su realizaci&oacute;n y dejo
	constancia de que los datos consignados son correctos.</p>
	<div id="div_inscribir">		
		<input id="botonInscribir" name="botonInscribir" type="button" value="Inscribir" />
	</div>		
	
</div>

<div id="div_resultado"></div>
<input id="botonOtro" name="botonOtro" type="button" value="Inscribir Otro Jugador" hidden="hidden"/>
<input id="botonPago2" name="botonPago2" type="button" value="Contol de Pagos" hidden="hidden"/>

			</div><!-- .entry-content -->
</article><!-- #post-## -->
			
		</main><!-- #main -->
	</div><!-- #primary -->

			<div class="sp-footer-sponsors">
				<div class="sportspress">	
				<?php include 'sponsors.php'; ?>

				</div>			
			</div>
			
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-area">
			<div id="quaternary" class="footer-widgets" role="complementary">
		
		<div class="footer-widget-region">
									
		<aside id="awesomeweatherwidget-2" class="widget widget_awesomeweatherwidget">
		<h3 class="widget-title">El Clima en la cancha</h3>
		<div id="awesome-weather-bella-vista-buenos-aires" class="awesome-weather-wrap awecf awe_wide awe_custom awe_with_stats awe-code-701 awe-desc-niebla awe-preset-atmosphere darken" style=" color: #ffffff;   ">
			
		<div id="cont_321e0f7f5561f9896a0c2aabd859cb3c"><script type="text/javascript" async src="https://www.meteored.com.ar/wid_loader/321e0f7f5561f9896a0c2aabd859cb3c"></script></div>
			
		</aside>
		</div>
		
		
	<div class="footer-widget-region">
		<div class="sp-widget-align-none">
		
		
		<aside id="sportspress-facebook-2" class="widget widget_sportspress widget_sp_facebook">
									
	<h3 class="widget-title">Nuestro Facebook</h3>	
	<div class="sp-template sp-template-facebook">
		<div class="sp-facebook">
		<iframe class="" name="f3843f4291321de" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:page Facebook Social Plugin" style="border: medium none; visibility: visible; width: 300px; height: 300px;" src="https://www.facebook.com/v2.8/plugins/page.php?adapt_container_width=true&app_id=818713328266556&channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FXBwzv5Yrm_1.js%3Fversion%3D42%23cb%3Df11e782fa4acbaa%26domain%3Dwww.campeonatoinfantildefutbol.com.ar%26origin%3Dhttp%253A%252F%252Fwww.campeonatoinfantildefutbol.com.ar%252Ff8c41713042498%26relation%3Dparent.parent&container_width=300&hide_cover=false&href=https%3A%2F%2Fwww.facebook.com%2Frevistaregatas%2F&locale=en_US&sdk=joey&show_facepile=true&small_header=false&tabs=timeline" frameborder="0">
		</iframe>
		</div>
	</div>
	</aside>
	
	</div></div>
									
							</div>
		</div><!-- .footer-area -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<p>&nbsp;</p>

<div id="dialog" title="Atenci&oacute;n">
Cargando...	
</div>

<div id="dialog2" title="Control de Pagos">
Cargando...	
</div>

</body>
