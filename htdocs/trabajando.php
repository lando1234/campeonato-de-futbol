<!DOCTYPE html>
<html lang="es-ES">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="xmlrpc.php">
    <title>Campeonato Infantil de F&uacute;tbol &#8211; Noviembre 2019 &#8211; Club de Regatas Bella Vista</title>
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />

    <link rel='stylesheet' id='style-css' href='css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='framework-css' href='css/framework.css' type='text/css' media='all' />
    <link rel='stylesheet' id='trabajando-css' href='css/nuevo/trabajando.css' type='text/css' media='all' />

    <link rel='stylesheet' id='agregado-css' href='css/agregado.css' type='text/css' media='all' />
    <link rel='stylesheet' id='style002-css' href='css/style002.css' type='text/css' media='all' />
    <link rel='stylesheet' id='sportspress' href='css/sportspress-sponsors.css' type='text/css' media='all' />

    <link rel="stylesheet" type="text/css" href="css/themes/smoothness/jquery-ui-1.8.4.custom.css" />

    <script type="text/javascript" src="codigo/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="codigo/jquery-ui-1.9.0.custom.min.js"></script>

    <link rel="stylesheet" type="text/css" href="slick/slick.css">
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css">
    <script src="slick/slick.js" type="text/javascript" charset="utf-8"></script>


    <script type="text/javascript" src="codigo/jugadores.js"></script>


</head>

<body class="home page-template-default page page-id-242 custom-background">

    <div class="sp-header"></div>
    <div id="page" class="hfeed site">


        <header id="masthead" class="site-header" role="banner">
            <div class="header-area header-area-has-search">
                <div class="site-branding site-branding-empty">
                    <div class="site-identity"></div>
                </div><!-- .site-branding -->
                <div class="site-banner" style="position: relative;height: 148px;">
                    <img class="site-banner-image" src="images/banner1000x148.jpg" alt="Club de Regatas Bella Vista"
                        style="position: absolute;z-index: -1;">
                </div><!-- .site-banner -->
                <div class="site-menu">

                    <nav id="site-navigation" class="main-navigation" role="navigation">


                        <div class="menuIz" style="width: 85%; padding:20px;">
                            
                        </div>

                        <div class="menuDe" style="width: 15%; padding:20px;">
                        </div>

                    </nav>


                </div>
            </div>
        </header><!-- #masthead -->

        <div id="content" class="site-content">

            <div id="primary" class="">
                <main id="main" class="site-main" role="main">



                    <article id="post-242" class="post-242 page type-page status-publish">
                        <div class="trabajando">
                            <div class="trabajando__contenedor-titulo">

                                <h1 class="trabajando__titulo">Lo sentimos, la p&aacute;gina no est&aacute; disponible</h1>
                                <h2 class="trabajando__subtitulo">Por favor intent&aacute; m&aacute;s tarde</h2>
                            </div>

                            <a class="trabajando__link" href="menu.php">Volver al home &crarr;</a>

                        </div>
                    </article><!-- #post-## -->



                </main><!-- #main -->
            </div><!-- #primary -->
            <style type="text/css">
                .sp-footer-sponsors {
                    background: #f4f4f4;
                    color: #363f48;
                }

                .sp-footer-sponsors .sp-sponsors .sp-sponsors-title {
                    color: #363f48;
                }
            </style>
            <div class="sp-footer-sponsors">
                <div class="sportspress">
                <?php include 'sponsors.php'; ?>

                </div>
            </div>

        </div><!-- #content -->

        <footer id="colophon" class="site-footer" role="contentinfo">
            <div class="footer-area">
                <div id="quaternary" class="footer-widgets" role="complementary">

                    <div class="footer-widget-region">

                        <aside id="awesomeweatherwidget-2" class="widget widget_awesomeweatherwidget">
                            <h3 class="widget-title">El Clima en la cancha</h3>
                            <div id="awesome-weather-bella-vista-buenos-aires" class="awesome-weather-wrap awecf awe_wide awe_custom awe_with_stats awe-code-701 awe-desc-niebla awe-preset-atmosphere darken"
                                style=" color: #ffffff;   ">

                                <div id="cont_321e0f7f5561f9896a0c2aabd859cb3c">
                                    <script type="text/javascript" async src="https://www.meteored.com.ar/wid_loader/321e0f7f5561f9896a0c2aabd859cb3c"></script>
                                </div>

                        </aside>
                    </div>


                    <div class="footer-widget-region">
                        <div class="sp-widget-align-none">


                            <aside id="sportspress-facebook-2" class="widget widget_sportspress widget_sp_facebook">

                                <h3 class="widget-title">Nuestro Facebook</h3>
                                <div class="sp-template sp-template-facebook">
                                    <div class="sp-facebook">
                                        <iframe class="" name="f3843f4291321de" allowtransparency="true"
                                            allowfullscreen="true" scrolling="no" title="fb:page Facebook Social Plugin"
                                            style="border: medium none; visibility: visible; width: 300px; height: 300px;"
                                            src="https://www.facebook.com/v2.8/plugins/page.php?adapt_container_width=true&app_id=818713328266556&channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FXBwzv5Yrm_1.js%3Fversion%3D42%23cb%3Df11e782fa4acbaa%26domain%3Dwww.campeonatoinfantildefutbol.com.ar%26origin%3Dhttp%253A%252F%252Fwww.campeonatoinfantildefutbol.com.ar%252Ff8c41713042498%26relation%3Dparent.parent&container_width=300&hide_cover=false&href=https%3A%2F%2Fwww.facebook.com%2Frevistaregatas%2F&locale=en_US&sdk=joey&show_facepile=true&small_header=false&tabs=timeline"
                                            frameborder="0">
                                        </iframe>
                                    </div>
                                </div>
                            </aside>



                        </div>
                    </div>

                </div>
            </div><!-- .footer-area -->
        </footer><!-- #colophon -->
    </div><!-- #page -->

    <p>&nbsp;</p>

    <div id="dialog" title="Atenci&oacute;n">
        Cargando...
    </div>

</body>