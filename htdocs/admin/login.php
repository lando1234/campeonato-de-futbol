<!DOCTYPE html>
<html lang="es-ES">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="xmlrpc.php">
	<title>Campeonato Infantil de F&uacute;tbol &#8211; Noviembre 2019 &#8211; Club de Regatas Bella Vista</title>
	<link rel='dns-prefetch' href='//fonts.googleapis.com' />
	
	<link rel='stylesheet' id='style-css'  href='../css/style.css' type='text/css' media='all' />
	<link rel='stylesheet' id='framework-css'  href='../css/framework.css' type='text/css' media='all' />
	<link rel='stylesheet' id='style002-css'  href='../css/style002.css' type='text/css' media='all' />
	<link rel='stylesheet' id='sportspress'  href='../css/sportspress-sponsors.css' type='text/css' media='all' />
	<link rel='stylesheet' id='agregado-css'  href='../css/agregado.css' type='text/css' media='all' />

	<link rel="stylesheet" type="text/css" href="../css/themes/smoothness/jquery-ui-1.8.4.custom.css" />
	
	<script type="text/javascript" src="../codigo/jquery-1.8.2.js"></script>
	<script type="text/javascript" src="../codigo/jquery-ui-1.9.0.custom.min.js"></script>
		
	<script type="text/javascript" src="codigo/login.js"></script>
	
</head>

<body class="home page-template-default page page-id-242 custom-background">

<div class="sp-header"></div>
<div id="page" class="hfeed site">


	<header id="masthead" class="site-header" role="banner">
				<div class="header-area header-area-has-search">
				<div class="site-branding site-branding-empty">
					<div class="site-identity"></div>
				</div><!-- .site-branding -->
												
												<div class="site-menu">
	<nav id="site-navigation" class="main-navigation" role="navigation">
					
						
	<div class="menu-principal-container">
	<ul id="menu-principal" class="menu">

		<li id="menu-item-255" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-255"><a href="index.php">Inicio</a></li>
		
	</ul>
	</div>
	
	</nav>	
				</div>
						</div>
		</header><!-- #masthead -->

	<div id="content" class="site-content">
		
	<div id="primary" class="content-area-full-width content-area-right-sidebar">
		<main id="main" class="site-main" role="main">

			
				
<article id="post-242" class="post-242 page type-page status-publish hentry">
	<header class="entry-header">
				
		<h1 class="entry-title">Ingreso a la Administraci&oacute;n del Sistema</h1>	</header><!-- .entry-header -->

	<div class="entry-content">
		
<div id="divCarga" class="sportspress sp-widget-align-none">
	<div class="sp-template sp-template-league-table">
	<h4 class="sp-table-caption">Ingreso</h4>
	
	
	<div class="sp-table-wrapper">
	<table>
	<tr><td colspan='2'></td></tr>
	<tbody>
					<tr >
						<td class="formD">Usuario:</td>
						<td class="formC"><input type="text" maxlength="10" id="usuario" name="usuario" size="25" /> *
						</td>
					</tr>
					<tr>
						<td class="formD">Clave:</td>
						<td class="formC"><input type="password" maxlength="100" id="password"
							name="password" size="25" /> *
						</td>
					</tr>
				
	<tr><td colspan='2'></td></tr>
	</tbody>
	</table>
	</div>
	
<div class="msgerror" id="divmsgerror" name="divmsgerror"> </div>
	<div id="div_inscribir">		
		<input id="botonIngresar" name="botonIngresar" type="button" value="Ingresar" />
	</div>		
	
</div>



		</div><!-- .entry-content -->
</article><!-- #post-## -->
			
		</main><!-- #main -->
	</div><!-- #primary -->



			
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-area">
			<div id="quaternary" class="footer-widgets" role="complementary">
		
		<div class="footer-widget-region">
									
		
		</div>
		
		
	<div class="footer-widget-region">
		<div class="sp-widget-align-none">
		
		
	
	</div></div>
									
							</div>
		</div><!-- .footer-area -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<p>&nbsp;</p>

<div id="dialog" title="Atenci&oacute;n">
Cargando...	
</div>


</body>
