
<script type="text/javascript" src="codigo/editajugador.js"></script>

<?php

require_once "../../codigo/connw.php";

$dni = $_GET["dni"];  


$sql0 = "SELECT je.DNIjugador, j.nombreApellido, je.puesto, je.nivel, je.respEntrenador 
FROM jugador_equipo je 
inner join jugador j
on j.DNIjugador = je.DNIjugador
where je.DNIjugador ='$dni';";

$result = $mysqli->query($sql0);
$row = $result->fetch_assoc();

$nombreApellido = $row['nombreApellido']; 
$puesto = $row['puesto']; 
$nivel = $row['nivel']; 
$capitan = $row['respEntrenador']; 

?>

<b>Edita Juagdor</b>
<div class="sp-table-wrapper">

	<table>
	<tr><td colspan='2'></td></tr>
	<tbody>
					<tr>
						<td class="formD" style=" width: 50%;">DNI Jugador:</td>
						<td class="formC"><input type="text" maxlength="12" id="DNIjugador" name="DNIjugador" size="10" value="<?php echo $dni; ?>"  disabled/> 
						</td>
					</tr>
					<tr>
						<td class="formD" style=" width: 50%;">Nombre y Apellido:</td>
						<td class="formC"><input type="text" maxlength="12" id="nombreApellido" name="nombreApellido" size="30" value="<?php echo $nombreApellido; ?>"  disabled/> 
						</td>
					</tr>
					<tr>
						<td class="formD">Puesto:</td>
						<td class="formC">
                            <select id="puesto" name="puesto">
								<option value="Arq" <?php if($puesto=='Arq'){echo('selected');} ?>>Arq</option>
								<option value="Def" <?php if($puesto=='Def'){echo('selected');} ?>>Def</option>
								<option value="Med" <?php if($puesto=='Med'){echo('selected');} ?>>Med</option>
								<option value="Del" <?php if($puesto=='Del'){echo('selected');} ?>>Del</option>
                            </select> 
						</td>
					</tr>
					<tr>
						<td class="formD">Nivel:</td>
						<td class="formC">
                            <select id="nivel" name="nivel">
								<option value="1" <?php if($nivel=='1'){echo('selected');} ?>>1</option>
								<option value="2" <?php if($nivel=='2'){echo('selected');} ?>>2</option>
								<option value="3" <?php if($nivel=='3'){echo('selected');} ?>>3</option>
								<option value="4" <?php if($nivel=='4'){echo('selected');} ?>>4</option>
                            </select> 
						</td>
					</tr>
					<tr>
						<td class="formD">Capit&aacute;n:</td>
						<td class="formC">
                            <select id="capitan" name="capitan">
								<option value="0" <?php if($capitan=='0'){echo('selected');} ?>>No</option>
								<option value="1" <?php if($capitan=='1'){echo('selected');} ?>>Si</option>
                            </select> 
						</td>
					</tr>
					
					
	</tbody>
	</table>
	</div>
	<div id="div_inscribir">		
		<input id="botonCancelar" name="botonCancelar" type="button" value="Cancelar" />
		<input id="botonGuardar" name="botonGuardar" type="button" value="Guardar" />
	</div>		

	<input type="hidden"  id="idequipo" name="idequipo" value="<?php echo($idequipo); ?>" />