<script type="text/javascript" src="codigo/tablapartidosjuv.js"></script>

    
<?php 

require_once "../../codigo/connr.php"; 

if (isset($_GET["categoria"])){
    $cat = $_GET["categoria"];    
}else{
    $cat = "";
}


if ($cat!=""){
	 ?>
	 
	 <style>
		#ui-datepicker-div { display: none; }
	 </style>
	 
	 <br>
	 <table>
		<tr style="background-color: #e1e1e1;">
			<td>Equipo 1:</td>
			<td>&nbsp;</td>
			<td>Equipo 2:</td>
			<td>Dia:</td>
			<td>Hora:</td>
            <td>Cancha:</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td >
				<select id="NewEquipo1" name="NewEquipo1" style="width:150px;"> 
				<?php $sql1 = "SELECT idequipo, equipoDesc, zona
					FROM equipo_juvenil where idtorneo = (select idtorneo from torneo where estado='A')";
					if($cat!=""){$sql1 .= "and idcategoria='".$cat."' ";}
					$sql1 .= "order by idequipo";
					echo $sql1;
					$result1 = $mysqli->query($sql1);
					echo "<option value=''>Seleccione</option>";
					while ($row1 = $result1->fetch_assoc()){
					echo "<option value='".$row1['idequipo']."' >".$row1['equipoDesc']." (".substr($row1['zona'],0,3).")"."</option>";
				}?>
				</select> 
			</td>
			<td>
			VS.
			</td>
			<td>
				<select id="NewEquipo2" name="NewEquipo2" style="width:150px;">
				<?php $sql1 = "SELECT idequipo, equipoDesc, zona
					FROM equipo_juvenil where idtorneo = (select idtorneo from torneo where estado='A')";
					if($cat!=""){$sql1 .= "and idcategoria='".$cat."' ";}
					$sql1 .= "order by idequipo";
					echo $sql1;
					$result1 = $mysqli->query($sql1);
					echo "<option value=''>Seleccione</option>";
					while ($row1 = $result1->fetch_assoc()){
					echo "<option value='".$row1['idequipo']."' >".$row1['equipoDesc']." (".substr($row1['zona'],0,3).")"."</option>";
				}?>
				</select> 
			</td>
			<td>
				<input type="text" class="fecha" id="fechap" value="" size="12">
			</td>
			<td>
               <select id="horadesde" name="horadesde">
				<option value="07">07</option>
				<option value="08" selected >08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
				<option value="13">13</option>
				<option value="14">14</option>
				<option value="15">15</option>
				<option value="16">16</option>
				<option value="17">17</option>
				<option value="18">18</option>
				<option value="19">19</option>
				<option value="20">20</option>
				<option value="21">21</option>
				<option value="22">22</option>
			</select>
			:
	        	<select id="mindesde" name="mindesde">
				<option value="00" selected >00</option>
				<option value="15">15</option>
				<option value="30">30</option>
				<option value="45">45</option>
			</select>
			</td>
            <td>
				<input id="cancha" name="cancha" type="text" size="10" maxlength="100"/>
			</td>
			<td>
				<input id="btagregar" name="btagregar" type="button" value="Agregar" />
			</td>
		</tr>
		
	 </table>
	 
	

<?php 
	$sql = "select f.idfixture, f.equipo1, e1.equipoDesc as eDesc1, f.equipo2, e2.equipoDesc as eDesc2, date(fecha)as dia, time(fecha) as hora, f.zona1, f.zona2, f.resultado1, f.resultado2, f.estado, f.observaciones, f.cancha
			FROM fixture_juvenil f
			inner join equipo_juvenil e1
			on f.equipo1 = e1.idequipo
			inner join equipo_juvenil e2
			on f.equipo2 = e2.idequipo
			where f.idtorneo = (select idtorneo from torneo where estado='A') ";
            if($cat!=""){$sql .= "and f.idcategoria='".$cat."' ";}
			$sql .= "order by dia desc, hora desc, estado";

	if (!$result = $mysqli->query($sql)) {
		echo "Lo sentimos, este sitio web está experimentando problemas.";
		exit;
	}
	?>
    
	
	<span style="font-size:10px;"><u>Nota</u>: Para editar el partido, haga <b>doble click</b> sobre el mismo.</span>
    <br><br>
    	<table id="datatables" class="display dataTable" style="font-size: 11px;">
		<thead>
		<tr style="background-color: #c1c1c1;">
			<th>Equipo 1</th>
            <th>Z</th>
			<th>Resultado 1</th>
			<th>Equipo 2</th>
            <th>Z</th>
			<th>Resultado 2</th>
			<th>Dia</th>
			<th>Hora</th>
            <th>Cancha</th>
			<th>Estado</th>
			<th>Observaciones</th>
		</tr>
		</thead>
		<tbody>
		<?php
			while ($row = $result->fetch_assoc()) {
		?>
		<tr class="clickable-row" data-href="<?php echo $row['idfixture']?>">
			<td><?php echo $row['eDesc1']?></td>
            <td><?php echo substr($row['zona1'],0,3)?></td>
			<td><input type="text" id="Res1" size="2" style="text-align: center;" value="<?php echo $row['resultado1']?>" disabled></td>
			<td><?php echo $row['eDesc2']?></td>
            <td><?php echo substr($row['zona2'],0,3)?></td>
			<td><input type="text" id="Res2" size="2" style="text-align: center;" value="<?php echo $row['resultado2']?>" disabled></td>
            <td><?php echo $row['dia']?></td>
			<td><?php echo $row['hora']?></td>
            <td><?php echo $row['cancha']?></td>
			<td><?php echo $row['estado']?></td>
			<td><?php echo $row['observaciones']?></td>
			</tr>
			<?php }?>
		</tbody>
	</table>
    </br>
    <input id="txtcategoria" name="txtcategoria" type="text" hidden="hidden" value="<?php echo($cat); ?>"/>
    <input id="btpdf" name="btpdf" type="button" value="Fixture en PDF" />
	&nbsp;&nbsp;
	<input id="btregpdf" name="btregpdf" type="button" value="Registros de los partidos" />
	&nbsp;&nbsp;&nbsp;&nbsp;
    <input id="btActualizatabla" name="btActualizatabla" type="button" value="Actualiza Tabla Posiciones" />

<?php 
}
?>
