$(document).ready(function() {
	
    
	$("#botonAgrPagos").click(function(){
		var dniR = $("#dniR").val();
		var idtorneo = $("#idtorneo").val();
		$("#dialog").load("codigo/agregapagojuv.php?dniR="+dniR+"&idtorneo="+idtorneo);
		$("#dialog").dialog("open");
		
	});

    $(".cruzelim").click(function() {
		if (confirm('Está seguro de eliminar el pago?')) {
			data = "idpago="+$(this).attr('name');
			$.ajax({
                type: "GET",
                url: "codigo/eliminapagojuv.php",
                data: data,
                success: function(result) {
					if(result=="Ok."){
						$("#botonBusc").click();
					}else{
						alert("Ocurri&oacute; un error al borrar");
					}
                } 
            }); 
		} 
	});
	
	$(".ckhab").click(function() {
		var idequipo = $("#idequipo").val();
		var aprob = $(this).attr("checked") ? 1 : 0;
		data = "dni="+$(this).attr('id')+"&idequipo="+idequipo+"&aprob="+aprob;
		//alert(data);
		$.ajax({
                type: "GET",
                url: "codigo/habilitaequipojuv.php",
                data: data,
                success: function(result) {
					if(result=="Ok."){
						$("#botonBusc").click();
					}else{
						alert("Ocurri&oacute; un error al actualizar.");
					}
                } 
        }); 
	});	
	
	/*
	 $(".clickable-row").dblclick(function() {
		var dni = $(this).data("href");
			$("#dialog").load("codigo/editaInscripto.php?dni="+dni);
			$("#dialog").dialog("open");
		
    });
	*/
	/*
	$("#btCalculaDeuda").click(function() {
		var dniR = 'dni='+$("#dniResponsable").val();
			$.ajax({
                type: "GET",
				async: false,
				cache: false,
                url: "../inscripcion/actualizamontos.php",
                data: dniR,
                success: function(result) {
					$("#botonBusc").click();
                } 
        }); 
	});
	*/
	

});
