$(document).ready(function() {

	$("#botonGuardar").click(function(){
		
		if ($("#fechaPago").val()!="" && $("#monto").val()!="" && $.isNumeric($("#monto").val())){
			
			var fecha = $("#fechaPago").val();
			var fechabien = fecha.substring(6,10)+"-"+fecha.substring(3,5) +"-"+fecha.substring(0,2);
			var data = 'dniR='+$("#dniResponsable").val()
			+'&fechaPago='+fechabien
			+"&lugarPago="+$("#lugarPago").val()
			+"&recibo="+$("#recibo").val()
			+"&monto="+$("#monto").val()
			+"&idtorneo="+$("#idtorneo").val()
			;
		
			$.ajax({
                type: "GET",
                url: "codigo/guardapagojuv.php",
                data: data,
                success: function(result) {
					if(result=="Ok."){
						$("#botonBusc").click();
						$("#dialog").dialog("close");
						alert("Atencion: No se olvide de habilitar al equipo.");
					}else{
						alert("Ocurri&oacute; un error al guardar");
					}
                } 
            });
		}else{
			alert("Falta completar datos o son erroneos.")
		}
	});
	
	$("#botonCancelar").click(function(){
		$("#dialog").dialog("close");
	});
	
	
});