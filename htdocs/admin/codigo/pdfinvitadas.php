<?php

include ('../../inscripcion/fpdf/fpdf.php');
require_once "../../codigo/connr.php"; 
if (isset($_GET["categoria"])){
    $cat = $_GET["categoria"];    
}else{
    $cat = "";
}

//echo "cat ".$cat;

	$sql = "select i.DNIjugador, i.nombreApellido, i.idcategoria, i.socio, ic.chica, ic.mail
            from inscriptos i
            inner join invitacion_chicas ic
            on i.DNIjugador = ic.dni
			where idtorneo = (select idtorneo from torneo where estado='A') ";
            if($cat!=""){$sql .= "and idcategoria='".$cat."' ";}
			$sql .= "order by idcategoria, DNIjugador";


	$result = $mysqli->query($sql);

//echo $sql;
$pdf = new FPDF();
$pdf->AddPage();
$pdf->Ln(10);
$pdf->SetFont('Arial','',16);
$pdf->Write(5,utf8_decode("   - 49° Campeonato Infantil de Fútbol del Club Regatas Bella Vista -"));
$pdf->Ln(10);
$pdf->SetFont('Arial','',12);
$pdf->Write(5,utf8_decode("Listado de Invitadas"));
$pdf->SetFont('Arial','',10);
if($cat!=""){
    $pdf->Ln(5);
    $pdf->Write(5,utf8_decode("Categoria: ".$cat));
    $pdf->SetFont('Arial','B',10);    
}

$pdf->Ln(5);

$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(200,200,200);
$pdf->Cell(20,5,"DNI",1,0,'C','true');
$pdf->Cell(60,5,"Apellido y Nombre",1,0,'C','true');
$pdf->Cell(10,5,"Cat.",1,0,'C','true');
$pdf->Cell(10,5,"Socio",1,0,'C','true');
$pdf->Cell(40,5,"Nombre Invitada",1,0,'C','true');
$pdf->Cell(40,5,"Mail invitada",1,0,'C','true');
$pdf->Ln();


while ($row = $result->fetch_assoc()) {

	$pdf->SetFont('Arial','',8);	
	$pdf->Cell(20,5,$row['DNIjugador'],1,0,'C');
	$pdf->Cell(60,5,utf8_decode($row['nombreApellido']),1);
	$pdf->Cell(10,5,$row['idcategoria'],1,0,'C');
	if ($row['socio']=='0'){$ss="No";}else{$ss="Si";}
	$pdf->Cell(10,5,$ss,1,0,'C');
	$pdf->Cell(40,5,utf8_decode($row['chica']),1);
    $pdf->Cell(40,5,utf8_decode($row['mail']),1);
	$pdf->Ln();
}
$pdf->Ln(10);

$pdf->Output('Invitadas.pdf','D');
exit;

?>
