$(document).ready(function() {
	
    
	$("#botonAgrPagos").click(function(){
		var dniR = $("#dniR").val();
		var idtorneo = $("#idtorneo").val();
		$("#dialog").load("codigo/agregapago.php?dniR="+dniR+"&idtorneo="+idtorneo);
		$("#dialog").dialog("open");
		
	});

    $(".cruzelim").click(function() {
		if (confirm('Está seguro de eliminar el pago?')) {
			data = "idpago="+$(this).attr('name');
			$.ajax({
                type: "GET",
                url: "codigo/eliminapago.php",
                data: data,
                success: function(result) {
					if(result=="Ok."){
						$("#botonBusc").click();
					}else{
						alert("Ocurri&oacute; un error al borrar");
					}
                } 
            }); 
		} 
	});
	
	$(".ckhab").click(function() {
		var idtorneo = $("#idtorneo").val();
		var aprob = $(this).attr("checked") ? 1 : 0;
		data = "dni="+$(this).attr('id')+"&idtorneo="+idtorneo+"&aprob="+aprob;
		//alert(data);
		$.ajax({
                type: "GET",
                url: "codigo/habilitajugador.php",
                data: data,
                success: function(result) {
					if(result=="Ok."){
						$("#botonBusc").click();
					}else{
						alert("Ocurri&oacute; un error al actualizar.");
					}
                } 
        }); 
	});	
	
	 $(".clickable-row").dblclick(function() {
		var dni = $(this).data("href");
		/*var hab = $(this).data("hab");
		if(hab=="1"){
			alert("Para editar el jugador No tiene que estar habilitado");
		}else{
			$("#dialog").load("codigo/editaInscripto.php?dni="+dni);
			$("#dialog").dialog("open");
		}
		*/
			$("#dialog").load("codigo/editaInscripto.php?dni="+dni);
			$("#dialog").dialog("open");
		
    });

	
	$("#btCalculaDeuda").click(function() {
		var dniR = 'dni='+$("#dniR").val();
			$.ajax({
                type: "GET",
				async: false,
				cache: false,
                url: "../inscripcion/actualizamontos.php",
                data: dniR,
                success: function(result) {
					$("#botonBusc").click();
                } 
        }); 
	});

	

});
