<?php
include ('fpdf/fpdf.php');
require_once "../../codigo/connr.php";
$cat = $_GET["categoria"];
$ahora = date('d-m-Y H:i:s');

class PDF extends FPDF
{
function Footer()
{
    $this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,utf8_decode('Página ').$this->PageNo(),0,0,'C');
}
}

$pdf = new PDF();
$pdf->AddPage();

$pdf->SetFont('Arial','',16);
$pdf->Write(5,utf8_decode("   - 49° Campeonato Infantil de Fútbol del Club Regatas Bella Vista -"));
$pdf->Ln(10);
$pdf->SetFont('Arial','',14);
$pdf->Write(5,utf8_decode("Fixture - Categoría: "));
$pdf->Write(5,utf8_decode($cat));
$pdf->Ln();
$pdf->SetFont('Arial','',8);
$pdf->Write(5,$ahora);
$pdf->Ln(10);



$pdf->SetFillColor(200,200,200);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(35,6,"Equipo 1",1,0,'C','true');
$pdf->Cell(5,6,"Z",1,0,'C','true');
$pdf->Cell(15,6,"Resultado",1,0,'C','true');
$pdf->Cell(35,6,"Equipo 2",1,0,'C','true');
$pdf->Cell(5,6,"Z",1,0,'C','true');
$pdf->Cell(15,6,"Resultado",1,0,'C','true');
$pdf->Cell(15,6,utf8_decode("Día"),1,0,'C','true');
$pdf->Cell(15,6,"Hora",1,0,'C','true');
$pdf->Cell(10,6,"Cancha",1,0,'C','true');
$pdf->Cell(8,6,"Estado",1,0,'C','true');
$pdf->Cell(35,6,"Observaciones",1,0,'C','true');

$pdf->Ln();

    $sql2 = "select f.idfixture, f.equipo1, e1.equipoDesc as eDesc1, f.equipo2, e2.equipoDesc as eDesc2, date(fecha)as dia, time(fecha) as hora, 
			f.zona1, f.zona2,f.resultado1, f.resultado2, f.estado, f.observaciones, f.cancha
			FROM fixture f
			inner join equipo e1
			on f.equipo1 = e1.idequipo
			inner join equipo e2
			on f.equipo2 = e2.idequipo
			where f.idtorneo = (select idtorneo from torneo where estado='A')
            and f.idcategoria='".$cat."' 
			order by dia desc, hora desc, estado;";

    $result2 = $mysqli->query($sql2);

    while ($row2 = $result2->fetch_assoc()) {
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(35,6,$row2['eDesc1'],1,0,'L','true');
        $pdf->Cell(5,6,$row2['zona1'],1,0,'C','true');
        $pdf->Cell(15,6,$row2['resultado1'],1,0,'C','true');
		$pdf->Cell(35,6,$row2['eDesc2'],1,0,'L','true');
        $pdf->Cell(5,6,$row2['zona2'],1,0,'C','true');
        $pdf->Cell(15,6,$row2['resultado2'],1,0,'C','true');
		$pdf->Cell(15,6,$row2['dia'],1,0,'C','true');
		$pdf->Cell(15,6,$row2['hora'],1,0,'C','true');
		$pdf->Cell(10,6,$row2['cancha'],1,0,'C','true');
		$pdf->Cell(8,6,$row2['estado'],1,0,'C','true');
		$pdf->Cell(35,6,$row2['observaciones'],1,0,'C','true');
        $pdf->Ln();
    }
    $pdf->Ln();


$pdf->Output('Fixture.pdf','I');
exit;
?>
