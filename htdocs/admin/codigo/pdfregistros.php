<?php
include ('fpdf/fpdf.php');
require_once "../../codigo/connr.php";
$cat = $_GET["categoria"];
$ahora = date('d-m-Y H:i:s');

class PDF extends FPDF
{
function Footer()
{
    $this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,utf8_decode('Página ').$this->PageNo(),0,0,'C');
}
}

$pdf = new PDF();

    $sql2 = "select f.idfixture, f.equipo1, e1.equipoDesc as eDesc1, f.equipo2, e2.equipoDesc as eDesc2, date(fecha)as dia, time(fecha) as hora, 
			f.zona1, f.zona2,f.resultado1, f.resultado2, f.estado, f.observaciones, f.cancha
			FROM fixture_juvenil f
			inner join equipo_juvenil e1
			on f.equipo1 = e1.idequipo
			inner join equipo_juvenil e2
			on f.equipo2 = e2.idequipo
			where f.idtorneo = (select idtorneo from torneo where estado='A')
            and f.idcategoria='".$cat."' 
			order by dia desc, hora desc, estado;";

    $result2 = $mysqli->query($sql2);

    while ($row2 = $result2->fetch_assoc()) {
		$pdf->AddPage();
		$pdf->Ln();
		$pdf->SetFont('Arial','',16);
		$pdf->Write(5,utf8_decode("   - 49° Campeonato Infantil de Fútbol del Club Regatas Bella Vista -"));
		$pdf->Ln(10);
		$pdf->SetFont('Arial','',14);
		$pdf->Write(5,utf8_decode("Plantilla de Registros - Categoría: "));
		$pdf->Write(5,utf8_decode($cat));
		$pdf->Ln();
		$pdf->SetFont('Arial','',8);
		$pdf->Write(5,$ahora);
		$pdf->Ln(5);

		$pdf->SetFont('Arial','',12);
		$pdf->Write(5,utf8_decode("Partido:"));
		$pdf->Ln();
		
		$pdf->SetFillColor(200,200,200);
		$pdf->SetFont('Arial','B',6);
		$pdf->Cell(35,6,"Equipo 1",1,0,'C','true');
		$pdf->Cell(5,6,"Z",1,0,'C','true');
		$pdf->Cell(15,6,"Resultado",1,0,'C','true');
		$pdf->Cell(35,6,"Equipo 2",1,0,'C','true');
		$pdf->Cell(5,6,"Z",1,0,'C','true');
		$pdf->Cell(15,6,"Resultado",1,0,'C','true');
		$pdf->Cell(15,6,utf8_decode("Día"),1,0,'C','true');
		$pdf->Cell(15,6,"Hora",1,0,'C','true');
		$pdf->Cell(10,6,"Cancha",1,0,'C','true');
		$pdf->Cell(8,6,"Estado",1,0,'C','true');
		$pdf->Cell(35,6,"Observaciones",1,0,'C','true');
		$pdf->Ln();
		
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('Arial','',6);
        $pdf->Cell(35,6,utf8_decode($row2['eDesc1']),1,0,'L','true');
        $pdf->Cell(5,6,$row2['zona1'],1,0,'C','true');
        $pdf->Cell(15,6,$row2['resultado1'],1,0,'C','true');
		$pdf->Cell(35,6,utf8_decode($row2['eDesc2']),1,0,'L','true');
        $pdf->Cell(5,6,$row2['zona2'],1,0,'C','true');
        $pdf->Cell(15,6,$row2['resultado2'],1,0,'C','true');
		$pdf->Cell(15,6,$row2['dia'],1,0,'C','true');
		$pdf->Cell(15,6,$row2['hora'],1,0,'C','true');
		$pdf->Cell(10,6,$row2['cancha'],1,0,'C','true');
		$pdf->Cell(8,6,$row2['estado'],1,0,'C','true');
		$pdf->Cell(35,6,$row2['observaciones'],1,0,'C','true');
        $pdf->Ln();
		$pdf->Ln();
		
		
		$sql = "SELECT 
        rj.respDNI, rj.password, rj.nombreApellido, rj.respTelefono, rj.respMail,
        e.idequipo, e.equipoDesc, e.idcategoria, e.zona, c.q
        FROM responsable_juvenil rj
        inner join responsable_equipo_juvenil rej
        on rj.respDNI = rej.respDNI
        inner join equipo_juvenil e
        on e.idequipo = rej.idequipo
        left join (select idequipo, count(*) as q
            from jugador_equipo_juvenil
            group by idequipo)c
        on c.idequipo = e.idequipo
		left join (select respDNI, sum(monto) as total
			from pagos_juvenil
			group by respDNI)d
		on rj.respDNI=d.respDNI
        where e.idtorneo = (select idtorneo from torneo where estado='A')
		and e.idequipo = ".$row2['equipo1']."
        order by e.idcategoria, e.zona, e.idequipo;";
		
		$result = $mysqli->query($sql);
		$row = $result->fetch_assoc();
		
			$pdf->SetFont('Arial','B',10);
			$pdf->SetFillColor(200,200,200);
			$pdf->Cell(20,6,"Equipo:",1,0,'C','true');
			$pdf->SetFillColor(255,255,255);
			$pdf->Cell(55,6,utf8_decode($row['equipoDesc']),1,0,'C','true');
			
			$pdf->SetFont('Arial','',8);
			$pdf->SetFillColor(200,200,200);
			$pdf->Cell(20,6,"Responsable:",1,0,'C','true');
			$pdf->SetFillColor(255,255,255);
			$pdf->Cell(40,6,utf8_decode($row['nombreApellido']),1,0,'C','true');
			
			$pdf->SetFillColor(200,200,200);
			$pdf->Cell(18,6,utf8_decode("Teléfono:"),1,0,'C','true');
			$pdf->SetFillColor(255,255,255);
			$pdf->Cell(40,6,$row['respTelefono'],1,0,'C','true');
			$pdf->Ln();
						
			$sql3 = "select j.DNIjugador, j.nombreApellido, j.fechaNacimiento, j.telefono, j.email, 
					case when j.socioClub='1' then 'Si' else 'No' end as socioClub
					from responsable_equipo_juvenil rej
					inner join equipo_juvenil e
					on e.idequipo=rej.idequipo
					inner join jugador_equipo_juvenil je
					on e.idequipo=je.idequipo
					inner join jugador j
					on j.DNIjugador = je.DNIjugador
					where e.idtorneo = (select idtorneo from torneo where estado ='A')
					and rej.respDNI = ".$row['respDNI']." order by j.DNIjugador;";

			$result3 = $mysqli->query($sql3);
			
			$pdf->SetFillColor(200,200,200);
			$pdf->SetFont('Arial','B',6);
			$pdf->Cell(5,6,"Pos",1,0,'C','true');
			$pdf->Cell(15,6,"DNI",1,0,'C','true');
			$pdf->Cell(55,6,"Nombre Apellido",1,0,'C','true');
			$pdf->Cell(20,6,"Goles",1,0,'C','true');
			$pdf->Cell(20,6,"Amarilla",1,0,'C','true');
			$pdf->Cell(20,6,"Roja",1,0,'C','true');
			$pdf->Cell(58,6,"Observaciones",1,0,'C','true');
			$pdf->Ln();
			
			$pos = 0;
			while ($row3 = $result3->fetch_assoc()) {
				$pos ++;
				$pdf->SetFillColor(255,255,255);
				$pdf->SetFont('Arial','',6);
				
				$pdf->Cell(5,6,$pos,1,0,'C','true');
				$pdf->Cell(15,6,$row3['DNIjugador'],1,0,'C','true');
				$pdf->Cell(55,6,utf8_decode($row3['nombreApellido']),1,0,'C','true');
				$pdf->Cell(20,6,"",1,0,'C','true');
				$pdf->Cell(20,6,"",1,0,'C','true');
				$pdf->Cell(20,6,"",1,0,'C','true');
				$pdf->Cell(58,6,"",1,0,'C','true');
				$pdf->Ln();
			}
			$pdf->Ln();
			
			$sql = "SELECT 
        rj.respDNI, rj.password, rj.nombreApellido, rj.respTelefono, rj.respMail,
        e.idequipo, e.equipoDesc, e.idcategoria, e.zona, c.q
        FROM responsable_juvenil rj
        inner join responsable_equipo_juvenil rej
        on rj.respDNI = rej.respDNI
        inner join equipo_juvenil e
        on e.idequipo = rej.idequipo
        left join (select idequipo, count(*) as q
            from jugador_equipo_juvenil
            group by idequipo)c
        on c.idequipo = e.idequipo
		left join (select respDNI, sum(monto) as total
			from pagos_juvenil
			group by respDNI)d
		on rj.respDNI=d.respDNI
        where e.idtorneo = (select idtorneo from torneo where estado='A')
		and e.idequipo = ".$row2['equipo2']."
        order by e.idcategoria, e.zona, e.idequipo;";
		
		$result = $mysqli->query($sql);
		$row = $result->fetch_assoc();
		
			$pdf->SetFont('Arial','B',10);
			$pdf->SetFillColor(200,200,200);
			$pdf->Cell(20,6,"Equipo:",1,0,'C','true');
			$pdf->SetFillColor(255,255,255);
			$pdf->Cell(55,6,utf8_decode($row['equipoDesc']),1,0,'C','true');
			
			$pdf->SetFont('Arial','',8);
			$pdf->SetFillColor(200,200,200);
			$pdf->Cell(20,6,"Responsable:",1,0,'C','true');
			$pdf->SetFillColor(255,255,255);
			$pdf->Cell(40,6,utf8_decode($row['nombreApellido']),1,0,'C','true');
			
			$pdf->SetFillColor(200,200,200);
			$pdf->Cell(18,6,utf8_decode("Teléfono:"),1,0,'C','true');
			$pdf->SetFillColor(255,255,255);
			$pdf->Cell(40,6,$row['respTelefono'],1,0,'C','true');
			$pdf->Ln();
						
			$sql3 = "select j.DNIjugador, j.nombreApellido, j.fechaNacimiento, j.telefono, j.email, 
					case when j.socioClub='1' then 'Si' else 'No' end as socioClub
					from responsable_equipo_juvenil rej
					inner join equipo_juvenil e
					on e.idequipo=rej.idequipo
					inner join jugador_equipo_juvenil je
					on e.idequipo=je.idequipo
					inner join jugador j
					on j.DNIjugador = je.DNIjugador
					where e.idtorneo = (select idtorneo from torneo where estado ='A')
					and rej.respDNI = ".$row['respDNI']." order by j.DNIjugador;";

			$result3 = $mysqli->query($sql3);
			
			$pdf->SetFillColor(200,200,200);
			$pdf->SetFont('Arial','B',6);
			$pdf->Cell(5,6,"Pos",1,0,'C','true');
			$pdf->Cell(15,6,"DNI",1,0,'C','true');
			$pdf->Cell(55,6,"Nombre Apellido",1,0,'C','true');
			$pdf->Cell(20,6,"Goles",1,0,'C','true');
			$pdf->Cell(20,6,"Amarilla",1,0,'C','true');
			$pdf->Cell(20,6,"Roja",1,0,'C','true');
			$pdf->Cell(58,6,"Observaciones",1,0,'C','true');
			$pdf->Ln();
			
			$pos = 0;
			while ($row3 = $result3->fetch_assoc()) {
				$pos ++;
				$pdf->SetFillColor(255,255,255);
				$pdf->SetFont('Arial','',6);
				
				$pdf->Cell(5,6,$pos,1,0,'C','true');
				$pdf->Cell(15,6,$row3['DNIjugador'],1,0,'C','true');
				$pdf->Cell(55,6,utf8_decode($row3['nombreApellido']),1,0,'C','true');
				$pdf->Cell(20,6,"",1,0,'C','true');
				$pdf->Cell(20,6,"",1,0,'C','true');
				$pdf->Cell(20,6,"",1,0,'C','true');
				$pdf->Cell(58,6,"",1,0,'C','true');
				$pdf->Ln();
			}
			$pdf->Ln();
			$pdf->Ln();
			

    }

$pdf->Output('Registros'.$cat.'.pdf','I');
exit;

?>
