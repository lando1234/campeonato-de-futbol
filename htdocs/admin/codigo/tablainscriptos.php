	<link rel="stylesheet" type="text/css" href="../css/themes/smoothness/jquery-ui-1.8.4.custom.css" />
	<link rel="stylesheet" type="text/css" href="../media/css/demo_table_jui.css" />
	
	<script type="text/javascript" src="../codigo/jquery-1.8.2.js"></script>
	<script type="text/javascript" src="../codigo/jquery-ui-1.9.0.custom.min.js"></script>
	<script type="text/javascript" src="../media/js/jquery.dataTables.js"></script>	
    <script type="text/javascript" src="codigo/tablainscriptos.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('.dataTable').dataTable({
        "sPaginationType":"full_numbers",
        "aaSorting":[[7, "asc"]],
        "bJQueryUI":true
	});
});

</script>
    
<?php 

require_once "../../codigo/connr.php"; 



if (isset($_GET["categoria"])){
    $cat = $_GET["categoria"];    
}else{
    $cat = "";
}
	$tot=0;
	$sql = "SELECT DNIjugador, nombreApellido, sexo, idcategoria, socio, respDNI, respEntrenador, fechahora, dto, monto, aprobado
			FROM inscriptos
			where idtorneo = (select idtorneo from torneo where estado='A') ";
			if($cat!=""){$sql .= "and idcategoria='".$cat."' ";}
			if($_GET["soloMujeres"] == "true"){$sql .= "and sexo ='F'";}
			$sql .= "order by fechahora";

	if (!$result = $mysqli->query($sql)) {
		echo "Lo sentimos, este sitio web está experimentando problemas.";
		exit;
	}
	?>
    
    	<table id="datatables" class="display dataTable" style="font-size: 11px;">
		<thead>
		<tr>
			<th>DNI Jugador</th>
			<th>Nombre y Apellido</th>
			<th>Sexo</th>
			<th>Cat.</th>
			<th>Socio</th>
			<th>Resp DNI</th>
			<th>Entrenador</th>
			<th>Fecha Insc.</th>
			<th>Dto</th>
			<th>Monto</th>
			<th>Aprobado</th>
		</tr>
		</thead>
		<tbody>
		<?php
			while ($row = $result->fetch_assoc()) {
		?>
		<tr class="clickable-row" data-href="<?php echo $row['DNIjugador']?>" data-hab="<?php echo $row['aprobado']?>">
			<td><?php echo $row['DNIjugador']?></td>
			<td><?php echo $row['nombreApellido']?></td>
			<td><?php echo $row['sexo']?></td>
			<td><?php echo $row['idcategoria']?></td>
			<td><?php echo $row['socio']?></td>
			<td><?php echo $row['respDNI']?></td>
			<td><?php echo $row['respEntrenador']?></td>
			<td><?php echo $row['fechahora']?></td>
			<td><?php echo $row['dto']?></td>
			<td><?php echo $row['monto']?></td>
			<td><?php echo $row['aprobado']?></td>
			</tr>
			<?php }?>
		</tbody>
	</table>
    <span style="font-size:10px;"><u>Nota</u>: Para editar el jugador, haga <b>doble click</b> sobre el mismo.</span>
	


