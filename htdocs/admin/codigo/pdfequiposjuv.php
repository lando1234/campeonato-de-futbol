<?php
include ('fpdf/fpdf.php');
require_once "../../codigo/connr.php";
$ahora = date('d-m-Y H:i:s');

class PDF extends FPDF
{
function Footer()
{
    $this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,utf8_decode('Página ').$this->PageNo(),0,0,'C');
}
}

$pdf = new PDF();

$sql = "SELECT 
        rj.respDNI, rj.password, rj.nombreApellido, rj.respTelefono, rj.respMail,
        e.idequipo, e.equipoDesc, e.idcategoria, e.zona, c.q
        FROM responsable_juvenil rj
        inner join responsable_equipo_juvenil rej
        on rj.respDNI = rej.respDNI
        inner join equipo_juvenil e
        on e.idequipo = rej.idequipo
        left join (select idequipo, count(*) as q
            from jugador_equipo_juvenil
            group by idequipo)c
        on c.idequipo = e.idequipo
		left join (select respDNI, sum(monto) as total
			from pagos_juvenil
			group by respDNI)d
		on rj.respDNI=d.respDNI
        where e.idtorneo = (select idtorneo from torneo where estado='A')
        order by e.idcategoria, e.zona, e.idequipo;";

$result = $mysqli->query($sql);

while ($row = $result->fetch_assoc()) {

$pdf->AddPage();

$pdf->SetFont('Arial','',16);
$pdf->Write(5,utf8_decode("   - 49° Campeonato Infantil de Fútbol del Club Regatas Bella Vista -"));
$pdf->Ln(10);
$pdf->SetFont('Arial','',14);
$pdf->Write(5,utf8_decode("Planilla de Equipos Juveniles"));
$pdf->Ln();
$pdf->SetFont('Arial','',8);
$pdf->Write(5,$ahora);
$pdf->Ln(10);

$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(200,200,200);
$pdf->Cell(20,6,"Equipo:",1,0,'C','true');
$pdf->SetFillColor(255,255,255);
$pdf->Cell(58,6,$row['equipoDesc'],1,0,'C','true');
$pdf->SetFillColor(200,200,200);
$pdf->Cell(20,6,"Zona:",1,0,'C','true');
$pdf->SetFillColor(255,255,255);
$pdf->Cell(10,6,$row['zona'],1,0,'C','true');
$pdf->SetFillColor(200,200,200);
$pdf->Cell(20,6,utf8_decode("Categoría:"),1,0,'C','true');
$pdf->SetFillColor(255,255,255);
$pdf->Cell(10,6,$row['idcategoria'],1,0,'C','true');
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('Arial','',12);
$pdf->Write(5,utf8_decode("Responsable:"));
$pdf->Ln();

$pdf->SetFont('Arial','',8);
$pdf->SetFillColor(200,200,200);
$pdf->Cell(52,6,"Nombre Apellido",1,0,'C','true');
$pdf->Cell(15,6,"DNI",1,0,'C','true');
$pdf->Cell(30,6,"Password",1,0,'C','true');
$pdf->Cell(40,6,utf8_decode("Teléfono"),1,0,'C','true');
$pdf->Cell(45,6,"Mail",1,0,'C','true');
$pdf->Cell(10,6,"Q Jug",1,0,'C','true');

$pdf->Ln();
$pdf->SetFillColor(255,255,255);
$pdf->Cell(52,6,$row['nombreApellido'],1,0,'C','true');
$pdf->Cell(15,6,$row['respDNI'],1,0,'C','true');
$pdf->Cell(30,6,utf8_decode($row['password']),1,0,'C','true');
$pdf->Cell(40,6,$row['respTelefono'],1,0,'C','true');
$pdf->Cell(45,6,$row['respMail'],1,0,'C','true');
$pdf->Cell(10,6,$row['q'],1,0,'C','true');

$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('Arial','',12);
$pdf->Write(5,utf8_decode("Jugadores:"));
$pdf->Ln();

$pdf->SetFillColor(200,200,200);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(5,6,"Pos",1,0,'C','true');
$pdf->Cell(15,6,"DNI",1,0,'C','true');
$pdf->Cell(52,6,"Nombre Apellido",1,0,'C','true');
$pdf->Cell(20,6,"F. Nac",1,0,'C','true');
$pdf->Cell(40,6,utf8_decode("Teléfono"),1,0,'C','true');
$pdf->Cell(50,6,"Mail",1,0,'C','true');
$pdf->Cell(10,6,"Socio",1,0,'C','true');
$pdf->Ln();


    $sql2 = "select j.DNIjugador, j.nombreApellido, j.fechaNacimiento, j.telefono, j.email, 
            case when j.socioClub='1' then 'Si' else 'No' end as socioClub
			from responsable_equipo_juvenil rej
			inner join equipo_juvenil e
			on e.idequipo=rej.idequipo
			inner join jugador_equipo_juvenil je
			on e.idequipo=je.idequipo
			inner join jugador j
			on j.DNIjugador = je.DNIjugador
			where e.idtorneo = (select idtorneo from torneo where estado ='A')
			and rej.respDNI = ".$row['respDNI']." order by j.DNIjugador;";

    $result2 = $mysqli->query($sql2);
    $pos = 0;
    while ($row2 = $result2->fetch_assoc()) {
        $pos ++;
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('Arial','',8);
        
        $pdf->Cell(5,6,$pos,1,0,'C','true');
        $pdf->Cell(15,6,$row2['DNIjugador'],1,0,'C','true');
        $pdf->Cell(52,6,utf8_decode($row2['nombreApellido']),1,0,'C','true');
        $pdf->Cell(20,6,$row2['fechaNacimiento'],1,0,'C','true');
        $pdf->Cell(40,6,$row2['telefono'],1,0,'C','true');
        $pdf->Cell(50,6,$row2['email'],1,0,'C','true');
        $pdf->Cell(10,6,$row2['socioClub'],1,0,'C','true');
        $pdf->Ln();
    }
    $pdf->Ln();

}

$pdf->Output('Equiposjuvenil.pdf','I');
exit;
?>
