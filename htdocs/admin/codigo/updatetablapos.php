<?php

require_once "../../codigo/connw.php";

			
if (isset($_POST["idcategoria"])){
    $idcategoria = $_POST["idcategoria"];

	$sql0 = "select idtorneo from torneo where estado ='A';";
	$result = $mysqli->query($sql0);
	$row = $result->fetch_assoc();
	$torneo = $row['idtorneo'];
	
	$sql1 ="delete from tabla_posiciones 
			where idtorneo = $torneo
			and idcategoria = '$idcategoria'";
	$mysqli->query($sql1);
	
	$sql2 ="INSERT INTO tabla_posiciones
		(idequipo,idtorneo,idcategoria,zona, equipoDesc,PJ,G,E,P,GF,GC,Dif,Pts)
		select 
		a.idequipo
		,'$torneo' as idtorneo
		,'$idcategoria' as idcategoria
		,izona as zona
		,e.equipoDesc
		,sum(pj) as pj
		,sum(g) as g
		,sum(e) as e
		,sum(p) as p
		,sum(gf) as gf
		,sum(gc) as gc
		,sum(gf) - sum(gc) as dif
		,sum(pts) as pts
		from (
		select f.equipo1 as idequipo
		,f.zona1 as izona
		,count(*)as pj
		,sum(case when f.resultado1 > f.resultado2 then 1 else 0 end) as g
		,sum(case when f.resultado1 = f.resultado2 then 1 else 0 end) as e
		,sum(case when f.resultado1 < f.resultado2 then 1 else 0 end) as p
		,sum(resultado1) as gf
		,sum(resultado2) as gc
		,sum(case when f.resultado1 > f.resultado2 then 3 when f.resultado1 < f.resultado2 then 0 else 1 end) as pts
		from fixture f
		where f.estado = 'J'
		and f.idtorneo = '$torneo'
		and f.idcategoria = '$idcategoria'
		group by f.equipo1, f.zona1
		union all
		select f.equipo2 as idequipo
		,f.zona2 as izona
		,count(*)as pj
		,sum(case when f.resultado2 > f.resultado1 then 1 else 0 end) as g
		,sum(case when f.resultado1 = f.resultado2 then 1 else 0 end) as e
		,sum(case when f.resultado2 < f.resultado1 then 1 else 0 end) as p
		,sum(resultado2) as gf
		,sum(resultado1) as gc
		,sum(case when f.resultado2 > f.resultado1 then 3 when f.resultado2 < f.resultado1 then 0 else 1 end) as pts
		from fixture f
		where f.estado = 'J'
		and f.idtorneo = '$torneo'
		and f.idcategoria = '$idcategoria'
		group by f.equipo2, f.zona2
		union all
		select idequipo,zona as izona,0,0,0,0,0,0,0
		from equipo
		where idtorneo = '$torneo'
		and idcategoria = '$idcategoria'
		)a
		inner join equipo e
		on a.idequipo = e.idequipo
		group by a.idequipo, a.izona";
		
	$mysqli->query($sql2);
	
	//echo ($sql2);
	echo "Ok.";
   
    
}

?>


