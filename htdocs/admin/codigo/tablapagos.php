<script type="text/javascript" src="codigo/tablapagos.js"></script>

<?php 

require_once "../../codigo/connw.php";

if (isset($_GET["dniresp"])){
    $dni = $_GET["dniresp"];    
}else{
    $dni = "";
}


	$sql1 ="select nombreApellido from responsable where respDNI = '$dni'";
	if (!$result1 = $mysqli->query($sql1)) {
		echo "Lo sentimos, este sitio web est&aacute; experimentando problemas.";
		exit;
	}
	$row1 = $result1->fetch_assoc();
	$nombreResp=$row1['nombreApellido'];

    if ($nombreResp!=""){
		?>
		</br>
        Detalle del Importe del Responsable: <b><?php echo $nombreResp; ?></b> (DNI: <?php echo $dni; ?>)</br></br>
        <input id="dniR" type="hidden" value="<?php echo $dni; ?>"/>
		
       <?php
        $sql = "SELECT idtorneo, respDNI,DNIjugador,nombreApellido,sexo,idcategoria,fechahora,socio,dto,monto,aprobado
        FROM inscriptos
        where respDNI='$dni'
        and idtorneo =(select idtorneo from torneo where estado='A')
        order by fechahora asc;
        ";

        if (!$result = $mysqli->query($sql)) {
            echo "Lo sentimos, este sitio web est&aacute; experimentando problemas.";
            exit;
        }
		$cant = $result->num_rows;
		if($cant>0){
        ?>
		<div>
        <table id="tbpagos" >
        <tr>
            <th>Dni Jugador</th>
            <th>Nombre y Apellido</th>
            <th>Sexo</th>
            <th>Categor&iacute;a</th>
            <th>Inscripto</th>
            <th>Socio</th>
            <th>Dto</th>
            <th>Monto</th>
			<th>Hab.</th>
        </tr>
        
        <?php
            $total = 0;
            while ($row = $result->fetch_assoc()) {
                $total = $total+$row['monto'];
        ?>
        <tr>
            <td class="clickable-row" data-href="<?php echo $row['DNIjugador']?>" data-hab="<?php echo $row['aprobado']?>" onmouseover="this.style.backgroundColor='#c1c1c1'" onmouseout="this.style.backgroundColor=''"><?php echo ($row['DNIjugador']);?></td>
            <td class="clickable-row" data-href="<?php echo $row['DNIjugador']?>" data-hab="<?php echo $row['aprobado']?>" onmouseover="this.style.backgroundColor='#c1c1c1'" onmouseout="this.style.backgroundColor=''"><?php echo ($row['nombreApellido']);?></td>
            <td class="clickable-row" data-href="<?php echo $row['DNIjugador']?>" data-hab="<?php echo $row['aprobado']?>" onmouseover="this.style.backgroundColor='#c1c1c1'" onmouseout="this.style.backgroundColor=''"><?php echo ($row['sexo']);?></td>
            <td class="clickable-row" data-href="<?php echo $row['DNIjugador']?>" data-hab="<?php echo $row['aprobado']?>" onmouseover="this.style.backgroundColor='#c1c1c1'" onmouseout="this.style.backgroundColor=''"><?php echo ($row['idcategoria']);?></td>
            <td class="clickable-row" data-href="<?php echo $row['DNIjugador']?>" data-hab="<?php echo $row['aprobado']?>" onmouseover="this.style.backgroundColor='#c1c1c1'" onmouseout="this.style.backgroundColor=''"><?php echo ($row['fechahora']);?></td>
            <td class="clickable-row" data-href="<?php echo $row['DNIjugador']?>" data-hab="<?php echo $row['aprobado']?>" onmouseover="this.style.backgroundColor='#c1c1c1'" onmouseout="this.style.backgroundColor=''"><?php if ($row['socio']=='0'){echo "No";}else{echo "Si";}?></td>
            <td><?php echo ($row['dto']);?></td>
            <td><?php echo ("$ ".$row['monto']);?></td>
			<td><input type="checkbox" class="ckhab" id="<?php echo ($row['DNIjugador']);?>" <?php if($row['aprobado']=='1'){echo 'checked';} ?>></td>
        </tr>
        <?php
			$idtorneo=$row['idtorneo'];
			} 
            $result->free();
            $result1->free();

        ?>
        <tr>
            <th colspan='7' style="text-align: right;">Total: </th>
            <th><?php echo("$ ".$total.".00");?></th>
			<th></th>
        </tr>
        </table>
      <span style="font-size:10px;"><u>Nota</u>: Para editar el jugador, haga <b>doble click</b> sobre el mismo.</span>
		<div style="float:right;padding:0px 15px;">
			<input style="text-align: center; padding:5px 10px;" id="btCalculaDeuda" name="btCalculaDeuda" type="button" value="Recalcular Deuda" />
		</div> 
	  </div>
        
		
		</br>
        <input id="idtorneo" type="hidden" value="<?php echo($idtorneo);?>"/>
		
        <div id="primary" class="content-area content-area-right-sidebar" style="min-height: 0;">
        <b>Detalle de Pagos:</b>
           
        <?php
            $sql4 = "SELECT idpago, fechaPago, lugarPago, recibo, monto 
			FROM pagos 
			where respDNI = '$dni' 
			and idtorneo = (select idtorneo from torneo where estado='A');";
            $result4 = $mysqli->query($sql4);
                
            $pagado = 0;
            if ($result4->num_rows>0){
            ?>    
                <table id="tbpagos" style="width: 100%;">
                <tr>
                    <th>Fecha</th>
                    <th>Lugar</th>
                    <th>Recibo</th>
					<th>Monto</th>
					<th>Borrar</th>
                </tr>
                    
            <?php
                    
                while ($row4 = $result4->fetch_assoc()) {
                    $pagado = $pagado+$row4['monto'];
					$fpago = substr($row4['fechaPago'],8,10).'/'.substr($row4['fechaPago'],5,2).'/'.substr($row4['fechaPago'],0,4); 
            ?>
                <tr>
                    <td><?php echo ($fpago);?></td>
                    <td><?php echo ($row4['lugarPago']);?></td>
					<td><?php echo ($row4['recibo']);?></td>
                    <td><?php echo ("$ ".$row4['monto'].".00");?></td>
					<td><img class="cruzelim" name="<?php echo ($row4['idpago']);?>" src="../images/eliminar.png" /></td>
                </tr>
                
            <?php }
                
            ?>  
                <tr>
                <th colspan='3' style="text-align: right;">Total: </th>
                <th><?php echo("$ ".$pagado.".00");?></th>
				<th></th>
                </tr>
                </table>      
            <?php
            }else{
                echo "<p>A&uacute;n no se registraron pagos.</p>";
            }
            
        ?>
			<div  style="text-align: center;">
			<input style="text-align: center;" id="botonAgrPagos" name="botonAgrPagos" type="button" value="Agregar Pagos" />
			</div>
        </div>
		
		<div id="secondary" class="widget-area widget-area-right" role="complementary">
		<p>
		<br>
		<table>
		<tr>
			<td>
				Pagado: <b><?php echo("$ ".$pagado.".00");?></b></br>
			</td>
		</tr>		
		<tr>
			<td>		
				Adeudado: <b><?php echo("$ ".($total-$pagado).".00");?></b>
			</td>
		</tr>		
		</table>
        </p>
			<div  id="mensajepago" name="mensajepago" style="text-align: center;">
				
			</div>
        </div>
        <?php
		}else{
			echo "</br></br>&nbsp;&nbsp;&nbsp;<font color='red'>No hay inscriptos bajo &eacute;ste responsable.</font>";
		}

    }else{
        echo "</br></br>&nbsp;&nbsp;&nbsp;<font color='red'>No se encontraron datos para ese DNI</font>";
    }
?>
