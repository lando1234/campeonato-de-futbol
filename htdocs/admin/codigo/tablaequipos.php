
<script type="text/javascript" src="codigo/tablaequipos.js"></script>

<?php 

require_once "../../codigo/connr.php"; 

if (isset($_GET["categoria"])){
    $cat = $_GET["categoria"];    
}else{
    $cat = "";
}

function color($puesto){
    switch ($puesto) {
    case 'Arq':
        return "style='background:#fffebe;'";
        break;
    case 'Def':
        return "style='background:#cafd93;'";
        break;
    case 'Med':
        return "style='background:#b2e3ff;'";
        break;
    case 'Del':
        return "style='background:#fdd6a4;'";
        break;
    default:
        return "";
        break;
    }

}

if ($cat!=""){
$totalj=0;    
?>


<div id="DivSinAsig" class="eq-sup" >
    <table>
        <tr>
            <td style="background:#c1c1c1;">Nivel:</td>
            <td>1 - Regular</td>
            <td>2 - Bueno</td>
            <td>3 - Muy Bueno</td>
            <td>4 - Excelente</td>
        </tr>
        <tr>
            <td style="background:#c1c1c1;">Puesto:</td>
            <td style="background:#fffebe;">Arq - Arquero</td>
            <td style="background:#cafd93;">Def - Defensor</td>
            <td style="background:#b2e3ff;">Med - Medio Campo</td>
            <td style="background:#fdd6a4;">Del - Delantero</td>
        </tr>
        <tr>
            <td style="background:#c1c1c1;">Capit&aacute;n:</td>
            <td style="background:#ddddff;">Nombre</td>
        </tr>
    </table>
        
</div>
<div id="DivSinAsig" class="eq-izq" >
    <?php
    $tot = 0;
    $sql = "SELECT i.DNIjugador, j.nombreApellido, j.puesto, j.nivel, i.respEntrenador, SUBSTRING(j.fechaNacimiento,1,4) as anio
            FROM inscriptos i
            inner join jugador j
            on i.DNIjugador = j.DNIjugador
            where i.idtorneo = (select idtorneo from torneo where estado = 'A')
            and i.DNIjugador not in (SELECT je.DNIjugador FROM equipo e inner join jugador_equipo je 
            on e.idequipo = je.idequipo where e.idtorneo = (select idtorneo from torneo where estado = 'A'))
            and  i.aprobado = '1'
            and i.idcategoria = '".$cat."' 
            order by j.puesto, j.nivel";
    if (!$result = $mysqli->query($sql)) {
        echo "Lo sentimos, este sitio web está experimentando problemas.";
        exit;
    }
    ?>
<input style="padding: 5px;" type="button" name="btnarmar" id="btnarmar" value="Auto" />
<input style="padding: 5px;" type="button" name="btnguardar" id="btnguardar" value="Guardar" />
<label id="wguardar" style="color: red; font-weight: bolder;"> </label>
<input style="padding: 5px;background-color: #dd5555;font-size: 12px;float: right;" type="button" name="btlimpiar" id="btlimpiar" value="Limpiar" />

</br></br>
Sin Asignacion:
<br>(Datos informados por el Responsable)    
    <table id='table-draggable0' name='table-draggable0' >  
                <tbody class="connectedSortable">  
                    <tr>
                        <th>Nombre</th>
                        <th>Pos</th>  
                        <th>Nivel</th>
						<th>A&ntilde;o</th>  						
                    </tr>
        <?php
            while ($row = $result->fetch_assoc()) {
                $totalj++;
		?>
                    <tr data-href='<?php echo $row['DNIjugador']; ?>' data-entrenador='<?php echo $row['respEntrenador']; ?>'>   
                        <td <?php if($row['respEntrenador']=='1'){echo ("style='background:#ddddff;'");} ?>><?php echo $row['nombreApellido']; ?></td>
                        <td <?php echo color(ucfirst(substr($row['puesto'],0,3))); ?>><?php echo ucfirst(substr($row['puesto'],0,3)); ?></td>
                        <td><?php echo $row['nivel']; $tot++; ?></td>
						<td><?php echo $row['anio'];?></td>
                    </tr> 
        <?php }?> 
                </tbody> 
        </table>
    <div id="divtotal0" name="divtotal0">Cantidad:&nbsp;&nbsp;<input type="text" size="2" value="<?php echo ($tot); ?>" style="text-align: center;"/></div>
    <input type="text" id="cant0" name="cant0" value="0" hidden="hidden">
    <input type="text" id="prom0" name="prom0" value="0" hidden="hidden">
    
</div>

<span style="font-size:10px;">Para editar un jugador, haga doble click sobre &eacute;l. Tienen que estar guardados. (Datos modificados)</span>
<div id="Divequipos" class="eq-der" >

<?php

    $tab=1;
    $abajo=0;
    $sql1 = "SELECT idequipo, equipoDesc, zona 
            FROM equipo
            where idtorneo = (select idtorneo from torneo where estado = 'A')
            and idcategoria = '".$cat."' ;";

    if (!$result1 = $mysqli->query($sql1)) {
        echo "Lo sentimos, este sitio web está experimentando problemas.";
        exit;
    }

    while ($row1 = $result1->fetch_assoc()) {
        ?>
    <div class='divtablaeq'>
    <?php echo "<b>".$row1['equipoDesc']."</b> - Zona: <b>".$row1['zona']."</b>"; ?>
    
    <img class="elimequipo" name="<?php echo ($row1['idequipo']);?>" src="../images/eliminar.png" />
    <input class="editaequipo" style="padding: 0px; font-size: 10px;" type="button" name="<?php echo ($row1['idequipo']);?>" value="Editar" />
    
        <table id='table-draggable<?php echo $tab; ?>' data-idequipo='<?php echo $row1['idequipo']; ?>' >
                <tbody class="connectedSortable">  
                    <tr>
                        <th>Nombre</th>
                        <th>Pos</th>  
                        <th>Nivel</th>
						<th>A&ntilde;o</th>						
                    </tr>
                        <?php
                        $sql2 = "SELECT j.DNIjugador, j.nombreApellido, je.puesto, je.nivel, je.respEntrenador, SUBSTRING(j.fechaNacimiento,1,4) as anio
                        FROM jugador_equipo je 
                        inner join jugador j on je.DNIjugador = j.DNIjugador
                        inner join inscriptos i on i.DNIjugador = j.DNIjugador
                        where i.aprobado = '1'
                        and i.idtorneo = (select idtorneo from torneo where estado = 'A')
                        and idequipo = ".$row1['idequipo']." order by je.puesto, je.nivel;";
                        $result2 = $mysqli->query($sql2);
                        while ($row2 = $result2->fetch_assoc()) {
                            $totalj++;
                        ?>
                            <tr data-href='<?php echo $row2['DNIjugador']; ?>' data-entrenador='<?php echo $row2['respEntrenador']; ?>'>      
                            <td <?php if($row2['respEntrenador']=='1'){echo ("style='background:#ddddff;'");} ?>><?php echo $row2['nombreApellido']; ?></td>
                            <td <?php echo color($row2['puesto']); ?>><?php echo ($row2['puesto']); ?></td>
                            <td><?php echo $row2['nivel']; ?></td>
							<td><?php echo $row2['anio']; ?></td>
                            </tr> 
                        <?php }?> 
                </tbody> 
        </table>
    <div id="divtotal<?php echo $tab; ?>" name="divtotal<?php echo $tab; ?>"></div>
    <input type="text" id="cant<?php echo $tab; ?>" name="cant<?php echo $tab; ?>" value="0" hidden="hidden" >
    <input type="text" id="prom<?php echo $tab; ?>" name="prom<?php echo $tab; ?>" value="0" hidden="hidden">
    </div>
    <?php 
        $tab +=1;
        $abajo +=1;
        if ($abajo==2){
            echo "<div style='clear: both;'></div>";
            $abajo=0;
        }
    }
    echo "<div style='clear: both;'></div>";
    echo("<input style='padding: 5px;' type='button' name='btnuevoequipo' id='btnuevoequipo' value='Agregar Equipo' />");
    
    echo("<input style='padding: 5px;float:right;' type='button' name='btpdf' id='btpdf' value='Bajar a PDF' />");
    
    echo("<input type='text' id='TotalJug' name='TotalJug' value='".$totalj."' hidden='hidden'>");
    echo("<input type='text' id='TotalEq' name='TotalEq' value='".($tab-1)."' hidden='hidden'>");
    echo("<input type='text' id='idcategoria' name='idcategoria' value='".$cat."' hidden='hidden'>");
    
}//fin del categoria vacio
?>

</div>

