<?php
include ('fpdf/fpdf.php');
require_once "../../codigo/connr.php";
$cat = $_GET["categoria"];
$ahora = date('d-m-Y H:i:s');

class PDF extends FPDF
{
function Footer()
{
    $this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,utf8_decode('Página ').$this->PageNo(),0,0,'C');
}
}

$pdf = new PDF();
$pdf->AddPage();

$pdf->SetFont('Arial','',16);
$pdf->Write(5,utf8_decode("   - 49° Campeonato Infantil de Fútbol del Club Regatas Bella Vista -"));
$pdf->Ln(10);
$pdf->SetFont('Arial','',14);
$pdf->Write(5,utf8_decode("Planilla de Equipos - Categoría: "));
$pdf->Write(5,utf8_decode($cat));
$pdf->Ln();
$pdf->SetFont('Arial','',8);
$pdf->Write(5,$ahora);
$pdf->Ln(10);

$sql = "SELECT idequipo, equipoDesc, zona 
            FROM equipo
            where idtorneo = (select idtorneo from torneo where estado = 'A')
            and idcategoria = '".$cat."' ;";

$result = $mysqli->query($sql);

$pag=0;
while ($row = $result->fetch_assoc()) {
	if($pag==2){
		$pdf->AddPage();
		$pdf->SetFont('Arial','',16);
		$pdf->Write(5,utf8_decode("   - 49° Campeonato Infantil de Fútbol del Club Regatas Bella Vista -"));
		$pdf->Ln(10);
		$pdf->SetFont('Arial','',14);
		$pdf->Write(5,utf8_decode("Planilla de Equipos - Categoría: "));
		$pdf->Write(5,utf8_decode($cat));
		$pdf->Ln();
		$pdf->SetFont('Arial','',8);
		$pdf->Write(5,$ahora);
		$pdf->Ln(10);
		$pag=0;
	}
	
$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(200,200,200);
$pdf->Cell(20,6,"Equipo:",1,0,'C','true');
$pdf->SetFillColor(255,255,255);
$pdf->Cell(58,6,$row['equipoDesc'],1,0,'C','true');
$pdf->SetFillColor(200,200,200);
$pdf->Cell(20,6,"Zona:",1,0,'C','true');
$pdf->SetFillColor(255,255,255);
$pdf->Cell(10,6,$row['zona'],1,0,'C','true');
$pdf->Ln();

$pdf->SetFillColor(200,200,200);
$pdf->SetFont('Arial','B',6);
$pdf->Cell(5,6,"Pos",1,0,'C','true');
$pdf->Cell(12,6,"DNI",1,0,'C','true');
$pdf->Cell(35,6,"Nombre Apellido",1,0,'C','true');
$pdf->Cell(8,6,"Puesto",1,0,'C','true');
$pdf->Cell(6,6,"Nivel",1,0,'C','true');
$pdf->Cell(6,6,utf8_decode("Cap."),1,0,'C','true');
$pdf->Cell(6,6,utf8_decode("Año"),1,0,'C','true');
$pdf->Cell(30,6,"Mail",1,0,'C','true');
$pdf->Cell(25,6,"Obra Social",1,0,'C','true');
$pdf->Cell(25,6,"Nro Afiliado",1,0,'C','true');
$pdf->Cell(35,6,"Tel Emergencias",1,0,'C','true');
$pdf->Ln();

    $sql2 = "SELECT j.DNIjugador, j.nombreApellido, je.puesto, je.nivel, 
    case when je.respEntrenador='0' then '' else 'SI' end as capitan,
     SUBSTRING(j.fechaNacimiento,1,4) as anio,
    j.email, j.obraSocial, j.afiliado, j.telEmergencias
    FROM jugador_equipo je 
    inner join jugador j on je.DNIjugador = j.DNIjugador
    inner join inscriptos i on i.DNIjugador = j.DNIjugador
    where i.aprobado = '1'
    and i.idtorneo = (select idtorneo from torneo where estado = 'A')
    and idequipo = ".$row['idequipo']." order by je.puesto, je.nivel;";

    $result2 = $mysqli->query($sql2);
	$pos = 0;
    while ($row2 = $result2->fetch_assoc()) {
		$pos ++;
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('Arial','',6);
		$pdf->Cell(5,6,$pos,1,0,'C','true');
        $pdf->Cell(12,6,$row2['DNIjugador'],1,0,'C','true');
        $pdf->Cell(35,6,$row2['nombreApellido'],1,0,'L','true');
        $pdf->Cell(8,6,$row2['puesto'],1,0,'C','true');
        $pdf->Cell(6,6,$row2['nivel'],1,0,'C','true');
        $pdf->Cell(6,6,$row2['capitan'],1,0,'C','true');
        $pdf->Cell(6,6,$row2['anio'],1,0,'C','true');
        $pdf->Cell(30,6,$row2['email'],1,0,'L','true');
        $pdf->Cell(25,6,$row2['obraSocial'],1,0,'L','true');
        $pdf->Cell(25,6,$row2['afiliado'],1,0,'C','true');
        $pdf->Cell(35,6,$row2['telEmergencias'],1,0,'C','true');
        $pdf->Ln();
    }
    $pdf->Ln();
    $pdf->Ln();
	$pag++;
}

$pdf->Output('Equipos.pdf','I');
exit;

?>
