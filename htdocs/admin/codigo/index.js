$(document).ready(function(){
	
	$("#dialog").dialog({
            autoOpen: false,
            modal: true,
            width: 450,
            height: 250,
            show: "show",
            hide: "hide"
	});
	
	$("#dialog").click(function() {
   		 	$("#dialog").html("");
	        $("#dialog").dialog("close");
        });

	$("#btcontacto").click(function(){
		$("#dialog").html("Por favor, env&iacute;e un mail a secretar&iacute;a con su consulta.</br></br>secretaria@regatasbellavista.com.ar</br></br>Muchas Gracias.");
		$("#dialog").dialog("open");
		
	}); 
								
	$(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
	
	
});		
