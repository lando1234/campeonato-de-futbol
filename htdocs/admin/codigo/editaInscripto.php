<?php 
include ("bloqueDeSeguridad.php");
require_once "../../codigo/connr.php"; 

if (isset($_GET["dni"])){
    $dni = $_GET["dni"];    
}else{
    $dni = "";
}

//echo "dni="+$dni;

$sql = "select j.DNIjugador, j.sexo, j.nombreApellido, j.fechaNacimiento, j.respDNI, j.localidad, j.email, 
j.calle, j.nro, j.piso, j.dto, j.telefono, j.colegio, j.obraSocial, j.afiliado, j.telEmergencias, j.puesto, 
j.nivel, j.escuelaFutbol, j.grado, i.idcategoria, i.socio, i.respEntrenador
from 
(SELECT DNIjugador, sexo, nombreApellido, fechaNacimiento, respDNI, localidad, email, calle, nro, piso, dto,
	telefono, colegio, obraSocial, afiliado, telEmergencias, puesto, nivel, escuelaFutbol, grado
	FROM jugador )j
left join
(SELECT DNIjugador, nombreApellido, sexo, idcategoria, socio, respDNI, respEntrenador
FROM inscriptos
where idtorneo = (select idtorneo from torneo where estado='A')
)i
on j.DNIjugador = i.DNIjugador
where j.DNIjugador = '$dni'";


	if (!$result = $mysqli->query($sql)) {
		echo "Lo sentimos, este sitio web esta experimentando problemas.";
	exit;
	}
	
	$row = $result->fetch_assoc();
	
	$fnac = substr($row['fechaNacimiento'],8,10).'/'.substr($row['fechaNacimiento'],5,2).'/'.substr($row['fechaNacimiento'],0,4); 
		
?>

<script type="text/javascript" src="codigo/editaInscripto.js"></script>

<b>Datos del Jugador</b>
<div name="divjugador" id="divjugador" class="sp-table-wrapper" >
	<table name="formulario" id="formulario">
	<tr><td colspan='2'></td></tr>
	<tbody>
	<tr>
						<td class="formD">DNI del Jugador:</td>
						<td class="formC"><input type="text" maxlength="10" id="dni" name="dni"
							size="43" disabled value="<?php echo $row['DNIjugador'];?>"/>
						</td>
					</tr>
					<tr>
						<td class="formD">Sexo:</td>
						<td class="formC">
						<input type="text" maxlength="10" id="sexo" name="sexo"size="43" disabled 
						value="<?php if($row['sexo']=='M'){echo "Masculino";}else{echo "Femenino";} ?>"/>
						</td>
					</tr>
					<tr>
						<td class="formD">Nombre y Apellido:</td>
						<td class="formC"><input type="text" maxlength="45" id="nombre_y_apellido"
							name="nombre_y_apellido" size="43" value="<?php echo $row['nombreApellido'];?>"/>
						</td>
					</tr>
					<tr>
						<td class="formD">Fecha de nacimiento: dd/mm/aaaa</td>
						<td class="formC"><input type="text" maxlength="10" id="fecha_de_nacimiento"
							name="fecha_de_nacimiento" size="43" value="<?php echo $fnac;?>"/>
						</td>

					</tr>
					<tr>
						<td class="formD">Categor&iacute;a:</td>
						<td class="formC"><input type="text" maxlength="45" id="categoria" name="categoria"
							size="43" value="<?php echo $row['idcategoria'];?>"/>
					</tr>
					<tr>
						<td class="formD">Email Jugador:</td>
						<td class="formC"><input type="text" maxlength="45" id="email" name="email"
							size="43" value="<?php echo $row['email'];?>"/>
						</td>
					</tr>
					<tr>
						<td class="formD">Localidad:</td>
						<td class="formC"><input type="text" maxlength="45" id="localidad" name="localidad"
							size="43" value="<?php echo $row['localidad'];?>"/>
						</td>
					</tr>
					<tr>
						<td class="formD">Calle:</td>
						<td class="formC"><input type="text" maxlength="45" id="domicilio_calle"
							name="domicilio_calle" size="43" value="<?php echo $row['calle'];?>"/></td>
					</tr>
					<tr>
						<td class="formD">Nro:</td>
						<td class="formC"><input type="text" maxlength="10" id="domicilio_numero"
							name="domicilio_numero" size="43" value="<?php echo $row['nro'];?>"/>
						</td>
					</tr>
					<tr>
						<td class="formD">Piso:</td>
						<td class="formC"><input type="text" maxlength="3" id="domicilio_piso"
							name="domicilio_piso" size="43" value="<?php echo $row['piso'];?>"/>
						</td>
					</tr>
					<tr>
						<td class="formD">Dto:</td>
						<td class="formC"><input type="text" maxlength="3" id="domicilio_dto"
							name="domicilio_dto" size="43" value="<?php echo $row['dto'];?>" /></td>
					</tr>

					<tr>
						<td class="formD">Tel&eacute;fono:</td>
						<td class="formC"><input type="text" maxlength="20" id="telefono"
							name="telefono" size="43" value="<?php echo $row['telefono'];?>"/>
						</td>
					</tr>
					<tr>
						<td class="formD">Colegio:</td>
						<td class="formC"><input type="text" maxlength="20" id="colegio"
							name="colegio" size="43" value="<?php echo $row['colegio'];?>"/>
						</td>
					</tr>
					<tr>
						<td class="formD">Prepaga/Obra Social:</td>
						<td class="formC"><input type="text" maxlength="40" id="obraSocial"
							name="obraSocial" size="43" value="<?php echo $row['obraSocial'];?>"/>
						</td>
					</tr>
					<tr>
						<td class="formD">Nro de Afiliado:</td>
						<td class="formC"><input type="text" maxlength="40"
							id="afiliado" name="afiliado" size="43" value="<?php echo $row['afiliado'];?>"/>
						</td>
					</tr>
					<tr>
						<td class="formD">Tel&eacute;fonos en caso de Emergencias:</td>
						<td class="formC"><input type="text" maxlength="50"
							id="telEmergencias"
							name="telEmergencias" size="43" value="<?php echo $row['telEmergencias'];?>"/>
						</td>
					</tr>
					<tr>
						<td class="formD">Socio del Club:</td>
						<td class="formC"><select id="es_socio" name="es_socio">
								<option value="1" <?php if($row['socio']=='1'){echo "selected";} ?>>SI</option>
								<option value="0" <?php if($row['socio']=='0'){echo "selected";} ?>>NO</option>
						</select>
						</td>
					</tr>

					<tr>
						<td class="formD">Puesto en el que prefiere jugar:</td>
						<td class="formC"><select id="puesto_preferido" name="puesto_preferido">
								<option value="delantero" <?php if($row['puesto']=='delantero'){echo "selected";} ?>>Delantero</option>
								<option value="medio campo" <?php if($row['puesto']=='medio campo'){echo "selected";} ?>>Medio campo</option>
								<option value="defensor" <?php if($row['puesto']=='defensor'){echo "selected";} ?>>Defensor</option>
								<option value="arquero" <?php if($row['puesto']=='arquero'){echo "selected";} ?>>Arquero</option>
						</select>
						</td>
					</tr>
					<tr>
						<td class="formD">Nivel de juego</td>
						<td class="formC"><select id="nivel_de_juego" name="nivel_de_juego">
								<option value="1" <?php if($row['nivel']=='1'){echo "selected";} ?>>Regular</option>
								<option value="2" <?php if($row['nivel']=='2'){echo "selected";} ?>>Bueno</option>
								<option value="3" <?php if($row['nivel']=='3'){echo "selected";} ?>>Muy Bueno</option>
								<option value="4" <?php if($row['nivel']=='4'){echo "selected";} ?>>Excelente</option>
						</select>
						</td>
					</tr>
					<tr>
						<td class="formD">Asiste a escuelita de f&uacute;tbol</td>
						<td class="formC"><select id="asiste_esc_futbol"
							name="asiste_esc_futbol">
								<option value="1" <?php if($row['escuelaFutbol']=='1'){echo "selected";} ?>>SI</option>
								<option value="0" <?php if($row['escuelaFutbol']=='0'){echo "selected";} ?>>NO</option>
						</select>
						</td>
					</tr>
					<tr>
						<td class="formD">DNI Responsable:</td>
						<td class="formC"><input type="text" maxlength="50" disabled
							id="dniResponsable"	name="dniResponsable" size="43" value="<?php echo $row['respDNI'];?>"/>
						</td>
					</tr>
					<tr>	
						<td class="formD">&iquest;Responsable entrenador y su hijo capitan?</td>
						<td class="formC"><select id="padre_puede_colaborar"
							name="padre_puede_colaborar">
								<option value="1" <?php if($row['respEntrenador']=='1'){echo "selected";} ?>>SI</option>
								<option value="0" <?php if($row['respEntrenador']=='0'){echo "selected";} ?>>NO</option>
						</select>
						</td>
					</tr>
	<tr><td colspan='2'></td></tr>
	</tbody>
	</table>
	<div id="div_inscribir">
		<input id="botonCancelar" name="botonCancelar" type="button" value="Cancelar" />
		&nbsp;&nbsp;
		<input id="botonGuardar" name="botonGuardar" type="button" value="Guardar" />
	</div>	
	</div>


