
<script type="text/javascript" src="codigo/editapartidojuv.js"></script>

<?php

require_once "../../codigo/connw.php";

$idfixture = $_GET["idfixture"];  


$sql0 = "select f.idfixture, f.equipo1, e1.equipoDesc as eDesc1, f.equipo2, e2.equipoDesc as eDesc2, date(fecha)as dia, time(fecha) as hora, f.zona, f.resultado1, f.resultado2, f.estado, f.observaciones, f.cancha
		FROM fixture_juvenil f
		inner join equipo_juvenil e1
		on f.equipo1 = e1.idequipo
		inner join equipo_juvenil e2
		on f.equipo2 = e2.idequipo
		where f.idfixture = $idfixture;";

//echo $sql0;
$result = $mysqli->query($sql0);
$row = $result->fetch_assoc();
$idfixture= $row['idfixture'];
$estado= $row['estado'];
?>

<b>Edita Partido</b>
<div class="sp-table-wrapper">

	<table>
	<tr><td colspan='2'></td></tr>
	<tbody>
					<tr>
						<td class="formD" style=" width: 50%;">Equipo 1:</td>
						<td class="formC"><input type="text" maxlength="22" id="equipo1" name="equipo1" size="22" value="<?php echo $row['eDesc1']; ?>"  disabled/> 
						</td>
					</tr>
					<tr>
						<td class="formD" style=" width: 50%;">Resultado 1:</td>
						<td class="formC"><input type="text" maxlength="3" id="resultado1" name="resultado1" size="3" value="<?php echo $row['resultado1']; ?>" style="text-align: center;font-weight:bold; font-size:15px;" /> 
						</td>
					</tr>
					<tr>
						<td class="formD" style=" width: 50%;">Equipo 2:</td>
						<td class="formC"><input type="text" maxlength="22" id="equipo2" name="equipo2" size="22" value="<?php echo $row['eDesc2']; ?>"  disabled/> 
						</td>
					</tr>
					<tr>
						<td class="formD" style=" width: 50%;">Resultado 2:</td>
						<td class="formC"><input type="text" maxlength="3" id="resultado2" name="resultado2" size="3" value="<?php echo $row['resultado2']; ?>" style="text-align: center;font-weight:bold; font-size:15px;" /> 
						</td>
					</tr>
					<tr>
						<td class="formD" style=" width: 50%;">Fecha:</td>
						<td class="formC"><input type="text" class="fecha" id="fechape" value="<?php echo $row['dia']; ?>" size="12">
						</td>
					</tr>
					<tr>
						<td class="formD" style=" width: 50%;">Hora:</td>
						<td class="formC"><input type="text" class="fecha" id="horape" value="<?php echo $row['hora']; ?>" size="12">
						</td>
					</tr>
                    <tr>
						<td class="formD" style=" width: 50%;">Cancha:</td>
						<td class="formC"><input type="text" maxlength="200" id="canchanew" name="canchanew" size="22" value="<?php echo $row['cancha']; ?>" /> 
						</td>
					</tr>
					<tr>
						<td class="formD">Estado:</td>
						<td class="formC">
                            <select id="estado" name="estado">
								<option value="P" <?php if($estado=='P'){echo('selected');} ?>>Pendiente</option>
								<option value="J" <?php if($estado=='J'){echo('selected');} ?>>Jugado</option>
								<option value="S" <?php if($estado=='S'){echo('selected');} ?>>Suspendido</option>
                            </select> 
						</td>
					</tr>
					<tr>
						<td class="formD" style=" width: 50%;">Observaciones:</td>
						<td class="formC"><input type="text" maxlength="200" id="observaciones" name="observaciones" size="22" value="<?php echo $row['observaciones']; ?>" /> 
						</td>
					</tr>
					
	</tbody>
	</table>
	</div>
	<div id="div_inscribir">		
		<input id="botonCancelar" name="botonCancelar" type="button" value="Cancelar" />
		&nbsp;&nbsp;&nbsp;
		<input id="botonEliminar" name="botonEliminar" type="button" value="Eliminar" style="background-color: #dd5555;"/>
		&nbsp;&nbsp;&nbsp;
		<input id="botonGuardar" name="botonGuardar" type="button" value="Guardar" />
	</div>		

	<input type="hidden"  id="idfixture" name="idfixture" value="<?php echo($idfixture); ?>" />
