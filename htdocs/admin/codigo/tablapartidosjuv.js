$(document).ready(function() {

    $("#fechap").datepicker({
			showOn: "button",
			showAnim:"show",
			buttonImage: "../images/calendar.gif",
			buttonImageOnly: true,
			dateFormat: 'yy-mm-dd',
			numberOfMonths: 1,
			dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
	                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo','Junio', 'Julio', 'Agosto', 'Septiembre','Octubre', 'Noviembre', 'Diciembre'],
	                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr','May', 'Jun', 'Jul', 'Ago','Sep', 'Oct', 'Nov','Dic']  

		});
        
            
   	 $(".clickable-row").dblclick(function() {
		var idfixture = $(this).data("href");
			//alert(idfixture);
			$("#dialog").load("codigo/editapartidojuv.php?idfixture="+idfixture);
			$("#dialog").dialog("open");
		
    });

    $("#btagregar").click(function(){
		
        if( $("#NewEquipo1").val() == "" || $("#NewEquipo2").val() == "" || $("#fechap").val() == ""){
            alert("Faltan Datos");
        }else{
            if($("#NewEquipo1").val() == $("#NewEquipo2").val()){
                alert("No puede seleccionar el mismo equipo en los dos campos.");
            }else{
				
				var fechabien = $("#fechap").val()+" "+$("#horadesde").val()+":"+$("#mindesde").val()+":00";
                var data = 'idcategoria='+$("#txtcategoria").val()
				+"&equipo1="+$("#NewEquipo1").val()
				+"&equipo2="+$("#NewEquipo2").val()
                +"&cancha="+$("#cancha").val()
				+"&fecha="+fechabien
				;
			
				$.ajax({
					type: "POST",
					url: "codigo/agregapartidojuv.php",
					data: data,
					success: function(result) {
						//alert(result);
						$("#selcategoria").change();
					} 
				});
				
				
            }
        }
	});

	
	$("#btpdf").click(function() {
        //alert("pdf");
        var idcategoria = $("#txtcategoria").val();
		window.open('codigo/pdfpartidosjuv.php?categoria='+idcategoria, '_blank');
    });
	
		
	$("#btregpdf").click(function() {
        //alert("pdf");
        var idcategoria = $("#txtcategoria").val();
		window.open('codigo/pdfregistros.php?categoria='+idcategoria, '_blank');
    });
	
	$("#btActualizatabla").click(function() {
        //alert("btActualizatabla");
		var data = 'idcategoria='+$("#txtcategoria").val();
		$.ajax({
					type: "POST",
					url: "codigo/updatetablaposjuv.php",
					data: data,
					success: function(result) {
						//alert(result);
						alert("Tabla de posiciones actualizada");
					} 
		});
				
    });
	
});
