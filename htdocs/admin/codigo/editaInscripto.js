$(document).ready(function(){
	
    $("#botonCancelar").click(function(){
		
		$("#dialog").html("");
	    $("#dialog").dialog("close");
		
	}); 
	
    $("#botonGuardar").click(function(){
		
		//Validar formulario
		$(".error").remove();
		
		if( $("#nombre_y_apellido").val().length < 3){
            $("#nombre_y_apellido").focus().after("</br><span class='error'>Debe ingresar el Nombre y Apellido.</span>");
            return false;
		}else if( $("#dniResponsable").val() == ""){
			$("#dniResponsable").focus().after("</br><span class='error'>Debe ingresar el DNI.</span>");
			return false;
        }else if( Number($("#dniResponsable").val()) < 1000000){
            $("#dniResponsable").focus().after("</br><span class='error'>Debe ingresar un DNI v&aacute;lido.</span>");
            return false;
		}else if( $("#categoria").val() ==""){
            $("#fecha_de_nacimiento").focus().after("</br><span class='error'>Debe ingresar una fecha correcta.</span>");
            return false;
		}
		
		//alert("OK");
		Guardar();
		
	});	
	
	$(document).on("keyup", function(){
		$(".error").remove();
	});
	
	/*
	function ActualizaMontos(){
			var dniR = 'dni='+$("#dniResponsable").val();
			$.ajax({
                type: "GET",
				async: false,
				cache: false,
                url: "../inscripcion/actualizamontos.php",
                data: dniR,
                success: function(result) {
					return '1';
                } 
        }); 
	}
	*/
	
	function Guardar(){
		
		
		var fecha = $("#fecha_de_nacimiento").val();
		var fechabien = fecha.substring(6,10)+"-"+fecha.substring(3,5) +"-"+fecha.substring(0,2);
		var data = "dni="+$("#dni").val()
		+"&nomapellido="+$("#nombre_y_apellido").val()
		+"&fechanac="+fechabien
		+"&categoria="+$("#categoria").val()
		+"&email="+$("#email").val()
		+"&localidad="+$("#localidad").val()
		+"&calle="+$("#domicilio_calle").val()
		+"&numero="+$("#domicilio_numero").val()
		+"&piso="+$("#domicilio_piso").val()
		+"&dto="+$("#domicilio_dto").val()
		+"&telefono="+$("#telefono").val()
		+"&colegio="+$("#colegio").val()
		+"&obraSocial="+$("#obraSocial").val()
		+"&afiliado="+$("#afiliado").val()
		+"&telEmergencias="+$("#telEmergencias").val()
		+"&socio="+$("#es_socio").val()
		+"&puesto="+$("#puesto_preferido").val()
		+"&nivel="+$("#nivel_de_juego").val()
		+"&asiste="+$("#asiste_esc_futbol").val()
		+'&dniR='+$("#dniResponsable").val()
		+"&colaborar="+$("#padre_puede_colaborar").val()
		;
	
		
		$.ajax({
                type: "POST",
                url: "codigo/upinscripto.php",
				async: false,
				cache: false,
                data: data,
                success: function(result) {
					//alert(result);
					//$("#dialog").html(result);
					if (result=="OK."){
						//a=ActualizaMontos();
						if ($(location).attr('href').search("pagos.php")>0){
							$("#botonBusc").click();
						}else{
							location.reload();
						}
						alert("Recuerde verificar los Montos del Responsable.")
						$("#dialog").dialog("close");
					}else{
						$("#dialog").html(result);
					}

                } 
        }); 
	
	}
	
});		

