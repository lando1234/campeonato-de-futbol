$(document).ready(function() {
	

	$("#botonGuardar").click(function(){
		
			var data = 'resultado1='+$("#resultado1").val()
			+"&resultado2="+$("#resultado2").val()
			+"&fecha="+$("#fechape").val()
			+"&hora="+$("#horape").val()
            +"&cancha="+$("#canchanew").val()
			+"&estado="+$("#estado").val()
			+"&observaciones="+$("#observaciones").val()
			+"&idfixture="+$("#idfixture").val()
			;
			$.ajax({
                type: "POST",
                url: "codigo/updatepartidojuv.php",
                data: data,
                success: function(result) {
					//alert(result);
					$("#selcategoria").change();
					$("#dialog").dialog("close");
                } 
            });

	});
	
	$("#resultado1,#resultado2").change(function(){
		 $("#estado").val('J');
	});
		
	$("#botonEliminar").click(function(){
		if (confirm('Está seguro de eliminar el partido?')) {
			data = "idfixture="+$("#idfixture").val();
			$.ajax({
                type: "GET",
                url: "codigo/eliminapartidojuv.php",
                data: data,
                success: function(result) {
					$("#selcategoria").change();
					$("#dialog").dialog("close");
                } 
            }); 
		} 		
	});
	
	$("#botonCancelar").click(function(){
		$("#dialog").dialog("close");
	});
	
	
});
