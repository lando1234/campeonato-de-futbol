
<script type="text/javascript" src="codigo/editaequipo.js"></script>

<?php

require_once "../../codigo/connw.php";

$idequipo = $_GET["idequipo"];  


$sql0 = "SELECT equipoDesc, idcategoria, zona FROM equipo where idequipo = $idequipo;";

$result = $mysqli->query($sql0);
$row = $result->fetch_assoc();

$idcategoria = $row['idcategoria']; 
$equipoDesc = $row['equipoDesc']; 
$zona = $row['zona']; 

?>

<b>Edita Equipo</b>
<div class="sp-table-wrapper">

	<table>
	<tr><td colspan='2'></td></tr>
	<tbody>
					<tr>
						<td class="formD" style=" width: 50%;">Categor&iacute;a:</td>
						<td class="formC"><input type="text" maxlength="12" id="idcategoria" name="idcategoria" size="2" value="<?php echo $idcategoria; ?>"  disabled/> 
						</td>
					</tr>
					<tr>
						<td class="formD" style=" width: 50%;">Nombre Equipo:</td>
						<td class="formC"><input type="text" maxlength="22" id="equipoDesc" name="equipoDesc" size="18"  value="<?php echo $equipoDesc; ?>"  /> *
						</td>
					</tr>
					<tr>
						<td class="formD">Zona:</td>
						<td class="formC">
                            <select id="zona" name="zona">
								<option value="A" <?php if($zona=='A'){echo('selected');} ?>>A</option>
								<option value="B" <?php if($zona=='B'){echo('selected');} ?>>B</option>
                                <option value="C" <?php if($zona=='C'){echo('selected');} ?>>C</option>
                                <option value="D" <?php if($zona=='D'){echo('selected');} ?>>D</option>
                                <option value="Ganadores" <?php if($zona=='Ganadores'){echo('selected');} ?>>Ganadores</option>
								<option value="Ganadores A" <?php if($zona=='Ganadores A'){echo('selected');} ?>>Ganadores A</option>
								<option value="Ganadores B" <?php if($zona=='Ganadores B'){echo('selected');} ?>>Ganadores B</option>
                                <option value="Intermedia" <?php if($zona=='Intermedia'){echo('selected');} ?>>Intermedia</option>
                                <option value="Estimulo" <?php if($zona=='Estimulo'){echo('selected');} ?>>Estimulo</option>
								<option value="Estimulo A" <?php if($zona=='Estimulo A'){echo('selected');} ?>>Estimulo A</option>
								<option value="Estimulo B" <?php if($zona=='Estimulo B'){echo('selected');} ?>>Estimulo B</option>
                            </select> 
						</td>
					</tr>
					
	</tbody>
	</table>
	</div>
	<div id="div_inscribir">		
		<input id="botonCancelar" name="botonCancelar" type="button" value="Cancelar" />
		<input id="botonGuardar" name="botonGuardar" type="button" value="Guardar" />
	</div>		

	<input type="hidden"  id="idequipo" name="idequipo" value="<?php echo($idequipo); ?>" />
