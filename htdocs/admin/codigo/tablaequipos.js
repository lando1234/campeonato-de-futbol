$(document).ready(function() {
    
  
     $("tbody.connectedSortable").sortable({
        connectWith: ".connectedSortable",
        items: "> tr:not(:first)",
        
        receive: function( event, ui ) {
            calTodas();
            $("#wguardar").html("<- (*)");
        }
     });
    
    function calTodas(){
        var r=new Array() 
        for($i=0;$i<20;$i++){
            if ( $("#divtotal"+$i).length){
                r = calcular('table-draggable'+$i);
                $('#divtotal'+$i).html("Cantidad= "+r[0]+" - Promedio= "+r[1]);
                $('#cant'+$i).val(r[0]);
                $('#prom'+$i).val(r[1]);
            }
        }
    }
    
    function calcular(tabla){
        //alert("no");
        var suma = 0;
        var cuenta = -1;
        var r=new Array();
        $('#'+tabla+' tr').each(function(){
            suma += parseInt($(this).find('td').eq(2).text()||0,10);
            cuenta += 1;
        })
        if(cuenta>0){
            r[0]=cuenta;
            r[1]=(suma/cuenta).toFixed(2);
        }else{
            r[0]=0;
            r[1]=0;
        }           
        return r
    }
    
    calTodas();
    
    
    $( ".divtablaeq .connectedSortable  tr:not(:first-child)").dblclick(function() {
        var dni = $(this).data('href');
		$("#dialog").load("codigo/editajugador.php?dni="+dni);
		$("#dialog").dialog("open");

    });
    
	
    $("#btnarmar").click(function() {
        //alert("armar");
        
		$cantEq = $("#TotalEq").val();
		if(Number($cantEq)==0){
			alert("Al menos tiene que haber un equipo.");
		}else{
			$ind=1;
            $('#table-draggable0').find('tr:not(:first-child)').each(function (rowIndex, r) {
				$("#table-draggable"+$ind).append(this);
				$ind ++;
                if ($ind > $cantEq){$ind=1;}
			});
			$("#wguardar").html("<- (*)");
		}
		
		calTodas();
		
		//Viejo - Equipos de 11 jugadores
		/*
        $cantEqNec = Math.ceil($("#TotalJug").val()/11);
        $cantEq = $("#TotalEq").val();
        if($cantEq < $cantEqNec){
            alert("Cantidad de equipos insuficientes.");
        }else{
            $ind=1;
            $('#table-draggable0').find('tr:not(:first-child)').each(function (rowIndex, r) {
                while(Number($("#cant"+$ind).val())>=11){
					$ind ++;
				}
				$("#table-draggable"+$ind).append(this);
				$("#cant"+$ind).val(Number($("#cant"+$ind).val())+1);
                $ind ++;
                if ($ind > $cantEqNec){$ind=1;}
            });
			$("#wguardar").html("<- (*)");
        }
		
		calTodas();
        */
       
	   
        
    });
    
    $("#btlimpiar").click(function() {
        //alert("limpia");
                
        if (confirm('Esta seguro que quiere borrar los equipos y las modificaciones?')) {
            var data = "idcategoria="+$("#idcategoria").val();
            
            $.ajax({
                type: "POST",
                url: "codigo/limpiaequipo.php",
				async: false,
				cache: false,
                data: data,
                success: function(result) {
					//alert(result);
					//if (result=="OK."){
                        $("#selcategoria").change();
					//}

                } 
            });
        } 
        
    });
    
    
    $("#btnguardar").click(function() {
        //alert("guardar");
        var listado = [];
        
        listado.push([0, $("#idcategoria").val()]);

        $('#Divequipos table').each(function(){
            //alert("equipo "+$(this).data("idequipo"));
            idequipo =  $(this).data("idequipo");
            $(this).find('tr:not(:first-child)').each (function( column, tr) {
                //alert($(tr).data("href"));
                dnijug = $(tr).data("href");
				pos = $(tr).find('td:eq(1)').html();
				nivel = $(tr).find('td:eq(2)').html();
				entrenador = $(tr).data("entrenador");
                listado.push([dnijug,idequipo,pos,nivel,entrenador]);
            });  
        
        $("#wguardar").html("");
            
        });

        var jsonString = JSON.stringify(listado);
        //alert(jsonString);
         
         $.ajax({
                type: "POST",
                url: "codigo/guardarequipos.php",
				async: false,
				cache: false,
                 data: {data : jsonString}, 
                success: function(result) {
					//alert(result);
					//if (result=="OK."){
                        $("#selcategoria").change();
					//}

                } 
            });
            
            
        /*
        $('#Divequipos table').each(function(){
            //alert("equipo "+$(this).data("idequipo"));
            idequipo =  $(this).data("idequipo");
            $(this).find('tr:not(:first-child)').each (function( column, tr) {
                //alert($(tr).data("href"));
                dnijug = $(tr).data("href");
                sql = "insert into jugador_equipo (DNIjugador,idequipo) values('"+dnijug+"','"+idequipo+"')";
                alert(sql);
            });  

            
        });
        */
        
    });

    $(".elimequipo").click(function() {
		if (confirm('Esta seguro de eliminar el equipo?')) {
			data = "idequipo="+$(this).attr('name');
            //alert(data);
			$.ajax({
                type: "POST",
                url: "codigo/eliminaequipo.php",
                data: data,
                success: function(result) {
					 $("#selcategoria").change();
                } 
            });
		} 
	});
    
	
    $(".editaequipo").click(function() {
        var idequipo = $(this).attr('name');
		$("#dialog").load("codigo/editaequipo.php?idequipo="+idequipo);
		$("#dialog").dialog("open");

	});
    
	
    $("#btnuevoequipo").click(function() {
        var idcategoria = $("#idcategoria").val();
		$("#dialog").load("codigo/nuevoequipo.php?idcategoria="+idcategoria);
		$("#dialog").dialog("open");

    });
    
    $("#btpdf").click(function() {
        //alert("pdf");
        var idcategoria = $("#idcategoria").val();
		window.open('codigo/pdfequipos.php?categoria='+idcategoria, '_blank');
    });
    
});



