<?php 
include ("codigo/bloqueDeSeguridad.php");
require_once "../codigo/connr.php"; 

?>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="xmlrpc.php">
	<title>Campeonato Infantil de F&uacute;tbol &#8211; Noviembre 2019 &#8211; Club de Regatas Bella Vista</title>
	<link rel='dns-prefetch' href='//fonts.googleapis.com' />
	
	<link rel='stylesheet' id='style-css'  href='../css/style.css' type='text/css' media='all' />
	<link rel='stylesheet' id='framework-css'  href='../css/framework.css' type='text/css' media='all' />
	<link rel='stylesheet' id='style002-css'  href='../css/style002.css' type='text/css' media='all' />
	<link rel='stylesheet' id='sportspress'  href='../css/sportspress-sponsors.css' type='text/css' media='all' />
	<link rel='stylesheet' id='agregado-css'  href='../css/agregado.css' type='text/css' media='all' />

	<link rel="stylesheet" type="text/css" href="../css/themes/smoothness/jquery-ui-1.8.4.custom.css" />
	
	<script type="text/javascript" src="../codigo/jquery-1.8.2.js"></script>
	<script type="text/javascript" src="../codigo/jquery-ui-1.9.0.custom.min.js"></script>
		
	<script type="text/javascript" src="codigo/index.js"></script>
	
</head>

<body class="home page-template-default page page-id-242 custom-background">

<div class="sp-header"></div>
<div id="page" class="hfeed site">


	<header id="masthead" class="site-header" role="banner">
				<div class="header-area header-area-has-search">
				<div class="site-branding site-branding-empty">
					<div class="site-identity"></div>
				</div><!-- .site-branding -->
												
    <div class="site-menu">
        <?php include 'menu.php'; ?>
     </div>
</div>

		</header><!-- #masthead -->

	<div id="content" class="site-content">
		
	<div id="primary" class="content-area-full-width content-area-right-sidebar">
	<main id="main" class="site-main" role="main">

			
				
	<article id="post-242" class="post-242 page type-page status-publish hentry">
		<header class="entry-header">
			<h1 class="entry-title">Administraci&oacute;n</h1>	
		</header><!-- .entry-header -->

	</article><!-- #post-## -->
			
	</main><!-- #main -->
	</div><!-- #primary -->

	<div id="primary" class="widget-area content-area-rigth-sidebar">
		<main id="main" class="site-main" role="main">
				
<article id="post-242" class="post-242 page type-page status-publish hentry">
	<header class="entry-header">
				
			</header><!-- .entry-header -->

	<div class="entry-content">
		
<div class="sportspress sp-widget-align-none"><div class="sp-template sp-template-league-table">
	<h4 class="sp-table-caption">Inscriptos por Categor&iacute;a</h4>
	
	<?php
	$tot=0;
	$sql = "select c.idcategoria, case when i.cantidad is null then '-' else i.cantidad end as cantidad
			from categoria c
			left join (
				select i.idcategoria, count(*) as cantidad
				from inscriptos i
				where i.idtorneo = (select idtorneo from torneo where estado='A')
				group by i.idcategoria)i
			on c.idcategoria = i.idcategoria
			order by c.idcategoria";
	
	if (!$result = $mysqli->query($sql)) {
		echo "Lo sentimos, este sitio web está experimentando problemas.";
		exit;
	}
	?>
	
	
	<table class="sp-league-table sp-data-table sp-sortable-table sp-scrollable-table sp-paginated-table" data-sp-rows="10"><thead>
	<tr>
	
	<th class="data-name"></th>
	<th class="data-pj"></th>
	<tbody>
	<?php while ($row = $result->fetch_assoc()) { ?>
	<tr class="odd sp-row-no-0 clickable-row"  data-href="inscriptos.php?categoria=<?php echo $row['idcategoria']; ?>">
		<td class="data-name"><?php echo $row['idcategoria']; ?></td>
	<td class="data-pj"><?php $tot=$tot+$row['cantidad']; echo $row['cantidad']; ?></td>
	</tr>
	<?php }?>
	<tr class="odd sp-row-no-0 clickable-row" data-href="inscriptos.php" style="background:#dedede;">
		<td class="data-name"><b>Total</b></td>
	<td class="data-pj"><b><?php echo $tot; ?></b></td>
	</tr>
	</tbody>
	</table>
	
	</div>
</div>

			</div><!-- .entry-content -->
</article><!-- #post-## -->
			
		</main><!-- #main -->
	</div><!-- #primary -->

<div id="primary" class="widget-area content-area-rigth-sidebar">
		<main id="main" class="site-main" role="main">
				
<article id="post-242" class="post-242 page type-page status-publish hentry">
	<header class="entry-header">
				
			</header><!-- .entry-header -->

	<div class="entry-content">
		
<div class="sportspress sp-widget-align-none"><div class="sp-template sp-template-league-table">
	<h4 class="sp-table-caption">Aprobados (Pagados)</h4>
	
	<?php
	$tot=0;
	$sql = "select c.idcategoria, case when i.cantidad is null then '-' else i.cantidad end as cantidad
			from categoria c
			left join (
				select i.idcategoria, count(*) as cantidad
				from inscriptos i
				where i.idtorneo = (select idtorneo from torneo where estado='A')
				and aprobado=1
				group by i.idcategoria)i
			on c.idcategoria = i.idcategoria
			order by c.idcategoria";
	
	if (!$result = $mysqli->query($sql)) {
		echo "Lo sentimos, este sitio web está experimentando problemas.";
		exit;
	}
	?>
	
	<div class="sp-table-wrapper">
	<table class="sp-league-table sp-data-table sp-sortable-table sp-scrollable-table sp-paginated-table" data-sp-rows="10"><thead>
	<tr>
	
	<th class="data-name"></th>
	<th class="data-pj"></th>
	<tbody>
	<?php while ($row = $result->fetch_assoc()) { ?>
	<tr class="odd sp-row-no-0">
		<td class="data-name"><?php echo $row['idcategoria']; ?></td>
	<td class="data-pj"><?php $tot=$tot+$row['cantidad']; echo $row['cantidad']; ?></td>
	</tr>
	<?php }?>
	<tr class="odd sp-row-no-0" style="background:#dedede;">
		<td class="data-name"><b>Total</b></td>
	<td class="data-pj"><b><?php echo $tot; ?></b></td>
	</tr>
	</tbody>
	</table>
	</div></div>
</div>

			</div><!-- .entry-content -->
</article><!-- #post-## -->
			
		</main><!-- #main -->
	</div><!-- #primary -->
	
<div id="secondary" class="widget-area widget-area-right-sidebar" role="complementary">
		<main id="main" class="site-main" role="main">
				
<article id="post-242" class="post-242 page type-page status-publish hentry">
	<header class="entry-header">
				
			</header><!-- .entry-header -->

	<div class="entry-content">
		
<div class="sportspress sp-widget-align-none"><div class="sp-template sp-template-league-table">
	<h4 class="sp-table-caption">Falta de pago</h4>
	
	<?php
	$tot=0;
	$sql = "select c.idcategoria, case when i.cantidad is null then '-' else i.cantidad end as cantidad
			from categoria c
			left join (
				select i.idcategoria, count(*) as cantidad
				from inscriptos i
				where i.idtorneo = (select idtorneo from torneo where estado='A')
				and aprobado=0
				group by i.idcategoria)i
			on c.idcategoria = i.idcategoria
			order by c.idcategoria";
	
	if (!$result = $mysqli->query($sql)) {
		echo "Lo sentimos, este sitio web está experimentando problemas.";
		exit;
	}
	?>
	
	<div class="sp-table-wrapper">
	<table class="sp-league-table sp-data-table sp-sortable-table sp-scrollable-table sp-paginated-table" data-sp-rows="10"><thead>
	<tr>
	
	<th class="data-name"></th>
	<th class="data-pj"></th>
	<tbody>
	<?php while ($row = $result->fetch_assoc()) { ?>
	<tr class="odd sp-row-no-0">
		<td class="data-name"><?php echo $row['idcategoria']; ?></td>
	<td class="data-pj"><?php $tot=$tot+$row['cantidad']; echo $row['cantidad']; ?></td>
	</tr>
	<?php }?>
	<tr class="odd sp-row-no-0" style="background:#dedede;">
		<td class="data-name" ><b>Total</b></td>
	<td class="data-pj"><b><?php echo $tot; ?></b></td>
	</tr>
	</tbody>
	</table>
	</div></div>
</div>

			</div><!-- .entry-content -->
</article><!-- #post-## -->
			
		</main><!-- #main -->
	</div><!-- #primary -->


			
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-area">
			<div id="quaternary" class="footer-widgets" role="complementary">
		
		<div class="footer-widget-region">
									
		
		</div>
		
		
	<div class="footer-widget-region">
		<div class="sp-widget-align-none">
		
		
	
	</div></div>
									
							</div>
		</div><!-- .footer-area -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<p>&nbsp;</p>

<div id="dialog" title="Atenci&oacute;n">
Cargando...	
</div>


</body>
