<?php 
include ("codigo/bloqueDeSeguridad.php");
require_once "../codigo/connr.php"; 

?>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="xmlrpc.php">
	<title>Campeonato Infantil de F&uacute;tbol &#8211; Noviembre 2019 &#8211; Club de Regatas Bella Vista</title>
	<link rel='dns-prefetch' href='//fonts.googleapis.com' />
	
	<link rel='stylesheet' id='style-css'  href='../css/style.css' type='text/css' media='all' />
	<link rel='stylesheet' id='framework-css'  href='../css/framework.css' type='text/css' media='all' />
	<link rel='stylesheet' id='style002-css'  href='../css/style002.css' type='text/css' media='all' />
	<link rel='stylesheet' id='sportspress'  href='../css/sportspress-sponsors.css' type='text/css' media='all' />
	<link rel='stylesheet' id='agregado-css'  href='../css/agregado.css' type='text/css' media='all' />

	<link rel="stylesheet" type="text/css" href="../css/themes/smoothness/jquery-ui-1.8.4.custom.css" />
	
	<script type="text/javascript" src="../codigo/jquery-1.8.2.js"></script>
	<script type="text/javascript" src="../codigo/jquery-ui-1.9.0.custom.min.js"></script>
	
		
	<script type="text/javascript" src="codigo/juvenilequipos.js"></script>
	
	

</head>

<body class="home page-template-default page page-id-242 custom-background">

<div class="sp-header"></div>
<div id="page" class="hfeed site">


	<header id="masthead" class="site-header" role="banner">
				<div class="header-area header-area-has-search">
				<div class="site-branding site-branding-empty">
					<div class="site-identity"></div>
				</div><!-- .site-branding -->
												
    <div class="site-menu">
        <?php include 'menu.php'; ?>
     </div>
    
						</div>
		</header><!-- #masthead -->

	<div id="content" class="site-content">
		
	<div id="primary" class="content-area-full-width content-area-right-sidebar">
	<main id="main" class="site-main" role="main">

			
				
	<article id="post-242" class="post-242 page type-page status-publish hentry">
		<header class="entry-header">
			<h1 class="entry-title">Montos</h1>	
		</header><!-- .entry-header -->

	</article><!-- #post-## -->
			
	</main><!-- #main -->
	</div><!-- #primary -->

	<div id="primary" class="content-area-full-width content-area-right-sidebar">
		<main id="main" class="site-main" role="main">
				
<article id="post-242" class="post-242 page type-page status-publish hentry">
	<header class="entry-header">
				
			</header><!-- .entry-header -->

	<div class="entry-content">
	Monto Infantiles por Categor&iacute;a
<div class="sportspress sp-widget-align-none"><div class="sp-template sp-template-league-table">
	<h4 class="sp-table-caption"></h4>
	
	<?php
	$sql = "select s.idcategoria, s.cant as QSoc, s.monto as Socios, n.cant as QNsoc, n.monto as NoSocios, s.cant+n.cant as Cantidad, s.monto + n.monto as total
			from (
			SELECT idcategoria, count(*) as cant, sum(monto) as monto
			FROM inscriptos
			where idtorneo = (select idtorneo from torneo where estado='A')
			and aprobado = 1
			and socio = 0
			group by idcategoria
			)n
			inner join (
			SELECT idcategoria, count(*) as cant, sum(monto)as monto
			FROM inscriptos
			where idtorneo = (select idtorneo from torneo where estado='A')
			and aprobado = 1
			and socio = 1
			group by idcategoria
			)s
			on s.idcategoria = n.idcategoria
			order by 1
			";
	
	if (!$result = $mysqli->query($sql)) {
		echo "Lo sentimos, este sitio web está experimentando problemas.";
		exit;
	}
	?>
	
	
	<table id="datatables" class="display dataTable" style="font-size: 11px;">
		<thead>
		<tr style="background-color: #e0e0e0;">
			<th>Categor&iacute;a</th>
			<th>Q Socios</th>
            <th>Monto Socios</th>
			<th>Q No Socios</th>
			<th>Monto No Socios</th>
			<th>Q Total</th>
            <th>Monto Total</th>
		</tr>
		</thead>
		<tbody>
		<?php
			$Qsocios = 0;
			$socios = 0;
			$QNsocios = 0;
			$Nsocios = 0;
			$Qtotal = 0;
			$total = 0;
			
			while ($row = $result->fetch_assoc()) {
			$Qsocios = $Qsocios + $row['QSoc'];
			$socios = $socios + $row['Socios'];
			$QNsocios = $QNsocios + $row['QNsoc'];			
			$Nsocios = $Nsocios + $row['NoSocios'];
			$Qtotal = $Qtotal + $row['Cantidad'];
			$total = $total + $row['total'];
		?>
		<tr>
			<td><?php echo $row['idcategoria']?></td>
			<td><?php echo $row['QSoc'];?></td>
            <td><?php echo '$ '.number_format($row['Socios'], 0, ',', '.');?></td>
			<td><?php echo $row['QNsoc'];?></td>
			<td><?php echo '$ '.number_format($row['NoSocios'], 0, ',', '.');?></td>
			<td><?php echo $row['Cantidad'];?></td>
            <td><?php echo '$ '.number_format($row['total'], 0, ',', '.');?></td>
			</tr>
			<?php }?>
		<tr style="background-color: #e0e0e0;">
		<td style="font-weight: bolder;">Total</td>
		<td style="font-weight: bolder;"><?php echo $Qsocios; ?></td>
		<td style="font-weight: bolder;"><?php echo '$ '.number_format($socios, 0, ',', '.'); ?></td>
		<td style="font-weight: bolder;"><?php echo $QNsocios; ?></td>
		<td style="font-weight: bolder;"><?php echo '$ '.number_format($Nsocios, 0, ',', '.'); ?></td>
		<td style="font-weight: bolder;"><?php echo $Qtotal; ?></td>
		<td style="font-weight: bolder;"><?php echo '$ '.number_format($total, 0, ',', '.'); ?></td>
		</tr>
		</tbody>
	</table>
	
	</div>
</div>


			</div><!-- .entry-content -->
			
<div class="entry-content">
	Monto Juveniles por Categor&iacute;a
<div class="sportspress sp-widget-align-none"><div class="sp-template sp-template-league-table">
	<h4 class="sp-table-caption"></h4>
	
	<?php
	$sql = "SELECT idcategoria, sum(monto) as monto
			FROM responsable_equipo_juvenil rej
			inner join equipo_juvenil ej
			on rej.idequipo = ej.idequipo
			where ej.idtorneo = (select idtorneo from torneo where estado='A')
			and rej.aprobado = 1
			group by idcategoria
			order by 1
			";
	
	if (!$result = $mysqli->query($sql)) {
		echo "Lo sentimos, este sitio web está experimentando problemas.";
		exit;
	}
	?>
	
	
	<table id="datatables" class="display dataTable" style="font-size: 11px;">
		<thead>
		<tr style="background-color: #e0e0e0;">
			<th>Categor&iacute;a</th>
            <th>Total</th>
		</tr>
		</thead>
		<tbody>
		<?php
			$monto = 0;
			while ($row = $result->fetch_assoc()) {
			$monto = $monto + $row['monto'];
		?>
		<tr>
			<td><?php echo $row['idcategoria']?></td>
            <td><?php echo '$ '.number_format($row['monto'], 0, ',', '.');?></td>
			</tr>
			<?php }?>
		<tr style="background-color: #e0e0e0;">
		<td style="font-weight: bolder;">Total</td>
		<td style="font-weight: bolder;"><?php echo '$ '.number_format($monto, 0, ',', '.'); ?></td>
		</tr>
		</tbody>
	</table>
	
	</div>
</div>


			</div><!-- .entry-content -->			
			
<div class="entry-content">
	Ingresos Registrados Totales (Pagos Ingresados al sistema)
<div class="sportspress sp-widget-align-none"><div class="sp-template sp-template-league-table">
	<h4 class="sp-table-caption"></h4>
	
	<?php
	$sql = "SELECT 'Infantil' as Tipo, sum(monto) as monto
			FROM pagos
			where idtorneo = (select idtorneo from torneo where estado='A')
			union
			SELECT 'Juvenil' as Tipo, sum(monto) as monto
			FROM pagos_juvenil
			where idtorneo = (select idtorneo from torneo where estado='A')
			";
	
	if (!$result = $mysqli->query($sql)) {
		echo "Lo sentimos, este sitio web está experimentando problemas.";
		exit;
	}
	?>
	
	
	<table id="datatables" class="display dataTable" style="font-size: 11px;">
		<thead>
		<tr style="background-color: #e0e0e0;">
			<th>Torneo</th>
            <th>Total</th>
		</tr>
		</thead>
		<tbody>
		<?php
			$final = 0;
			while ($row = $result->fetch_assoc()) {
			$final = $final + $row['monto'];
		?>
		<tr>
			<td><?php echo $row['Tipo']?></td>
            <td><?php echo '$ '.number_format($row['monto'], 0, ',', '.');?></td>
			</tr>
			<?php }?>
		<tr style="background-color: #e0e0e0;">
		<td style="font-weight: bolder;">Total</td>
		<td style="font-weight: bolder;"><?php echo '$ '.number_format($final, 0, ',', '.'); ?></td>
		</tr>
		</tbody>
	</table>
	
	</div>
</div>


			</div><!-- .entry-content -->					
			
</article><!-- #post-## -->
			
		</main><!-- #main -->
	</div><!-- #primary -->




			
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-area">
			<div id="quaternary" class="footer-widgets" role="complementary">
		
		<div class="footer-widget-region">
									
		
		</div>
		
		
	<div class="footer-widget-region">
		<div class="sp-widget-align-none">
		
		
	
	</div></div>
									
							</div>
		</div><!-- .footer-area -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<p>&nbsp;</p>

<div id="dialog" title="Atenci&oacute;n">
Cargando...	
</div>


</body>
