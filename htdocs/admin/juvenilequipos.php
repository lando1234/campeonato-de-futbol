<?php 
include ("codigo/bloqueDeSeguridad.php");
require_once "../codigo/connr.php"; 

?>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="xmlrpc.php">
	<title>Campeonato Infantil de F&uacute;tbol &#8211; Noviembre 2019 &#8211; Club de Regatas Bella Vista</title>
	<link rel='dns-prefetch' href='//fonts.googleapis.com' />
	
	<link rel='stylesheet' id='style-css'  href='../css/style.css' type='text/css' media='all' />
	<link rel='stylesheet' id='framework-css'  href='../css/framework.css' type='text/css' media='all' />
	<link rel='stylesheet' id='style002-css'  href='../css/style002.css' type='text/css' media='all' />
	<link rel='stylesheet' id='sportspress'  href='../css/sportspress-sponsors.css' type='text/css' media='all' />
	<link rel='stylesheet' id='agregado-css'  href='../css/agregado.css' type='text/css' media='all' />

	<link rel="stylesheet" type="text/css" href="../css/themes/smoothness/jquery-ui-1.8.4.custom.css" />
	
	<script type="text/javascript" src="../codigo/jquery-1.8.2.js"></script>
	<script type="text/javascript" src="../codigo/jquery-ui-1.9.0.custom.min.js"></script>
	
		
	<script type="text/javascript" src="codigo/juvenilequipos.js"></script>
	
	

</head>

<body class="home page-template-default page page-id-242 custom-background">

<div class="sp-header"></div>
<div id="page" class="hfeed site">


	<header id="masthead" class="site-header" role="banner">
				<div class="header-area header-area-has-search">
				<div class="site-branding site-branding-empty">
					<div class="site-identity"></div>
				</div><!-- .site-branding -->
												
    <div class="site-menu">
        <?php include 'menu.php'; ?>
     </div>
    
						</div>
		</header><!-- #masthead -->

	<div id="content" class="site-content">
		
	<div id="primary" class="content-area-full-width content-area-right-sidebar">
	<main id="main" class="site-main" role="main">

			
				
	<article id="post-242" class="post-242 page type-page status-publish hentry">
		<header class="entry-header">
			<h1 class="entry-title">Equipos Juveniles</h1>	
		</header><!-- .entry-header -->

	</article><!-- #post-## -->
			
	</main><!-- #main -->
	</div><!-- #primary -->

	<div id="primary" class="content-area-full-width content-area-right-sidebar">
		<main id="main" class="site-main" role="main">
				
<article id="post-242" class="post-242 page type-page status-publish hentry">
	<header class="entry-header">
				
			</header><!-- .entry-header -->

	<div class="entry-content">
		
<div class="sportspress sp-widget-align-none"><div class="sp-template sp-template-league-table">
	<h4 class="sp-table-caption"></h4>
	
	<?php
	$sql = "SELECT 
        rj.respDNI, rj.password, rj.nombreApellido, rj.respTelefono, rj.respMail,
        e.equipoDesc, e.idcategoria, e.zona, c.q, rej.aprobado, case when d.total is null then 0 else d.total end as pagado
        FROM responsable_juvenil rj
        inner join responsable_equipo_juvenil rej
        on rj.respDNI = rej.respDNI
        inner join equipo_juvenil e
        on e.idequipo = rej.idequipo
        left join (select idequipo, count(*) as q
            from jugador_equipo_juvenil
            group by idequipo)c
        on c.idequipo = e.idequipo
		left join (select respDNI, sum(monto) as total
			from pagos_juvenil
			group by respDNI)d
		on rj.respDNI=d.respDNI
        where e.idtorneo = (select idtorneo from torneo where estado='A')
		order by e.idcategoria, e.zona, rj.respDNI";
	
	if (!$result = $mysqli->query($sql)) {
		echo "Lo sentimos, este sitio web está experimentando problemas.";
		exit;
	}
	?>
	
	
	<table id="datatables" class="display dataTable" style="font-size: 11px;">
		<thead>
		<tr>
			<th>Resp DNI</th>
            <th>Password</th>
			<th>Nombre Respopnsable</th>
            <th>Tel&eacute;fono</th>
            <th>Mail</th>
            <th>Nombre equipo</th>
			<th>Cat.</th>
			<th>Zona</th>
            <th>Jug</th>
			<th>Aprobado</th>
			<th>Pagado</th>
		</tr>
		</thead>
		<tbody>
		<?php
			while ($row = $result->fetch_assoc()) {
		?>
		<tr class="clickable-row" data-href="<?php echo $row['respDNI']?>">
			<td><?php echo $row['respDNI']?></td>
            <td><?php echo $row['password']?></td>
			<td><?php echo $row['nombreApellido']?></td>
            <td><?php echo $row['respTelefono']?></td>
            <td><?php echo $row['respMail']?></td>
			<td><?php echo $row['equipoDesc']?></td>
			<td><?php echo $row['idcategoria']?></td>
			<td><?php echo $row['zona']?></td>
            <td><?php echo $row['q']?></td>
			<td><?php echo $row['aprobado']?></td>
			<td><?php echo $row['pagado']?></td>
			</tr>
			<?php }?>
		</tbody>
	</table>
	
	</div>
</div>

<input style='padding: 5px;float:left;' type='button' name='btpdf' id='btpdf' value='Bajar a PDF' />

			</div><!-- .entry-content -->
</article><!-- #post-## -->
			
		</main><!-- #main -->
	</div><!-- #primary -->




			
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-area">
			<div id="quaternary" class="footer-widgets" role="complementary">
		
		<div class="footer-widget-region">
									
		
		</div>
		
		
	<div class="footer-widget-region">
		<div class="sp-widget-align-none">
		
		
	
	</div></div>
									
							</div>
		</div><!-- .footer-area -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<p>&nbsp;</p>

<div id="dialog" title="Atenci&oacute;n">
Cargando...	
</div>


</body>
