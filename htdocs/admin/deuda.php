<?php 
include ("codigo/bloqueDeSeguridad.php");
require_once "../codigo/connr.php"; 

?>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="xmlrpc.php">
	<title>Campeonato Infantil de F&uacute;tbol &#8211; Noviembre 2019 &#8211; Club de Regatas Bella Vista</title>
	<link rel='dns-prefetch' href='//fonts.googleapis.com' />
	
	<link rel='stylesheet' id='style-css'  href='../css/style.css' type='text/css' media='all' />
	<link rel='stylesheet' id='framework-css'  href='../css/framework.css' type='text/css' media='all' />
	<link rel='stylesheet' id='style002-css'  href='../css/style002.css' type='text/css' media='all' />
	<link rel='stylesheet' id='sportspress'  href='../css/sportspress-sponsors.css' type='text/css' media='all' />
	<link rel='stylesheet' id='agregado-css'  href='../css/agregado.css' type='text/css' media='all' />

	<link rel="stylesheet" type="text/css" href="../css/themes/smoothness/jquery-ui-1.8.4.custom.css" />
	<link rel="stylesheet" type="text/css" href="../media/css/demo_table_jui.css" />
	
	<script type="text/javascript" src="../codigo/jquery-1.8.2.js"></script>
	<script type="text/javascript" src="../codigo/jquery-ui-1.9.0.custom.min.js"></script>
	<script type="text/javascript" src="../media/js/jquery.dataTables.js"></script>	
	<script type="text/javascript" src="codigo/deuda.js"></script>
	
<script type="text/javascript">
$(document).ready(function() {
    $('.dataTable').dataTable({
        "sPaginationType":"full_numbers",
        "aaSorting":[[8, "desc"]],
        "bJQueryUI":true
	});
});

</script>
			
</head>

<body class="home page-template-default page page-id-242 custom-background">

<div class="sp-header"></div>
<div id="page" class="hfeed site">


	<header id="masthead" class="site-header" role="banner">
				<div class="header-area header-area-has-search">
				<div class="site-branding site-branding-empty">
					<div class="site-identity"></div>
				</div><!-- .site-branding -->
												
    <div class="site-menu">
        <?php include 'menu.php'; ?>
     </div>
     
</div>
		</header><!-- #masthead -->

	<div id="content" class="site-content">
		
	<div id="primary" class="content-area-full-width content-area-right-sidebar">
	<main id="main" class="site-main" role="main">

			
				
	<article id="post-242" class="post-242 page type-page status-publish hentry">
		<header class="entry-header">
			<h1 class="entry-title">Pagos Adeudados</h1>	
		</header><!-- .entry-header -->

	</article><!-- #post-## -->
			
	
		
				
<article id="post-242" class="post-242 page type-page status-publish hentry">
	<header class="entry-header">
				
			</header><!-- .entry-header -->

	<div class="entry-content">
		
<div class="sportspress sp-widget-align-none"><div class="sp-template sp-template-league-table">
	<h4 class="sp-table-caption"></h4>
	
	<div class="sp-table-wrapper">
    <div id="divpagos" name="divpagos" style="min-height: 200px;">
    
    
    
<?php 

if (isset($_GET["categoria"])){
    $cat = $_GET["categoria"];    
}else{
    $cat = "";
}


	$tot=0;
	
	$sql = "select respDNI, nombreApellido, respTelefono, respMail, Qinsc, aprobados, monto, pago ,monto - pago as debe
            from
            (SELECT r.respDNI, r.nombreApellido, r.respTelefono, r.respMail, d.Qinsc, d.aprobados, d.monto, 
            case when p.pago is null then 0 else p.pago end as pago
            FROM responsable r
            inner join
            (SELECT respDNI, sum(monto) as monto, count(*) as Qinsc, sum(aprobado) aprobados
            FROM inscriptos
            where idtorneo = (select idtorneo from torneo where estado='A')
            group by respDNI)d
            on r.respDNI = d.respDNI
            left join 
            (SELECT respDNI, sum(monto) as pago
            from pagos
            where idtorneo = (select idtorneo from torneo where estado='A')
            group by respDNI
            )p
            on r.respDNI = p.respDNI
            )t
            where monto - pago <> 0
            order by debe desc
            ";

	if (!$result = $mysqli->query($sql)) {
		echo "Lo sentimos, este sitio web está experimentando problemas.";
		exit;
	}
	?>
    
    	<table id="datatables" class="display dataTable" style="font-size: 11px;">
		<thead>
		<tr>
			<th>Responsable</th>
			<th>Nombre y Apellido</th>
			<th>Telefono</th>
			<th>Mail</th>
			<th>Q Insc.</th>
            <th>Q Aprob.</th>
			<th>Monto</th>
			<th>Pagado</th>
			<th>Deuda</th>
		</tr>
		</thead>
		<tbody>
		<?php
			while ($row = $result->fetch_assoc()) {
			$tot=$tot+$row['debe'];
		?>
		<tr class="clickable-row" data-href="<?php echo $row['respDNI']?>">
			<td><?php echo $row['respDNI']?></td>
			<td><?php echo $row['nombreApellido']?></td>
			<td><?php echo $row['respTelefono']?></td>
			<td><?php echo $row['respMail']?></td>
			<td><?php echo $row['Qinsc']?></td>
            <td><?php echo $row['aprobados']?></td>
			<td><?php echo $row['monto']?></td>
			<td><?php echo $row['pago']?></td>
			<td><?php echo $row['debe']?></td>
			</tr>
			<?php }?>
		</tbody>
	</table>

   <span style="font-size:10px;"><u>Nota</u>: Para ir a pago, haga <b>doble click</b> sobre el jugador.</span>
    <div style="float:right; padding:0 10px;background-color: #e0e0e0;">Total adeudado = $  <?php echo $tot;?></div>
    </div>
	
	</div>
	</div>
</div>

			</div><!-- .entry-content -->
</article><!-- #post-## -->
			
		</main><!-- #main -->
	</div><!-- #primary -->



			
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-area">
			<div id="quaternary" class="footer-widgets" role="complementary">
		
		<div class="footer-widget-region">
									
		
		</div>
		
		
	<div class="footer-widget-region">
		<div class="sp-widget-align-none">
		
		
	
	</div></div>
									
							</div>
		</div><!-- .footer-area -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<p>&nbsp;</p>

<div id="dialog" title="Pagos">
Cargando...	
</div>

</body>
