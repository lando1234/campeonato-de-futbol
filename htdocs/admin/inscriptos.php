<?php 
include ("codigo/bloqueDeSeguridad.php");
require_once "../codigo/connr.php"; 

if (isset($_GET["categoria"])){
    $cat = $_GET["categoria"];    
}else{
    $cat = "";
}

$soloMujeres = $_SESSION["usuario"] == 'mujeres';
?>
<!DOCTYPE html>
<html lang="es-ES">
<head>
    
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="xmlrpc.php">
	<title>Campeonato Infantil de F&uacute;tbol &#8211; Noviembre 2019 &#8211; Club de Regatas Bella Vista</title>
	<link rel='dns-prefetch' href='//fonts.googleapis.com' />
	
	<link rel='stylesheet' id='style-css'  href='../css/style.css' type='text/css' media='all' />
	<link rel='stylesheet' id='framework-css'  href='../css/framework.css' type='text/css' media='all' />
	<link rel='stylesheet' id='style002-css'  href='../css/style002.css' type='text/css' media='all' />
	<link rel='stylesheet' id='sportspress'  href='../css/sportspress-sponsors.css' type='text/css' media='all' />
	<link rel='stylesheet' id='agregado-css'  href='../css/agregado.css' type='text/css' media='all' />

	<link rel="stylesheet" type="text/css" href="../css/themes/smoothness/jquery-ui-1.8.4.custom.css" />
	<link rel="stylesheet" type="text/css" href="../media/css/demo_table_jui.css" />
	
	<script type="text/javascript" src="../codigo/jquery-1.8.2.js"></script>
	<script type="text/javascript" src="../codigo/jquery-ui-1.9.0.custom.min.js"></script>
	<script type="text/javascript" src="../media/js/jquery.dataTables.js"></script>	
	<script type="text/javascript" src="codigo/inscriptos.js"></script>
	<script>
	 <?php 
		if($soloMujeres){
			echo("soloMujeres = true");
		} else {
			echo("soloMujeres = false");
		}
	 ?>
	</script>
			
</head>

<body class="home page-template-default page page-id-242 custom-background">


<div class="sp-header"></div>
<div id="page" class="hfeed site">


	<header id="masthead" class="site-header" role="banner">
				<div class="header-area header-area-has-search">
				<div class="site-branding site-branding-empty">
					<div class="site-identity"></div>
				</div><!-- .site-branding -->
												
	<div class="site-menu">
        <?php include 'menu.php'; ?>
     </div>
     
</div>
		</header><!-- #masthead -->

	<div id="content" class="site-content">
		
	<div id="primary" class="content-area-full-width content-area-right-sidebar">
	<main id="main" class="site-main" role="main">

			
				
	<article id="post-242" class="post-242 page type-page status-publish hentry">
		<header class="entry-header">
			<h1 class="entry-title">Inscriptos</h1>	
		</header><!-- .entry-header -->

	</article><!-- #post-## -->
			
	
		
				
<article id="post-242" class="post-242 page type-page status-publish hentry">
	<header class="entry-header">
				
			</header><!-- .entry-header -->

	<div class="entry-content">
		
<div class="sportspress sp-widget-align-none"><div class="sp-template sp-template-league-table">
	<h4 class="sp-table-caption">Inscriptos por Categor&iacute;a: 
    &nbsp;&nbsp;<select id="selcategoria" name="selcategoria" style="font-size:12px;text-align: center;">
		<?php $sql1 = "SELECT idcategoria FROM categoria order by 1;";
				if($soloMujeres){
					$sql1 = "SELECT idcategoria FROM categoria where fechaInicio is null order by 1";
				}
            $result1 = $mysqli->query($sql1);
                echo "<option value=''>Todas</option>";
            while ($row1 = $result1->fetch_assoc()){
                if($row1['idcategoria']==$cat){$ss='selected';}else{$ss='';}
                echo "<option value='".$row1['idcategoria']."' ".$ss.">".$row1['idcategoria']."</option>";
            }
            
        ?>
	</select></h4>
	
	<div class="sp-table-wrapper">
    <div id="tablainsc" name="tablainsc">
    
    </div>
	
	</div>
	</div>
</div>

			</div><!-- .entry-content -->
</article><!-- #post-## -->
			
		</main><!-- #main -->
	</div><!-- #primary -->



			
	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="footer-area">
			<div id="quaternary" class="footer-widgets" role="complementary">
		
		<div class="footer-widget-region">
									
		
		</div>
		
		
	<div class="footer-widget-region">
		<div class="sp-widget-align-none">
		
		
	
	</div></div>
									
							</div>
		</div><!-- .footer-area -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<p>&nbsp;</p>

<div id="dialog" title="Editar Inscriptos">
Cargando...	
</div>

</body>
