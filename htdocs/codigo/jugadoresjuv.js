$(document).ready(function(){
	
	$("#dialog").dialog({
            autoOpen: false,
            modal: true,
            width: 450,
            height: 250,
            show: "show",
            hide: "hide"
	});
	
	$("#dialog").click(function() {
   		 	$("#dialog").html("");
	        $("#dialog").dialog("close");
        });
		
	$(".regular").slick({
		dots: false,
		autoplay: true,
		autoplaySpeed: 4000,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 2
    });
	
	$("#fotos").click(function(){
		$(location).attr('href','fotos.php');
	});
	
	$("#btinicio").click(function(){
		$(location).attr('href','menu.php');		
	});

	$("#btcategoria").click(function(){
		$(location).attr('href','principaljuv.php');		
	});

    $("#btfixture").click(function(){
		$(location).attr('href','fixturejuv.php');		
	});
    
	$("#btcanchas").click(function(){
		$(location).attr('href','canchas.php');		
	});
	
	$("#btregalmento").click(function(){
		const loc = window.location.href;
		$(location).attr('href',`/reglamento/REGLAMENTO-${loc.substring(loc.indexOf('cat=')+4)}.pdf);
	});
	
	$("#btcontacto").click(function(){
		$("#dialog").html("Por favor, env&iacute;e un mail a secretar&iacute;a con su consulta.</br></br>secretaria@regatasbellavista.com.ar</br></br>Muchas Gracias.");
		$("#dialog").dialog("open");
		
	});

		
	$("#btautorizados").click(function(){
		$(location).attr('href','admin/');		
	});
	
});		
